function DatePicker(icon, field) {
    this.icon = icon;
    this.field = field;
    YAHOO.util.Event.addListener(window, 'load', this.initialize, this, true);
}
DatePicker.prototype = {

    icon : null,

    field : null,

    calendar : null,

    id : 'date-calendar',

    container : null,

    initialize : function() {
        YAHOO.util.Event.addListener(this.icon, 'click', this.click, this, true);
        this.renderContainer();
    },

    renderContainer : function() {
        this.container = document.createElement('div');
        this.container.style.display = 'none';
        document.body.appendChild(this.container);
    },

    click : function(e) {
        if(this.calendar === null) {
            this.renderCalendar();
        }
        this.calendar.show();
        this.positionCalendar();
    },

    renderCalendar : function() {
        this.calendar = new YAHOO.widget.Calendar(this.field+'-calendar', this.container, { title:'Choose a date:', close:true, navigator: true } );
        this.calendar.selectEvent.subscribe(this.populateDateField, this, true);
        this.calendar.render();
    },

    positionCalendar : function() {
        var position = YAHOO.util.Dom.getXY(this.field);
        position[1] = position[1] + 25;
        YAHOO.util.Dom.setXY(this.container, position);
    },

    populateDateField : function() {
        var date = this.calendar.getSelectedDates()[0];
        YAHOO.util.Dom.get(this.field).value = date.getMonth() + '/' + date.getDate() + '/' + date.getFullYear();
        this.calendar.hide();
    },

    hide : function() {
        if(this.calendar !== null) {
            this.calendar.hide();
        }
    }

};