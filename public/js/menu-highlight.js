function extractPageName(hrefString)
{
	var arr = hrefString.split('/');
	return  (arr.length < 2) ? hrefString : arr[arr.length-2].toLowerCase() + arr[arr.length-1].toLowerCase();
}
					 
function setActiveMenu(arr, crtPage)
{
	for (var i=0; i < arr.length; i++)
	{
		if(extractPageName(arr[i].href) == crtPage)
		{
			if (arr[i].parentNode.tagName != "DIV")
			{
				arr[i].className += " current";
				arr[i].parentNode.parentNode.parentNode.parentNode.firstChild.className += " current";
			}
		}
	}
}
					 
function setPage()
{
	hrefString = document.location.href ? document.location.href : document.location;				 
	var menuID = null;
	if (document.getElementById("menu-top") !=null ){
		menuID = document.getElementById("menu-top");
	}if (document.getElementById("menu-top-english") !=null ){
		menuID = document.getElementById("menu-top-english");
	}if (document.getElementById("menu-top-serbian") !=null ){
		menuID = document.getElementById("menu-top-serbian");
	}
	setActiveMenu(menuID.getElementsByTagName("a"), extractPageName(hrefString));
}

	window.onload=function()
{
	setPage();
}