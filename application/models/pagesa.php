<?php
	class Pagesa extends Eloquent{
		public static $per_page = 100;
		public static $timestamps = false;
		public static $table = "pagesat";

		public function transaksionet(){
			return $this->has_many("Transaksion", "pagesa_id");
		}
	}
?>