<?php
	class Transaksion extends Eloquent{
		
		public static $table = "transaksionet";
		public static $timestamps = false;

		public function pagesat(){
			return $this->belongs_to("Pagesa","transaksionet");
		}
		public function faturat(){
			return $this->belongs_to("Fatura", "transaksionet");
		}
	}
?>