<?php
	class Fatura extends Eloquent{
		public static $timestamps = false;
		public static $table = "newfaturat";
		public static $per_page = 100;

		public function user(){
			return $this->belongs_to('User');
		}
		public function transaksion(){
			return $this->has_many("Transaksion", "fatura_id");
		}
	}
?>