<?php
	 class User extends Eloquent{
	 	public static $timestamps = false;
	 	public static $table = "rm_users";
	 	public static $per_page = 100;

	 	public function id(){
	 		return $this->has_one('Id', 'username');
	 	}

	 	public function fatura(){
	 		return $this->has_many('Fatura', 'username');
	 	}
	 	public function service(){
	 		return $this->belongs_to('Service');
	 	}
	 	public function pagesa(){
	 		return $this->has_many('Pagesa','username');
	 	}
	 }
?>