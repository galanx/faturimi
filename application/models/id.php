<?php
	class Id extends Eloquent{
		public static $timestamps = false;
		public static $table = "newidit";

		public function user(){
			return $this->belongs_to("User");
		}
	}  
?>