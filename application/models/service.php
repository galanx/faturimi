<?php
	class Service extends Eloquent{
		public static $table = "rm_services";

		public function user(){
			return $this->has_one("User", "srvid");
		}
	}
?>