<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Simply tell Laravel the HTTP verbs and URIs it should respond to. It is a
| breeze to setup your application using Laravel's RESTful routing and it
| is perfectly suited for building large applications and simple APIs.
|
| Let's respond to a simple GET request to http://example.com/hello:
|
|		Route::get('hello', function()
|		{
|			return 'Hello World!';
|		});
|
| You can even respond to more than one URI:
|
|		Route::post(array('hello', 'world'), function()
|		{
|			return 'Hello World!';
|		});
|
| It's easy to allow URI wildcards using (:num) or (:any):
|
|		Route::put('hello/(:any)', function($name)
|		{
|			return "Welcome, $name.";
|		});
|
*/

/*Route::get('/', function()
{
	return View::make('home.index');
});*/

/*
|--------------------------------------------------------------------------
| Application 404 & 500 Error Handlers
|--------------------------------------------------------------------------
|
| To centralize and simplify 404 handling, Laravel uses an awesome event
| system to retrieve the response. Feel free to modify this function to
| your tastes and the needs of your application.
|
| Similarly, we use an event to handle the display of 500 level errors
| within the application. These errors are fired when there is an
| uncaught exception thrown in the application.
|
*/
//Log in
Route::get('profiler', function(){
	echo View::make("profiler::display");
});

Route::group(array("before"=>"check"),function(){
	Route::get("login", array("as"=>"login", "uses"=>"negenet.login@index"));
	Route::post("login/check", array("uses"=>"negenet.login@authenticate"));
});
	

Route::group(array('before'=>'auth'), function(){
	//Home
	Route::get('/', array('as' => 'admin', 'uses'=>'negenet.admin@index'));

	//User transactions
	Route::get('user/(:any)', array('before'=>'klientet','as'=>'user_view','uses'=>'negenet.admin@user'));

	//User search results
	Route::any('search', array('as'=>'search', 'uses'=>'negenet.admin@search'));
	Route::any('searchSingle', array('as'=>'searchSingle', 'uses'=>'negenet.admin@searchSingle'));

	//Listimi i faturave
	Route::get('fatura', array('as'=>'fatura','uses'=>'negenet.faturat@index'));
	Route::post('fatura/search', array('uses'=>'negenet.faturat@search'));

	//Fatura 
	Route::group(array("before"=>"ndrysho_fature"),function(){
		Route::get('fatura/edit/(:any)', array('as'=>'fatura_edit','uses'=>'negenet.faturat@edit'));
		Route::post('fatura/edit', array('uses'=>'negenet.faturat@ndrysho'));
	});

	Route::group(array("before"=>"shto_fature"), function(){

		Route::get('fatura/shto/(:any)', array('as'=>'fatura_shto','uses'=>'negenet.faturat@shto'));
		Route::post('shto/fatura', array('uses'=>'negenet.faturat@shtofat'));
		Route::get('fatura/shto/klient/(:any)', array('as'=>'fatura_shto_klient','uses'=>'negenet.faturat@shtofatKlientView'));
		Route::post('shto/klient/fatura', array('uses'=>'negenet.faturat@shtofatKlient'));

	});



	//Shto pagesa
	Route::group(array("before"=>"shto_pagese"),function(){
		Route::get('pagesa/shto/(:any)', array('as'=>'pagesa','uses'=>'negenet.admin@pagesa'));
		Route::post('pagesa/shto', array('uses'=>'negenet.admin@shtopag'));
	});

	//Zbritja manuale
	Route::group(array("before"=>"zbritje"),function(){
		Route::get('zbritja/shto/(:any)', array('as'=>'zbritja','uses'=>'negenet.admin@zbritja'));
		Route::post('zbritja/shto', array('uses'=>'negenet.admin@shtozbr'));
	});

	//TV
	Route::group(array("before"=>"shiqo_tv"),function(){
		Route::get('tv', array('uses'=>'negenet.tv@tv'));
		Route::get('tv/view', array('as'=>'tv_index','uses'=>'negenet.tv@index'));
		Route::get('tv/kartela/(:any)', array("as"=>"kartela_edit","uses"=>"negenet.tv@kartela"));
		Route::post('tv/kartela/edit', array("uses"=>"negenet.tv@kartelaEdit"));
		Route::post('tv/search', array('uses'=>'negenet.tv@search'));
	});

	//Kaca
	Route::group(array("before"=>"shiqo_kaca"),function(){
		Route::get("kaca",array("as"=>"kaca","uses"=>"negenet.kaca@index"));
		Route::post("kaca/search",array("uses"=>"negenet.kaca@search"));
		Route::post("kaca/shto",array("uses"=>"negenet.kaca@shto"));
	});

	//Provider
	Route::get("provider", array('before'=>'shiqo_provider',"as"=>"provider", "uses" => "negenet.provider@provider"));

	//Skadimi
	Route::group(array("before"=>"ndrysho_skadimin"),function(){
		Route::get("skadimi/(:any)", array("as"=>"skadimi","uses"=>"negenet.skadimi@skadimi"));
		Route::post("edit/skadimi", array("uses"=>"negenet.skadimi@edit"));
	});

	//Pakot
	Route::group(array("before"=>"pakot"),function(){
		Route::get("pakot", array("as"=>"pakot","uses"=>"negenet.pakot@pakot"));
		Route::post("pakot/edit", array("uses"=>"negenet.pakot@edit"));
	});


	//Raportet
	Route::group(array("before"=>"shiqo_raportet"),function(){
		Route::get("raportet", array("as"=>"raportet", "uses"=>"negenet.raportet@raportet"));
		Route::post("raportet/check", array("uses"=>"negenet.raportet@check"));
		Route::post("raportet/pako", array("uses"=>"negenet.pagesat@sipasPakos"));

		//Listimi i pagesave
		Route::get('pagesat', array('as'=>'pagesat','uses'=>'negenet.pagesat@index'));
		Route::post('pagesat/search', array('uses'=>'negenet.pagesat@search'));
		Route::get('pagesat/pazari', array('as'=>'pazari','uses'=>'negenet.pagesat@pazari'));
		Route::post('pagesat/pazarisearch', array('uses'=>'negenet.pagesat@pazarisearch'));
	});

	//Historiku
	Route::get("historiku/(:any)", array("as"=>"historiku","uses"=>"negenet.historiku@historiku"));

	//Shitja
	Route::group(array("before"=>"shitja"),function(){
		Route::get("shitja",array("as"=>"shitja","uses"=>"negenet.shitja@index"));
		Route::get("shitja/shtoFature",array("as"=>"shitjashto_fat","uses"=>"negenet.shitja@shtoFature"));
		Route::get("shitja/shtoHyrje",array("as"=>"shitjashto_hyrje","uses"=>"negenet.shitja@shtoHyrje"));
		Route::get("shitja/shtoProdukt",array("as"=>"regjistroProdukt","uses"=>"negenet.shitja@regjistroProdukt"));
		Route::get("shitja/stokuGjendja",array("as"=>"stokuGjendja","uses"=>"negenet.shitja@stokuGjendja"));
		Route::post("shitja/kerkoMallin",array("uses"=>"negenet.shitja@kerkoMallin"));
		Route::post("shitja/shto/produkt", array("uses"=>"negenet.shitja@shtoProdukt"));
		Route::post("shitja/shto/fatura", array("uses"=>"negenet.shitja@shtoFatura"));
		Route::post("shitja/shto/hyrje", array("uses"=>"negenet.shitja@addHyrje"));
		Route::get("shitja/malli",array("as"=>"malli","uses"=>"negenet.shitja@hyrjet"));
		Route::post("shitja/fatura",array("uses"=>"negenet.shitja@faturat"));
		Route::post("shitja/hyrjet",array("uses"=>"negenet.shitja@hyrjetMallit"));
		Route::get("shitja/ndrysho/(:any)",array("as"=>"shitja_ndrysho","uses"=>"negenet.shitja@ndrysho"));
		Route::post("shitja/edit",array("uses"=>"negenet.shitja@edit"));
		Route::get("shitja/kode",array("as"=>"kode","uses"=>"negenet.shitja@kode"));
		Route::get("shitja/mbushje",array("as"=>"kode","uses"=>"negenet.skadimi@tvmbushje"));
		Route::post("shitja/shtokode",array("uses"=>"negenet.shitja@shtokode"));
		Route::post("shitja/fshijkode",array("uses"=>"negenet.shitja@fshijkode"));
		Route::post("shitja/dergombushje",array("uses"=>"negenet.skadimi@dergombushje"));	
	});

	//Print
	Route::group(array("before"=>"printo_fature"), function(){
		Route::get("fatura/internet/(:any)", array("as"=>"internet","uses"=>"negenet.print@internet"));
		Route::get("fatura/shitja/(:any)", array("as"=>"fat_shitja","uses"=>"negenet.print@shitja"));
		Route::get("fatura/shitjarregullt/(:any)", array("as"=>"fat_shitja","uses"=>"negenet.print@shitjerregullt"));
		Route::get("fatura/hyrjet/(:any)", array("as"=>"fat_hyrjet","uses"=>"negenet.print@hyrja"));
	});
		
	//Konto
	Route::group(array("before"=>"konto"),function(){
		Route::get('konto',array('as'=>'konto','uses'=>'negenet.konto@index'));
		Route::get('konto/role',array('as'=>'konto_role','uses'=>'negenet.konto@role'));
		Route::get('konto/ndrysho',array('as'=>'konto_ndrysho','uses'=>'negenet.konto@info'));
		Route::get('konto/shto',array('before'=>'shto_user','as'=>'konto_shto','uses'=>'negenet.konto@shto'));
		Route::get('konto/ndrysho/konto/(:any)',array('before'=>'ndrysho_user','as'=>'konto_ndrysho_konto','uses'=>'negenet.konto@ndryshoKonto'));
		Route::get('konto/shto/role',array('before'=>'shto_rol','as'=>'konto_shto_role','uses'=>'negenet.konto@shtoRole'));
		Route::get('konto/ndrysho/role/(:any)',array('before'=>'ndrysho_rol','as'=>'konto_ndrysho_role','uses'=>'negenet.konto@ndryshoRole'));
		Route::post('konto/info',array('uses'=>'negenet.konto@kontoInfo'));
		Route::post('konto/shto/konto',array('before'=>'shto_user','uses'=>'negenet.konto@kontoShto'));
		Route::post('konto/edit/konto',array('before'=>'ndrysho_user','uses'=>'negenet.konto@kontoEdit'));
		Route::post('konto/add/role',array('before'=>'shto_rol','uses'=>'negenet.konto@addRole'));
		Route::post('konto/edit/role',array('before'=>'ndrysho_rol','uses'=>'negenet.konto@editRole'));
		Route::post('konto/delete/konto',array('before'=>'fshij_user','uses'=>'negenet.konto@deleteKonto'));
		Route::post('konto/delete/roles',array('before'=>'fshij_rol','uses'=>'negenet.konto@deleteRole'));
	});
	
	//Log
	Route::get("log",array("before"=>"shiqo_log","as"=>"log","uses"=>"negenet.faturimilog@index"));
	Route::post("log/search",array("before"=>"shiqo_log","uses"=>"negenet.faturimilog@search"));
	

	Route::post("/ajax", function(){
		return View::make("negenet.shitja.ajax");
	});
	
	Route::get("pa-autorizim",array("as"=>"pa-autorizim","uses"=>"negenet.autorizimi@autorizimi"));

	Route::get("logout", array("as"=>"logout", "uses"=>"negenet.login@logout"));
});







Event::listen('404', function()
{
	return Response::error('404');
});

Event::listen('500', function()
{
	return Response::error('500');
});

/*
|--------------------------------------------------------------------------
| Route Filters
|--------------------------------------------------------------------------
|
| Filters provide a convenient method for attaching functionality to your
| routes. The built-in before and after filters are called before and
| after every request to your application, and you may even create
| other filters that can be attached to individual routes.
|
| Let's walk through an example...
|
| First, define a filter:
|
|		Route::filter('filter', function()
|		{
|			return 'Filtered!';
|		});
|
| Next, attach the filter to a route:
|
|		Route::get('/', array('before' => 'filter', function()
|		{
|			return 'Hello World!';
|		}));
|
*/



Route::filter('before', function()
{
	// Do stuff before every request to your application...
});

Route::filter('after', function($response)
{
	// Do stuff after every request to your application...
});

Route::filter('csrf', function()
{
	if (Request::forged()) return Response::error('500');
});

Route::filter('auth', function()
{
	if (Auth::guest()) return Redirect::to('login');
});

Route::filter('check', function()
{
	if (Auth::check()) return Redirect::to_route('admin');
});


Route::filter('klientet', function()
{	
	if(!Auth::guest()){
		$roli = Role::where("id","=",Auth::user()->role_id)->first();
	}else{
		return Redirect::to('login');
	}	
	if ($roli->shiqo_klient!=1) return Redirect::to_route("pa-autorizim");
});

Route::filter('shto_fature', function()
{	if(!Auth::guest()){
		$roli = Role::where("id","=",Auth::user()->role_id)->first();
	}else{
		return Redirect::to('login');
	}	
	if ($roli->shto_fature!=1) return Redirect::to_route("pa-autorizim");
});

Route::filter('ndrysho_fature', function()
{	if(!Auth::guest()){
		$roli = Role::where("id","=",Auth::user()->role_id)->first();
	}else{
		return Redirect::to('login');
	}
	if ($roli->ndrysho_fature!=1) return Redirect::to_route("pa-autorizim");
});

Route::filter('printo_fature', function()
{	if(!Auth::guest()){
		$roli = Role::where("id","=",Auth::user()->role_id)->first();
	}else{
		return Redirect::to('login');
	}
	if ($roli->printo_fature!=1) return Redirect::to_route("pa-autorizim");
});

Route::filter('shto_pagese', function()
{	if(!Auth::guest()){
		$roli = Role::where("id","=",Auth::user()->role_id)->first();
	}else{
		return Redirect::to('login');
	}
	if ($roli->shto_pagese!=1) return Redirect::to_route("pa-autorizim");
});

Route::filter('zbritje', function()
{	if(!Auth::guest()){
		$roli = Role::where("id","=",Auth::user()->role_id)->first();
	}else{
		return Redirect::to('login');
	}
	if ($roli->zbritje!=1) return Redirect::to_route("pa-autorizim");
});

Route::filter('shiqo_tv', function()
{	if(!Auth::guest()){
		$roli = Role::where("id","=",Auth::user()->role_id)->first();
	}else{
		return Redirect::to('login');
	}
	if ($roli->shiqo_tv!=1) return Redirect::to_route("pa-autorizim");
});

Route::filter('shiqo_kaca', function()
{	if(!Auth::guest()){
		$roli = Role::where("id","=",Auth::user()->role_id)->first();
	}else{
		return Redirect::to('login');
	}
	if ($roli->shiqo_kaca!=1) return Redirect::to_route("pa-autorizim");
});

Route::filter('shiqo_raportet', function()
{	if(!Auth::guest()){
		$roli = Role::where("id","=",Auth::user()->role_id)->first();
	}else{
		return Redirect::to('login');
	}
	if ($roli->shiqo_raportet!=1) return Redirect::to_route("pa-autorizim");
});

Route::filter('shiqo_log', function()
{	if(!Auth::guest()){
		$roli = Role::where("id","=",Auth::user()->role_id)->first();
	}else{
		return Redirect::to('login');
	}
	if ($roli->shiqo_log!=1) return Redirect::to_route("pa-autorizim");
});

Route::filter('shiqo_provider', function()
{	if(!Auth::guest()){
		$roli = Role::where("id","=",Auth::user()->role_id)->first();
	}else{
		return Redirect::to('login');
	}
	if ($roli->shiqo_provider!=1) return Redirect::to_route("pa-autorizim");
});

Route::filter('ndrysho_skadimin', function()
{	if(!Auth::guest()){
		$roli = Role::where("id","=",Auth::user()->role_id)->first();
	}else{
		return Redirect::to('login');
	}
	if ($roli->ndrysho_skadimin!=1) return Redirect::to_route("pa-autorizim");
});

Route::filter('pakot', function()
{	if(!Auth::guest()){
		$roli = Role::where("id","=",Auth::user()->role_id)->first();
	}else{
		return Redirect::to('login');
	}
	if ($roli->pakot!=1) return Redirect::to_route("pa-autorizim");
});

Route::filter('shitja', function()
{	if(!Auth::guest()){
		$roli = Role::where("id","=",Auth::user()->role_id)->first();
	}else{
		return Redirect::to('login');
	}
	if ($roli->shitja!=1) return Redirect::to_route("pa-autorizim");
});

Route::filter('konto', function()
{	if(!Auth::guest()){
		$roli = Role::where("id","=",Auth::user()->role_id)->first();
	}else{
		return Redirect::to('login');
	}
	if ($roli->konto!=1) return Redirect::to_route("pa-autorizim");
});

Route::filter('shto_user', function()
{	if(!Auth::guest()){
		$roli = Role::where("id","=",Auth::user()->role_id)->first();
	}else{
		return Redirect::to('login');
	}
	if ($roli->shto_user!=1) return Redirect::to_route("pa-autorizim");
});

Route::filter('shto_rol', function()
{	if(!Auth::guest()){
		$roli = Role::where("id","=",Auth::user()->role_id)->first();
	}else{
		return Redirect::to('login');
	}
	if ($roli->shto_rol!=1) return Redirect::to_route("pa-autorizim");
});

Route::filter('ndrysho_user', function()
{	if(!Auth::guest()){
		$roli = Role::where("id","=",Auth::user()->role_id)->first();
	}else{
		return Redirect::to('login');
	}
	if ($roli->ndrysho_user!=1) return Redirect::to_route("pa-autorizim");
});

Route::filter('ndrysho_rol', function()
{	if(!Auth::guest()){
		$roli = Role::where("id","=",Auth::user()->role_id)->first();
	}else{
		return Redirect::to('login');
	}
	if ($roli->ndrysho_rol!=1) return Redirect::to_route("pa-autorizim");
});

Route::filter('fshij_user', function()
{	if(!Auth::guest()){
		$roli = Role::where("id","=",Auth::user()->role_id)->first();
	}else{
		return Redirect::to('login');
	}
	if ($roli->fshij_user!=1) return Redirect::to_route("pa-autorizim");
});

Route::filter('fshij_rol', function()
{	if(!Auth::guest()){
		$roli = Role::where("id","=",Auth::user()->role_id)->first();
	}else{
		return Redirect::to('login');
	}
	if ($roli->fshij_rol!=1) return Redirect::to_route("pa-autorizim");
});