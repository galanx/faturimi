<div id="edit">
		<script type="text/javascript">
			function validate(evt) {
			  var theEvent = evt || window.event;
			  var key = theEvent.keyCode || theEvent.which;
			  key = String.fromCharCode( key );
			  var regex = /[0-9]|\./;
			  if( !regex.test(key) ) {
			    theEvent.returnValue = false;
			    if(theEvent.preventDefault) theEvent.preventDefault();
			  }
			}

			//Display te hyra
			function display_hyra(){
				if(document.getElementById("hyra").checked){
					document.getElementById("hyraVleraLabel").setAttribute("class","visible");
					document.getElementById("hyraVlera").setAttribute("class","visible");
					document.getElementById("furnizuesiLabel").setAttribute("class","visible");
					document.getElementById("furnizuesi").setAttribute("class","visible");
					document.getElementById("pershkrimiLabel").setAttribute("class","visible");
					document.getElementById("pershkrimi").setAttribute("class","visible");
				}else{
					document.getElementById("hyraVleraLabel").setAttribute("class","hidden");
					document.getElementById("hyraVlera").setAttribute("class","hidden");
					document.getElementById("furnizuesiLabel").setAttribute("class","hidden");
					document.getElementById("furnizuesi").setAttribute("class","hidden");
					document.getElementById("pershkrimiLabel").setAttribute("class","hidden");
					document.getElementById("pershkrimi").setAttribute("class","hidden");
				}
			}

				//Display te dala
			function display_dala(){
				if(document.getElementById("dala").checked){
					document.getElementById("dalaVleraLabel").setAttribute("class","visible");
					document.getElementById("dalaVlera").setAttribute("class","visible");
					document.getElementById("furnizuesiDalaLabel").setAttribute("class","visible");
					document.getElementById("furnizuesiDala").setAttribute("class","visible");
					document.getElementById("shpenzuesiLabel").setAttribute("class","visible");
					document.getElementById("shpenzuesi").setAttribute("class","visible");
					document.getElementById("pershkrimiDalaLabel").setAttribute("class","visible");
					document.getElementById("pershkrimiDala").setAttribute("class","visible");
					document.getElementById("faturaLabel").setAttribute("class","visible");
					document.getElementById("fatura").setAttribute("class","visible");
				}else{
					document.getElementById("dalaVleraLabel").setAttribute("class","hidden");
					document.getElementById("dalaVlera").setAttribute("class","hidden");
					document.getElementById("furnizuesiDalaLabel").setAttribute("class","hidden");
					document.getElementById("furnizuesiDala").setAttribute("class","hidden");
					document.getElementById("shpenzuesiLabel").setAttribute("class","hidden");
					document.getElementById("shpenzuesi").setAttribute("class","hidden");
					document.getElementById("pershkrimiDalaLabel").setAttribute("class","hidden");
					document.getElementById("pershkrimiDala").setAttribute("class","hidden");
					document.getElementById("faturaLabel").setAttribute("class","hidden");
					document.getElementById("fatura").setAttribute("class","hidden");
				}
			}
	</script>
	<?php $roli = Role::where("id","=",Auth::user()->role_id)->first(); ?>
	<p style="color:#FF6666">@if(Session::has('msg'))
		{{Session::get('msg')}}
	@endif<p><br><br>
	{{Form::open('kaca/shto', 'POST')}}
	@if($roli->shto_hyra==1)
	{{Form::label('hyra','Të hyra në kacë: ')}}<br>
	<input type="checkbox" name="hyra" id="hyra" onclick="javascript:display_hyra();"/>
	{{Form::label('hyraVlera','Vlera: ',array("class"=>"hidden", "id"=>"hyraVleraLabel"))}}<br>
	{{Form::text('hyraVlera', "",array('style'=>'width:100px;', 'onkeypress'=>'validate(event)', "class"=>"hidden", "id"=>"hyraVlera"))}}
	{{Form::label('furnizuesi','Furnizuesi: ',array("class"=>"hidden","id"=>"furnizuesiLabel"))}}<br>
	{{Form::text('furnizuesi','', array("class"=>"hidden", "id"=>"furnizuesi"))}}
	{{Form::label('pershkrimi','Përshkrimi: ',array("class"=>"hidden", "id"=>"pershkrimiLabel"))}}
	{{Form::textarea('pershkrimi','',array("class"=>"hidden", "id"=>"pershkrimi"))}}
	@endif

	@if($roli->shto_dala==1)
	{{Form::label('dala','Pagesa nga kaca: ')}}<br>
	<input type="checkbox" name="dala" id="dala" onclick="javascript:display_dala();"/>
	{{Form::label('dalaVlera','Vlera: ',array("class"=>"hidden", "id"=>"dalaVleraLabel"))}}<br>
	{{Form::text('dalaVlera', "",array('style'=>'width:100px;', 'onkeypress'=>'validate(event)', "class"=>"hidden", "id"=>"dalaVlera"))}}
	{{Form::label('fatura','Fatura: ',array("class"=>"hidden", "id"=>"faturaLabel"))}}<br>
	{{Form::text('fatura', "",array('style'=>'width:100px;', "class"=>"hidden", "id"=>"fatura"))}}
	{{Form::label('furnizuesiDala','Furnizuesi: ',array("class"=>"hidden","id"=>"furnizuesiDalaLabel"))}}<br>
	{{Form::text('furnizuesiDala','', array("class"=>"hidden", "id"=>"furnizuesiDala"))}}
	{{Form::label('shpenzuesi','Shpenzuesi: ',array("class"=>"hidden","id"=>"shpenzuesiLabel"))}}<br>
	{{Form::text('shpenzuesi','', array("class"=>"hidden", "id"=>"shpenzuesi"))}}
	{{Form::label('pershkrimiDala','Përshkrimi: ',array("class"=>"hidden", "id"=>"pershkrimiDalaLabel"))}}<br>
	{{Form::textarea('pershkrimiDala','',array("class"=>"hidden", "id"=>"pershkrimiDala"))}}
	@endif
<br>
	{{Form::image('/img/submit.png', '',array('style'=>'width:50px; height:30px;'))}}
	{{Form::close()}}<br><br>
	<p>Gjendja e kaces: {{$totali?number_format($totali,2):0.00}}&euro;</p>
</div>
					<div id="kerkoDaten">
						<form name="data" action="/kaca/search", method="post">
							<label>Prej: </label>
							<input name="prej" type="date"/>
							<label> Deri: </label>
							<input name="deri" type="date"/>
							<input type="image" src="/img/submit.png" id="submitButton"/>
						</form>
					</div>
	<table><caption><button id="back" onclick="javascript:history.go(-1);">&lt;&lt;Mbrapa</button> Te hyrat dhe te dalat e fundit.</caption>	
		<tr><th>User</th><th>Furnizuesi</th><th>Shpenzuesi</th><th>Të hyra në kacë</th><th>Pagesa nga kaca</th><th>Pershkrimi</th><th>Data</th><th>Fatura</th></tr>
	<?php $history = Kaca::order_by("id","desc")?Kaca::order_by("id","desc")->paginate()->results:Kaca::all();
			$pagination = Kaca::order_by("id","desc")?Kaca::order_by("id","desc")->paginate()->links():"";	
	?>	
	@foreach($history as $kaca)
		<tr><td>{{$kaca->user}}</td><td>{{$kaca->furnizuesi}}</td><td>{{$kaca->shpenzuesi}}</td><td>{{$kaca->te_hyra?number_format($kaca->te_hyra,2)."&euro;":""}}</td><td>{{$kaca->te_dala?number_format($kaca->te_dala,2)."&euro;":""}}</td><td>{{$kaca->pershkrimi}}</td><td>{{date("d/m/Y H:i:s",strtotime($kaca->data))}}</td><td>{{$kaca->fatura}}</td></tr>
	@endforeach
	</table>{{$pagination}}