<div id="kerkoDaten">
						<form name="data" action="/kaca/search", method="post">
							<label>Prej: </label>
							<input name="prej" type="date"/>
							<label> Deri: </label>
							<input name="deri" type="date"/>
							<input type="image" src="/img/submit.png" id="submitButton"/>
						</form>
					</div>
	<table><caption><button id="back" onclick="javascript:history.go(-1);">&lt;&lt;Mbrapa</button> Te hyrat dhe te dalat e sipas datës së kërkuar.</caption>	
		<tr><th>User</th><th>Furnizuesi</th><th>Shpenzuesi</th><th>Te hyra në kacë</th><th>Pagesa nga kaca</th><th>Pershkrimi</th><th>Data</th><th>Fatura</th></tr>
	<?php $history = Kaca::order_by("id","desc")?Kaca::order_by("id","desc")->paginate()->results:Kaca::all();
			$pagination = Kaca::order_by("id","desc")?Kaca::order_by("id","desc")->paginate()->links():"";

			$total_hyra = 0;
			$total_dala = 0;	
	?>	
	@foreach($result as $kaca)
		<tr><td>{{$kaca->user}}</td><td>{{$kaca->furnizuesi}}</td><td>{{$kaca->shpenzuesi}}</td><td>{{$kaca->te_hyra?number_format($kaca->te_hyra,2)."&euro;":""}}</td><td>{{$kaca->te_dala?number_format($kaca->te_dala,2)."&euro;":""}}</td><td>{{$kaca->pershkrimi}}</td><td>{{date("d/m/Y H:i:s",strtotime($kaca->data))}}</td><td>{{$kaca->fatura}}</td></tr>

		<?php $total_hyra += $kaca->te_hyra?$kaca->te_hyra:0; 
			$total_dala += $kaca->te_dala?$kaca->te_dala:0;
		?>
	@endforeach
	</table>

	<table style="width:350px;">
		<tr><th>Total të hyra në kacë</th><th>Total pagesa nga kaca</th></tr>
		<tr><td>{{number_format($total_hyra,2)}}&euro;</td><td>{{number_format($total_dala,2)}}&euro;</td></tr>
	</table>