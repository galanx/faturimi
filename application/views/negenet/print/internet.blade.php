<?php $fatura = Fatura::where("id","=",$id)->first(); 
	$user = User::where("username","=",$fatura->username)->first();
?>

<div class="internet" style="border-bottom:1px dashed black;">
	<h1 style="font-family:Copperplate Gothic Bold;">Deftesë Parapagimi</h1>
	<div class="fatura">
		<p>Nr. {{Id::where("username","=",$user->username)->first()->id." - ".$id}}</p>
		<p>Data e pageses: _______________________</p>
	</div>
	<div class="user-info">
		<p>Emri: {{$user->firstname." ".$user->lastname}}</p>
		<p>Perdoruesi: {{$fatura->username}}</p>
		<p>Tel: {{$user->mobile?$user->mobile:""}}</p>
		<p>Adresa: {{$user->address?$user->address:"- -"}}</p>
		<p>Pako: {{Service::where("srvid","=",$user->srvid)->first()?Service::where("srvid","=",$user->srvid)->first()->srvname:""}}</p>
	</div>
	<div class="tabela">
		<table>
			<tr>
				<th>Pershkrimi</th><th>Vlera</th>
				@if($fatura->zbritja>0)	
					<th>Zbritja</th>
				@endif		
				<th>Total</th>
			</tr>
			<tr>
				<td>{{$fatura->pershkrimi." ".date("d/m/Y",strtotime($fatura->prej))." - ".date("d/m/Y",strtotime($fatura->deri))}}</td>
				<td>{{number_format($fatura->vlera,2)}}&euro;</td>
				@if($fatura->zbritja>0)
					<td>{{number_format($fatura->zbritja,2)}}&euro;</td>
				@endif	
				<td>{{number_format($fatura->vlera-$fatura->zbritja,2)}}&euro;</td>
			</tr>

		</table>
		<p id="totali">Totali: {{number_format($fatura->vlera-$fatura->zbritja,2)}}&euro;</p>
		<p id="nenshkrimi">____________________</p>

		<div id="verejtje">
			<em style="font-size:0.8em;">Verejtje: Prishjet dhe pengesat ne internet duhet te lajmerohen ne 
				telefonat 290 326 666, 045 500 430 ose te paraqiten ne zyren tone. 
				Prishjet dhe pengesat e palajmeraura nuk do te merren parasysh ne zbritje eventuale. Me rrespekt.</em>
		</div>
	</div>
</div>

<!--2nd-->

<div class="internet" style="border-bottom:1px dashed black;">
	<h1 style="font-family:Copperplate Gothic Bold;">Deftesë Parapagimi</h1>
	<div class="fatura">
		<p>Nr. {{Id::where("username","=",$user->username)->first()->id." - ".$id}}</p>
		<p>Data e pageses: _______________________</p>
	</div>
	<div class="user-info">
		<p>Emri: {{$user->firstname." ".$user->lastname}}</p>
		<p>Perdoruesi: {{$fatura->username}}</p>
		<p>Tel: {{$user->mobile?$user->mobile:""}}</p>
		<p>Adresa: {{$user->address?$user->address:"- -"}}</p>
		<p>Pako: {{Service::where("srvid","=",$user->srvid)->first()?Service::where("srvid","=",$user->srvid)->first()->srvname:""}}</p>
	</div>
	<div class="tabela">
		<table>
			<tr>
				<th>Pershkrimi</th><th>Vlera</th>
				@if($fatura->zbritja>0)	
					<th>Zbritja</th>
				@endif		
				<th>Total</th>
			</tr>
			<tr>
				<td>{{$fatura->pershkrimi." ".date("d/m/Y",strtotime($fatura->prej))." - ".date("d/m/Y",strtotime($fatura->deri))}}</td>
				<td>{{number_format($fatura->vlera,2)}}&euro;</td>
				@if($fatura->zbritja>0)
					<td>{{number_format($fatura->zbritja,2)}}&euro;</td>
				@endif	
				<td>{{number_format($fatura->vlera-$fatura->zbritja,2)}}&euro;</td>
			</tr>

		</table>
		<p id="totali">Totali: {{number_format($fatura->vlera-$fatura->zbritja,2)}}&euro;</p>
		<p id="nenshkrimi">____________________</p>

		<div id="verejtje">
			<em style="font-size:0.8em;">Verejtje: Prishjet dhe pengesat ne internet duhet te lajmerohen ne 
				telefonat 290 326 666, 045 500 430 ose te paraqiten ne zyren tone. 
				Prishjet dhe pengesat e palajmeraura nuk do te merren parasysh ne zbritje eventuale. Me rrespekt.</em>
		</div>
	</div>
</div>
