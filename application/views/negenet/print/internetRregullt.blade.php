<?php $fatura = Fatura::where("id","=",$normal_id)->first(); 
	$user = User::where("username","=",$fatura->username)->first();

	$tv = Tv::where("fatura_id","=",$fatura->id)->first();

	$vlera_totale = $fatura->vlera-$fatura->zbritja;
	$perqindja = 13.79;
	$tvsh = ($perqindja/100)*$vlera_totale;
	$pa_tvsh = $vlera_totale-((13.79/100)*$vlera_totale);

?>
<div class="internet" style="border-bottom:1px dashed black;padding:0;">
	<img src="/img/negenet.png" style="display:block;margin-bottom:20px;width:200px;"/>
	
	<div class="fatura" style="float:left;margin-bottom:10px;">
		<p>Fatura Nr. {{$id}}</p>
		<p>Data: {{date("d/m/Y",strtotime($fatura->prej))}}</p>
	</div>


	<div id="prej-per" style="font-size:0.8em;clear:both;">
		<div class="user-info" style="float:right;padding-right:80px;">
			<p><b>Emri:</b> {{$user->company?$user->company:$user->firstname." ".$user->lastname}}</p>
			<p><b>Adresa:</b> {{$user->address?$user->address:"- -"}}</p>
			<p><b>Nr. Fis:</b> {{$user->taxid?$user->taxid:"- -"}}</p>
		</div>
		<b><p>NEGENET SH.P.K</p>
		<p>Nr.Fiskal: 600806970</p>
		<p>Nr.TVSH: 330165490</p></b>
		<p><b>Fax:</b> +381 (038) 220 440</p>
		<p><b>Cel:</b> +377 (045) 500 430</p>
		<p><b>Email:</b> sales@negenet.com</p>
		<p><b>Homepage:</b> www.negenet.com</p>
	</div>


	<div class="tabela">
		<table style="margin-top:35px;margin-bottom:10px;">
			<tr>
				<th>Pershkrimi</th>
				<th>Sasia</th>
				<th>Njësia</th>
				<th>Çmimi</th>
				@if($fatura->zbritja>0&&!strpos($fatura->pershkrimi, " & TV Kyqje."))	
					<th>Zbritja</th>
				@endif		
			</tr>
			@if(!strpos($fatura->pershkrimi, " & TV Kyqje."))	
			<tr>
				<td>{{$fatura->pershkrimi." ".date("d/m/Y",strtotime($fatura->prej))." - ".date("d/m/Y",strtotime($fatura->deri))}}</td>
				<td>1</td>
				<td>
					kohë
				</td>
				<td>{{number_format($fatura->vlera,2)}}&euro;</td>
				@if($fatura->zbritja>0&&!strpos($fatura->pershkrimi, " & TV Kyqje."))
					<td>{{number_format($fatura->zbritja,2)}}&euro;</td>
				@endif	
			</tr>
			@else
			<tr>
			<?php 
				$rpl = str_replace(" + Kartele ","",$fatura->pershkrimi);
				$rpl = str_replace(" + Dekoder ","",$rpl);
				$rpl = str_replace(" & TV Kyqje.","",$rpl);
			?>
				<td>{{$rpl." ".date("d/m/Y",strtotime($fatura->prej))." - ".date("d/m/Y",strtotime($fatura->deri))}}</td>
				<td>1</td>
				<td>
					kohë
				</td>
				<td>{{number_format(($fatura->vlera-$tv->cmimi_kartele-$tv->cmimi_dekoder-$tv->cmimi_instalimit),2)}}&euro;</td>	
			</tr>
			<tr>
			<?php 
				$pershkrimi = "TV kyqje ";
				if($tv->cmimi_kartele>0){
					$pershkrimi .="+ kartele ";
				}
				if($tv->cmimi_dekoder>0){
					$pershkrimi .="+ dekoder";
				}
			?>
				<td>{{$pershkrimi}}</td>
				<td>1</td>
				<td>
					copë
				</td>
				<td>{{number_format(($tv->cmimi_kartele+$tv->cmimi_dekoder+$tv->cmimi_instalimit),2)}}&euro;</td>
			</tr>
			@endif
		</table>
		<div id="llogaria" style="font-size:0.8em;float:left;">
			<p style="text-decoration:underline;font-weight:bold">Llogaria Bankare / Bank Account</p>
			<p><b>PCB &nbsp;&nbsp;1189002784000116</b></p>
			<p><b>RBKO 1501030001785652</b></p>
			<p><b>BEK &nbsp;&nbsp;&nbsp;1401000000394678</b></p>
		</div>
		<div id="calc" style="float:right;font-size:0.8em;text-align:right; padding-right:40px;">
			@if($fatura->zbritja>0&&strpos($fatura->pershkrimi, " & TV Kyqje."))
				<p>	
					Zbritja: {{number_format($fatura->zbritja,2)}}&euro;
				</p>
			@endif
			<p>Totali pa TVSH: {{number_format($pa_tvsh,2)}}&euro;</p>
			<p>TVSH: {{number_format($tvsh,2)}}&euro;</p>
		</div>
		<pre style="clear:both;">                                    V.V.</pre>
		<p id="totali" style="font-weight:normal;padding-right:30px;">Vlera për pagesë: <b>{{number_format($fatura->vlera-$fatura->zbritja,2)}}&euro;</b></p>
		<p id="nenshkrimi" style="">____________________</p>
	</div>
</div>

<!--2nd-->

<div class="internet" style="padding:0;">
	<img src="/img/negenet.png" style="display:block;margin-bottom:20px;width:200px;"/>
	
	<div class="fatura" style="float:left;margin-bottom:10px;">
		<p>Fatura Nr. {{$id}}</p>
		<p>Data: {{date("d/m/Y",strtotime($fatura->prej))}}</p>
	</div>


	<div id="prej-per" style="font-size:0.8em;clear:both;">
		<div class="user-info" style="float:right;padding-right:80px;">
			<p><b>Emri:</b> {{$user->company?$user->company:$user->firstname." ".$user->lastname}}</p>
			<p><b>Adresa:</b> {{$user->address?$user->address:"- -"}}</p>
			<p><b>Nr. Fis:</b> {{$user->taxid?$user->taxid:"- -"}}</p>
		</div>
		<b><p>NEGENET SH.P.K</p>
		<p>Nr.Fiskal: 600806970</p>
		<p>Nr.TVSH: 330165490</p></b>
		<p><b>Fax:</b> +381 (038) 220 440</p>
		<p><b>Cel:</b> +377 (045) 500 430</p>
		<p><b>Email:</b> sales@negenet.com</p>
		<p><b>Homepage:</b> www.negenet.com</p>
	</div>


	<div class="tabela">
		<table style="margin-top:35px;margin-bottom:10px;">
			<tr>
				<th>Pershkrimi</th>
				<th>Sasia</th>
				<th>Njësia</th>
				<th>Çmimi</th>
				@if($fatura->zbritja>0&&!strpos($fatura->pershkrimi, " & TV Kyqje."))	
					<th>Zbritja</th>
				@endif		
			</tr>
			@if(!strpos($fatura->pershkrimi, " & TV Kyqje."))	
			<tr>
				<td>{{$fatura->pershkrimi." ".date("d/m/Y",strtotime($fatura->prej))." - ".date("d/m/Y",strtotime($fatura->deri))}}</td>
				<td>1</td>
				<td>
					kohë
				</td>
				<td>{{number_format($fatura->vlera,2)}}&euro;</td>
				@if($fatura->zbritja>0&&!strpos($fatura->pershkrimi, " & TV Kyqje."))
					<td>{{number_format($fatura->zbritja,2)}}&euro;</td>
				@endif	
			</tr>
			@else
			<tr>
			<?php 
				$rpl = str_replace(" + Kartele ","",$fatura->pershkrimi);
				$rpl = str_replace(" + Dekoder ","",$rpl);
				$rpl = str_replace(" & TV Kyqje.","",$rpl);
			?>
				<td>{{$rpl." ".date("d/m/Y",strtotime($fatura->prej))." - ".date("d/m/Y",strtotime($fatura->deri))}}</td>
				<td>1</td>
				<td>
					kohë
				</td>
				<td>{{number_format(($fatura->vlera-$tv->cmimi_kartele-$tv->cmimi_dekoder-$tv->cmimi_instalimit),2)}}&euro;</td>
			</tr>
			<tr>
			<?php 
				$pershkrimi = "TV kyqje ";
				if($tv->cmimi_kartele>0){
					$pershkrimi .="+ kartele ";
				}
				if($tv->cmimi_dekoder>0){
					$pershkrimi .="+ dekoder";
				}
			?>
				<td>{{$pershkrimi}}</td>
				<td>1</td>
				<td>
					copë
				</td>
				<td>{{number_format(($tv->cmimi_kartele+$tv->cmimi_dekoder+$tv->cmimi_instalimit),2)}}&euro;</td>
			</tr>
			@endif
		</table>
		<div id="llogaria" style="font-size:0.8em;float:left;">
			<p style="text-decoration:underline;font-weight:bold">Llogaria Bankare / Bank Account</p>
			<p><b>PCB &nbsp;&nbsp;1189002784000116</b></p>
			<p><b>RBKO 1501030001785652</b></p>
			<p><b>BEK &nbsp;&nbsp;&nbsp;1401000000394678</b></p>
		</div>
		<div id="calc" style="float:right;font-size:0.8em;text-align:right; padding-right:40px;">
			@if($fatura->zbritja>0&&strpos($fatura->pershkrimi, " & TV Kyqje."))
				<p>	
					Zbritja: {{number_format($fatura->zbritja,2)}}&euro;
				</p>
			@endif
			<p>Totali pa TVSH: {{number_format($pa_tvsh,2)}}&euro;</p>
			<p>TVSH: {{number_format($tvsh,2)}}&euro;</p>
		</div>
		<pre style="clear:both;">                                    V.V.</pre>
		<p id="totali" style="font-weight:normal;padding-right:30px;">Vlera për pagesë: <b>{{number_format($fatura->vlera-$fatura->zbritja,2)}}&euro;</b></p>
		<p id="nenshkrimi" style="">____________________</p>
	</div>
</div>