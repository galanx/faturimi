
<div class="internet" style="border-bottom:1px dashed black;">
	<h1 style="font-family:Copperplate Gothic Bold;">Fletë Dergesë</h1>
	
	<div class="fatura" style="float:left;margin-bottom:10px;">
		<p>Nr. {{$fatura->id}}</p>
		<p>Data: {{date("d/m/Y",strtotime($fatura->data))}}</p>
	</div>

	<?php $produktet = MalliShitja::where("shitja_id","=",$fatura->id)->get(); 
		$shuma = 0;
	?>
	<div class="user-info" style="float:right;padding-right:80px;">
		<p><b>Për:</b> {{$fatura->bleresi}}</p>
	</div>
	<div class="tabela">
		<table>
			<tr><th>Produkti</th>
				<th>Pershkrimi</th>
				<th>Çmimi</th>
				<th>Sasia</th>	
				<th>Gjithsej</th>
			</tr>
			@foreach($produktet as $produkti)
			<tr><td>{{Produktet::where("id","=",$produkti->p_id)->first()->emri}}</td>
				<td>{{Produktet::where("id","=",$produkti->p_id)->first()->pershkrimi}}</td>
				<td>{{number_format($produkti->cmimi,2)}}&euro;</td>
				<td>{{$produkti->sasia}}</td>
				<td>{{number_format($produkti->sasia*$produkti->cmimi,2)}}&euro;</td>
			</tr>
			<?php $shuma += $produkti->sasia*$produkti->cmimi ?>
			@endforeach
		</table>
		<p id="totali" style="font-weight:normal;padding-right:30px;">Total: <b>{{number_format($shuma,2)}}&euro;</b></p>
		<p style="padding-left:270px;" id="nenshkrimi">Pranoi:____________________</p>
	</div>
</div>

<!--2nd-->


<div class="internet">
	<h1 style="font-family:Copperplate Gothic Bold;">Fletë Dergesë</h1>
	
	<div class="fatura" style="float:left;margin-bottom:10px;">
		<p>Nr. {{$fatura->id}}</p>
		<p>Data: {{date("d/m/Y",strtotime($fatura->data))}}</p>
	</div>

	<?php $produktet = MalliShitja::where("shitja_id","=",$fatura->id)->get(); 
		$shuma = 0;
	?>
	<div class="user-info" style="float:right;padding-right:80px;">
		<p><b>Për:</b> {{$fatura->bleresi}}</p>
	</div>
	<div class="tabela">
		<table>
			<tr><th>Produkti</th>
				<th>Pershkrimi</th>
				<th>Çmimi</th>
				<th>Sasia</th>	
				<th>Gjithsej</th>
			</tr>
			@foreach($produktet as $produkti)
			<tr><td>{{Produktet::where("id","=",$produkti->p_id)->first()->emri}}</td>
				<td>{{Produktet::where("id","=",$produkti->p_id)->first()->pershkrimi}}</td>
				<td>{{number_format($produkti->cmimi,2)}}&euro;</td>
				<td>{{$produkti->sasia}}</td>
				<td>{{number_format($produkti->sasia*$produkti->cmimi,2)}}&euro;</td>
			</tr>
			<?php $shuma += $produkti->sasia*$produkti->cmimi ?>
			@endforeach
		</table>
		<p id="totali" style="font-weight:normal;padding-right:30px;">Total: <b>{{number_format($shuma,2)}}&euro;</b></p>
		<p style="padding-left:270px;" id="nenshkrimi">Pranoi:____________________</p>
	</div>
</div>