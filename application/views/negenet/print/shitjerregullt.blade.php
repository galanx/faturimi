@for($i=0;$i<2;$i++)

<div class="internet" style="border-bottom:1px dashed black;">
	<img src="/img/negenet.png" style="display:block;margin-bottom:20px;width:200px;"/>

	<div id="prej-per" style="font-size:0.8em;float:left;margin-bottom:20px;">	

		<p>Nr. {{$fatura->id}}</p>
		<p>Data: {{date("d/m/Y",strtotime($fatura->data))}}</p>
		<br><br>
		<b><p>NEGENET SH.P.K</p>
		<p>Nr.Fiskal: 600806970</p>
		<p>Nr.TVSH: 330165490</p></b>
		<p><b>Fax:</b> +381 (038) 220 440</p>
		<p><b>Cel:</b> +377 (045) 500 430</p>
		<p><b>Email:</b> sales@negenet.com</p>
		<p><b>Homepage:</b> www.negenet.com</p>
	</div>
	<?php $produktet = MalliShitja::where("shitja_id","=",$fatura->id)->get(); 
		$shuma = 0;
	?>
	<div class="user-info" style="font-size:0.8em;float:right;padding-right:80px;">
		<p><b>Për:</b> {{$fatura->bleresi}}</p>
		<p><b>Nr.Fiskal:</b> {{$fatura->fiskal?$fatura->fiskal:"-"}}</p>
	</div>
	<div class="tabela">
		<table style="margin:25px auto;">
			<tr><th>Produkti</th>
				<th>Pershkrimi</th>
				<th>Çmimi</th>
				<th>Sasia</th>	
				<th>Gjithsej</th>
			</tr>
			@foreach($produktet as $produkti)
			<tr><td>{{Produktet::where("id","=",$produkti->p_id)->first()->emri}}</td>
				<td>{{Produktet::where("id","=",$produkti->p_id)->first()->pershkrimi}}</td>
				<td>{{number_format($produkti->cmimi,2)}}&euro;</td>
				<td>{{$produkti->sasia}}</td>
				<td>{{number_format($produkti->sasia*$produkti->cmimi,2)}}&euro;</td>
			</tr>
			<?php $shuma += $produkti->sasia*$produkti->cmimi ?>
			@endforeach
		</table>
<?php
	$vlera_totale = $shuma;
	$perqindja = 13.79;
	$tvsh = ($perqindja/100)*$shuma;
	$pa_tvsh = $shuma-((13.79/100)*$shuma);
?>		
		<div id="llogaria" style="font-size:0.8em;float:left;">
			<p style="text-decoration:underline;font-weight:bold">Llogaria Bankare / Bank Account</p>
			<p><b>PCB &nbsp;&nbsp;1189002784000116</b></p>
			<p><b>RBKO 1501030001785652</b></p>
			<p><b>BEK &nbsp;&nbsp;&nbsp;1401000000394678</b></p>
		</div>
		<pre>                               Totali pa TVSH: {{number_format($pa_tvsh,2)}}&euro;</pre>
		<pre>                               TVSH: {{number_format($tvsh,2)}}&euro;</pre>
		<p id="totali" style="font-weight:normal;padding-right:30px;">Vlera për pagesë: <b>{{number_format($shuma,2)}}&euro;</b></p>
		<p style="padding-left:190px;" id="nenshkrimi">Pranoi:____________________</p>
	</div>
</div>
@endfor