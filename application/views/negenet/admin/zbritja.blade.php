<div id="edit">
	<script type="text/javascript">
			function validate(evt) {
			  var theEvent = evt || window.event;
			  var key = theEvent.keyCode || theEvent.which;
			  key = String.fromCharCode( key );
			  var regex = /[0-9]|\./;
			  if( !regex.test(key) ) {
			    theEvent.returnValue = false;
			    if(theEvent.preventDefault) theEvent.preventDefault();
			  }
			}
	</script>
	<h1>{{HTML::link_to_route("user_view",$username,$username)}}</h1><br>
	<p style="color:#FF6666">@if(Session::has('msg'))
		{{Session::get('msg')}}
	@endif</p><br><br>
	{{Form::open('zbritja/shto','POST')}}
	{{Form::label('vlera','Vlera e zbritur: ')}}
	{{Form::text('vlera',"0",array('style'=>'width:100px;', 'onkeypress'=>'validate(event)'))}}<br><br>
	{{Form::label('fatura','ID e fatures: ')}}
	{{Form::text('fatura',"0",array('style'=>'width:100px;', 'onkeypress'=>'validate(event)'))}}<br><br>
	{{Form::label('koment','Arsyeja e zbritjes (e detyrueshme): ')}}
	{{Form::textarea('koment')}}<br>
	{{Form::image('/img/submit.png', '',array('style'=>'width:50px; height:30px;'))}}
	{{Form::hidden('user',$username)}}
	{{Form::close()}}
</div>