<div id="edit">
	<script type="text/javascript">
			function validate(evt) {
			  var theEvent = evt || window.event;
			  var key = theEvent.keyCode || theEvent.which;
			  key = String.fromCharCode( key );
			  var regex = /[0-9]|\./;
			  if( !regex.test(key) ) {
			    theEvent.returnValue = false;
			    if(theEvent.preventDefault) theEvent.preventDefault();
			  }
			}
	</script>
	<h1>{{HTML::link_to_route("user_view",$username,$username)}}</h1><br>
	<p style="color:#FF6666">@if(Session::has('msg'))
		{{Session::get('msg')}}
	@endif</p><br><br>
	{{Form::open('pagesa/shto','POST')}}
	{{Form::label('vlera','Shuma e paguar: ')}}
	{{Form::text('vlera',"",array('style'=>'width:100px;', 'onkeypress'=>'validate(event)'))}}<br><br>
	{{Form::label('fatura','ID e faturës: ')}}
	<select name="fatura">
		<option value="">--Fatura--</option>
		@foreach(Fatura::where("username","=",$username)->get() as $fatura)
			@if(($fatura->vlera-$fatura->zbritja)!=Pagesa::where("fatura_id","=",$fatura->id)->sum("vlera"))
				<option value="{{$fatura->id}}">{{$fatura->id}}</option>
			@endif
		@endforeach
	</select><br><br>
	{{Form::label('koment','Komento (opsionale): ')}}
	{{Form::textarea('koment')}}<br>
	{{Form::image('/img/submit.png', '',array('style'=>'width:50px; height:30px;'))}}
	{{Form::hidden('user',$username)}}
	{{Form::close()}}
</div>