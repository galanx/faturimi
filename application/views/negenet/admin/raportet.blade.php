<div id="edit">
	<?php date_default_timezone_set("Europe/Tirane") ?>
	<?php $value = Pagesa::where("data",">",date("Y-m-d H:i:s",strtotime("- 12 hour"))); 
		$shitja = Shitja::where("data",">",date("Y-m-d",strtotime("- 24 hour")))->where("paguar",">",0)->get();

		$shuma_e_shitjes = 0;

		foreach($shitja as $shitje){
			foreach(MalliShitja::where("shitja_id","=",$shitje->id)->get() as $malli){
				$shuma_e_shitjes += $malli->sasia*$malli->cmimi;
			}
		}
	?>
	<h2>{{HTML::link_to_route("pazari","Pazari ditorë:")}} {{number_format(($value->sum("vlera")?$value->sum("vlera"):0)+$shuma_e_shitjes,2)}}&euro;</h2>
	<br><br>
	@if(Session::has("msg"))
		<p style="color:#FF6666">{{Session::get("msg")}}</p><br><br>
	@endif
	{{Form::open('raportet/check','POST')}}
	<select name="checks">
		<option value="1">Pagesat</option>
		<option value="7">Pag. sipas pakos</option>
		<option value="2">Faturat</option>
		<option value="6">Faturat e Rregullta</option>
		<option value="3">TV</option>
		<option value="4">Kyqjet</option>
		<option value="5">Arkivat</option>
	</select><br>
	<br>
	{{Form::label("prej","Prej: ")}}
	{{Form::date("prej")}}<br>
	{{Form::label("deri","Deri: ")}}
	{{Form::date("deri")}}<br><br>
	{{Form::label("klienti","Klienti (opsional): ")}}
	{{Form::text("klienti")}}<br><br>
	{{Form::image('/img/submit.png', '',array('style'=>'width:50px; height:30px;'))}}
	{{Form::close()}}
</div>
