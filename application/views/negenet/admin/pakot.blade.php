	<script type="text/javascript">
			function validate(evt) {
			  var theEvent = evt || window.event;
			  var key = theEvent.keyCode || theEvent.which;
			  key = String.fromCharCode( key );
			  var regex = /[0-9]|\./;
			  if( !regex.test(key) ) {
			    theEvent.returnValue = false;
			    if(theEvent.preventDefault) theEvent.preventDefault();
			  }
			}
	</script>

<div id="edit">
	@if(Session::has("error"))
		<p style="color:#FF6666;">{{Session::get("error")}}</p>
	@endif
	@if(Session::has("msg"))
		<p>{{Session::get("msg")}}</p>
	@endif
	{{Form::open('pakot/edit','POST')}}
	<select name="pakot">
		<option value="">--Zgjidh Pakon--</option>
		@foreach($services as $servis)
			<option>{{$servis->srvname}}</option>
		@endforeach
	</select>	
	<select name="muaj">
		<option value="1">1 mujor</option>
		<option value="2">2 mujor</option>
		<option value="3">3 mujor</option>
		<option value="4">4 mujor</option>
		<option value="5">5 mujor</option>		
		<option value="6">6 mujor</option>
		<option value="7">7 mujor</option>
		<option value="8">8 mujor</option>
		<option value="9">9 mujor</option>
		<option value="10">10 mujor</option>
		<option value="11">11 mujor</option>
		<option value="12">12 mujor</option>
	</select>
	{{Form::text("cmimi","",array("onkeypress"=>"validate(event)","style"=>"width:60px;"))}}
	{{Form::image('/img/submit.png', '',array('style'=>'width:50px; height:30px; margin-bottom:-10px'))}}
	{{Form::close()}}
</div>

<table style="width:500px;">
	<caption><button id="back" onclick="javascript:history.go(-1);">&lt;&lt;Mbrapa</button> Pakot me çmime.</caption>
	<tr><th>ID</th><th>Emri</th><th>Cmimi</th>
	@foreach(Pako::all() as $pako)
		<tr>
		<td>{{$pako->id}}</td>
		<td>{{$pako->emri}}</td>
		<td>{{$pako->cmimi}}&euro;</td>
		</tr>
	@endforeach
</table>