	<div id="te-dhenat" class="{{$class}}">
		<?php 
		$totali = 0;
			foreach($result as $transaksioni){
				$vlera = Fatura::where("id","=",$transaksioni->fatura_id)->first()?Fatura::where("id","=",$transaksioni->fatura_id)->first()->vlera:0;
							
							$pagesa = Pagesa::where("id","=",$transaksioni->pagesa_id)->first()?Pagesa::where("id","=",$transaksioni->pagesa_id)->first()->vlera:0;

							$vlera -= Fatura::where("id","=",$transaksioni->fatura_id)->first()?
							Fatura::where("id","=",$transaksioni->fatura_id)->first()->zbritja:0;
							$gjendja = $pagesa - $vlera;
							$totali += $gjendja;
			}

			$roli = Role::where("id","=",Auth::user()->role_id)->first();
		?>
		<h2>{{$username}} ({{number_format($totali,2)}}&euro;)
			{{HTML::link_to_route("historiku","Historiku", $username, array("class"=>"paglink"))}}
			@if(Id::where("username","=",$username)->first()->provider==1)
				@if($roli->shto_fature==1)
					{{HTML::link_to_route("fatura_shto","Shto faturë",$username, array("class"=>"paglink"))}} 
				@endif
				@if($roli->shto_pagese==1)	
					{{HTML::link_to_route("pagesa","Shto pagesë",$username, array("class"=>"paglink"))}}
				@endif	
			@else
				@if($roli->shto_fature==1)
			 		{{HTML::link_to_route("fatura_shto_klient","Shto faturë",$username, array("class"=>"paglink"))}}
			 	@endif		 
			@endif
		</h2>
		<br>
		<div id="group-one">
			<p><span>Adresa: </span>{{$adresa}}</p>
			<p><span>Paketa: </span>{{$paketa}}</p>
			<p><span>IP: </span>{{$ip}}</p>
		</div>
		<div id="group-two">	
			<p><span>Tel: </span>{{$tel}}</p>
			<p><span>Kompania: </span>{{$kompania}}</p>
			@if($roli->ndrysho_skadimin==1)
			<p onclick="window.open('/skadimi/{{$username}}','Skadimi','height=270, width=240, left=600px, top=180px, location=no, resizable=no')"><span>Data e skadimit: </span>{{$skadimi}}</p>
			@else
			<p onclick='alert("Nuk jeni të autorizuar që ta ndryshoni skadimin!");'><span>Data e skadimit: </span>{{$skadimi}}</p>
			@endif
		</div>	
	</div>
		<table><input type="button" value="Refresh" onClick="javascript:history.go(0);" id="refresh"/>
					<tr><th>Nr. Tr</th><th>ID. Fat</th><th>Pershkrimi</th><th>Faturuar</th><th>Paguar</th><th>Gjendja</th><th>Zbritja</th><th>Data</th><th></th><th></th></tr>
					<?php $totali = 0; ?>
					@foreach($result as $transaksioni)
						<?php
							//Gjendja
							$vlera = Fatura::where("id","=",$transaksioni->fatura_id)->first()?Fatura::where("id","=",$transaksioni->fatura_id)->first()->vlera:0;
							$pagesa = Pagesa::where("id","=",$transaksioni->pagesa_id)->first()?Pagesa::where("id","=",$transaksioni->pagesa_id)->first()->vlera:0;

							$vlera -= Fatura::where("id","=",$transaksioni->fatura_id)->first()?
							Fatura::where("id","=",$transaksioni->fatura_id)->first()->zbritja:0;
							$gjendja = $pagesa - $vlera;
							$totali += $gjendja;

							//Pershkrimi prej-deri
							$pershkrimi_ft = Fatura::where("id","=",$transaksioni->fatura_id)->first()?
							Fatura::where("id","=",$transaksioni->fatura_id)->first()->pershkrimi:"";
							$pershkrimi_pg = Pagesa::where("id","=",$transaksioni->pagesa_id)->first()?
							Pagesa::where("id","=",$transaksioni->pagesa_id)->first()->pershkrimi." - "
							.Pagesa::where("id","=",$transaksioni->pagesa_id)->first()->fatura_id:"";

							$data_re = Fatura::where('id','=',$transaksioni->fatura_id)->first();
							if($data_re&&$data_re->data_re=="0000-00-00"){
								$prej = Fatura::where("id","=",$transaksioni->fatura_id)->first()?
								Fatura::where("id","=",$transaksioni->fatura_id)->first()->prej:"";
							}else{
								$prej = Fatura::where("id","=",$transaksioni->fatura_id)->first()?
								Fatura::where("id","=",$transaksioni->fatura_id)->first()->data_re:"";
							}	
								$deri = Fatura::where("id","=",$transaksioni->fatura_id)->first()?
								Fatura::where("id","=",$transaksioni->fatura_id)->first()->deri:"";

							//ID e transaksionit
							$transaksioni_id = $transaksioni->user_transaction_id?$transaksioni->user_transaction_id:"NULL";

							//ID e fatures
							$fatura_id = $transaksioni->fatura_id?$transaksioni->fatura_id:"";
							$fatura_rregullt = Fatura::where("id","=",$transaksioni->fatura_id)->first()?
							Fatura::where("id","=",$transaksioni->fatura_id)->first()->id_rregullt:"";

							//Vlera e faturuar
							$vlera_debit = Fatura::where("id","=",$transaksioni->fatura_id)->first()?
							number_format(Fatura::where("id","=",$transaksioni->fatura_id)->first()->vlera,2)."&euro;":"";

							//Vlera e paguar
							$vlera_kredit = Pagesa::where("id","=",$transaksioni->pagesa_id)->first()?
							number_format(Pagesa::where("id","=",$transaksioni->pagesa_id)->first()->vlera,2)."&euro;":"";
							
							//Data
							$data_pg = Pagesa::where("id","=",$transaksioni->pagesa_id)->first()?
							date("d-m-Y H:i:s",strtotime(Pagesa::where("id","=",$transaksioni->pagesa_id)->first()->data)):"";
							
							//if($data_re&&$data_re->data_krijimit=="0000-00-00"){
								$data_ft = Fatura::where("id","=",$transaksioni->fatura_id)->first()?
								date("d-m-Y",strtotime(Fatura::where("id","=",$transaksioni->fatura_id)->first()->prej)):"";
							/*}else{
								$data_ft = Fatura::where("id","=",$transaksioni->fatura_id)->first()?
								Fatura::where("id","=",$transaksioni->fatura_id)->first()->data_krijimit:"";
							}*/

							//Zbritja
							$zbritja = Fatura::where("id","=",$transaksioni->fatura_id)->first()?
							number_format(Fatura::where("id","=",$transaksioni->fatura_id)->first()->zbritja,2)."&euro;":"";

							if($zbritja==0){
								$zbritja = "";
							}

							//Koment
							$koment_ft = Fatura::where("id","=",$transaksioni->fatura_id)->first()?
							Fatura::where("id","=",$transaksioni->fatura_id)->first()->koment:"";
							$koment_pauze = Fatura::where("id","=",$transaksioni->fatura_id)->first()?
							Fatura::where("id","=",$transaksioni->fatura_id)->first()->pauzakoment:"";
							$koment_pr = Fatura::where("id","=",$transaksioni->fatura_id)->first()?
							Fatura::where("id","=",$transaksioni->fatura_id)->first()->providerkoment:"";
							$koment_pg = Pagesa::where("id","=",$transaksioni->pagesa_id)->first()?
							Pagesa::where("id","=",$transaksioni->pagesa_id)->first()->koment:"";

							$val = $fatura_rregullt>9?"":0;
						?>
						<tr>
							<td>{{$transaksioni_id}}</td>
							<td>
								@if(Fatura::where("id","=",$transaksioni->fatura_id)->where("rregullt","=",1)->first())
									@if($roli->ndrysho_fature==1)
										{{HTML::link_to_route('fatura_edit',$val.$fatura_rregullt."/".date("m",strtotime($data_ft)),$fatura_id)}}
									@else
										{{$val.$fatura_rregullt."/".date("m",strtotime($data_ft))}}	
									@endif	
								@else
									@if($roli->ndrysho_fature==1)
										{{HTML::link_to_route('fatura_edit',$fatura_id,$fatura_id)}}
									@else
										{{$fatura_id}}	
									@endif	
								@endif
							</td>
							<td>{{$pershkrimi_ft.$pershkrimi_pg}} 
								<small>
									{{$prej?date("d-m-Y",strtotime($prej)):""}} - {{$deri?date("d-m-Y",strtotime($deri)):""}}
								</small>
							</td>
							<td>{{$vlera_debit}}</td>
							<td>{{$vlera_kredit}}</td>
							<td>{{number_format($totali,2)}}&euro;</td>
							<td>{{$zbritja}}</td>
							<td>{{$data_pg.$data_ft}}</td>
							<td>@if($koment_ft!=""||$koment_pauze!=""||$koment_pg!=""||$koment_pr)
								<input type="image" src="/img/koment_active.png"  onclick="alert('{{$koment_ft." - ".$koment_pauze.$koment_pr.$koment_pg}}')" />
								@else
								<input type="image" src="/img/koment_passive.png" onclick="" />
								@endif
							</td>
							<td>
								@if($fatura_id)
									@if($roli->printo_fature==1)
									<a href="/fatura/internet/{{$fatura_id}}"><image style="width:20px;;height:20px;" src="/img/Print.png"/></a>
									@endif
								@endif
							</td>
						</tr> 
					@endforeach
						<tr id="totali"><th></th><th></th><th></th><th></th><th></th><th>Totali: {{number_format($totali,2)}}&euro;</th><th></th><th></th><th></th><th></th></tr>
					</table>