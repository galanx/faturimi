<!DOCTYPE html>
<html>
	<head>
		<title>Faturimi</title>
		{{HTML::style('css/style.css')}}
		{{HTML::script('js/jquery.js')}}
		{{HTML::script('js/menu-highlight.js')}}
		{{HTML::script('js/addform.js')}}
		<link rel="shortcut icon" href="<?php echo URL::to('favicon.ico'); ?>">
	</head>
	<body>
		<?php date_default_timezone_set("Europe/Tirane"); ?>
		
		<div id="wrapper">
			<?php $roli = Role::where("id","=",Auth::user()->role_id)->first(); ?>
			<div id="header">
				<a href="/">{{HTML::image('img/negenet.png', '', array('id'=>'logo'))}}</a>
				<ul id="menu-top">
					<li><a href="/" id="klientet">Klientët</a></li>
					@if($roli->shiqo_tv==1)
						<li><a href="/tv" id="tv">TV</a></li>
					@endif	
					@if($roli->shiqo_kaca==1)
						<li><a href="/kaca" id="kaca">Kaca</a></li>
					@endif
					@if($roli->shiqo_raportet==1)	
						<li><a href="/raportet" id="raportet">Raportet</a></li>
					@endif
					@if($roli->shiqo_log==1)	
						<li><a href="/log" id="log">Log</a></li>
					@endif
					@if($roli->shiqo_provider==1)	
						<li><a href="/provider" id="provider">Providerët</a></li>
					@endif
					@if($roli->pakot==1)	
						<li><a href="/pakot" id="pakot">Pakot</a></li>
					@endif
					@if($roli->shitja==1)	
						<li><a href="/shitja" id="shitja">Shitja</a></li>
					@endif
					@if($roli->konto==1)	
						<li><a href="/konto" id="konto">Konto</a></li>
					@endif	
				</ul>
				<div id="qkyqu">
					<a href="/logout">Qkyqu</a>
				</div>
			</div>
			@if(URI::current()=="/"||URI::current()=="search")
			<div id="search-box">
				<form name="search" method="post" action="/search" style="">
					<input type="text" name="search"/>
					<input type="image" src="/img/search.png"/>
					<select name="arkiva" onchange="document.search.submit();">
						<option value="-1">--Zgjidh--</option>
						<option value="1" class="ok">Aktiv</option>
						<option value="2" class="expired">Te skaduar</option>
						<option value="3" class="disabled">Jo Aktiv</option>
						<option value="0" class="arkiva">Arkivë</option>
					</select>
				</form>
			</div>
			@else
			<div id="search-box" style="width:200px;">
				<form name="search" style="padding-bottom:13px;" method="post" action="/searchSingle" >
					<input type="text" name="search"/>
					<input type="image" src="/img/search.png"/>
				</form>
			</div>
			@endif
			