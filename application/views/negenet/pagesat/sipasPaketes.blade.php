				<table>
					<caption><button id="back" onclick="javascript:history.go(-1);">&lt;&lt;Mbrapa</button> Listimi sipas pakove. {{$prej==""?"":"Raportet prej ".date("d-m-Y",strtotime($prej))}}{{$deri==""?"":" deri ".date("d-m-Y",strtotime($deri))}}</caption>
					<tr><th>Paketa</th><th>Çmimi</th><th>Faturuar</th><th>Fatura</th><th>Kyqje</th><th>TV</th><th>Te hyrat</th>
					</tr><?php 
							$faturuar = 0;
							$klienta = 0; 
							$kyqje = 0;
							$tv_total = 0;
							$hyrja_total = 0;
						?>
					@foreach($result as $pako)
					<?php $te_hyrat = 0; 
							$tv = 0;
							$tv_pag=0;

					?>
						<?php 
							if($prej!=""&&$deri==""){
								$faturat = Fatura::where("pako","=",$pako->emri)->where("data_krijimit",">=",$prej)->get();
								$fat = Fatura::where("pako","=",$pako->emri)->where("data_krijimit",">=",$prej);
							}
							if($deri!=""&&$prej==""){
								$faturat = Fatura::where("pako","=",$pako->emri)->where("data_krijimit","<=",$deri)->get();
								$fat = Fatura::where("pako","=",$pako->emri)->where("data_krijimit","<=",$deri);
							}
							if($deri!=""&&$prej!=""){
								$faturat = Fatura::where("pako","=",$pako->emri)->where("data_krijimit",">=",$prej)->where("data_krijimit","<=",$deri)->get();
								$fat = Fatura::where("pako","=",$pako->emri)->where("data_krijimit",">=",$prej)->where("data_krijimit","<=",$deri);
							}
							if($prej==""&&$deri==""){
								$faturat = Fatura::where("pako","=",$pako->emri)->get();
								$fat = Fatura::where("pako","=",$pako->emri);
							}
								
							foreach($faturat as $fatura){
								$te_hyrat += Pagesa::where("fatura_id","=",$fatura->id)->first()?Pagesa::where("fatura_id","=",$fatura->id)->sum("vlera"):0;
								if($fatura->tv==1){
									$tv++;
									$tv_query = Tv::where("fatura_id","=",$fatura->id)->first();
									if($tv_query){
										$tv_pag += $tv_query->cmimi_kartele+$tv_query->cmimi_dekoder+$tv_query->cmimi_instalimit+$tv_query->cmimi_mujor;
									}
								}
							}
						?>
						<tr>
							<td>{{$pako->emri}}</td>
							<td>{{$pako->cmimi>0?number_format($pako->cmimi,2)."&euro;":0}}</td>
							<td>{{$fat->sum("vlera")>0?number_format($fat->sum("vlera")-$fat->sum("zbritja"),2)."&euro;":0}}</td>
							<td>{{$fat->count("pako")}}</td>
							<td>{{$fat->where("kyqja",">",0)->count("pako")}}</td>
							<td>{{$tv}}</td>
							<td>{{$te_hyrat>0?number_format($te_hyrat,2)."&euro;":0}}</td>
							<?php 

							if($prej!=""&&$deri==""){
								$faturuar += Fatura::where("pako","=",$pako->emri)->where("data_krijimit",">=",$prej)->sum("vlera")-Fatura::where("pako","=",$pako->emri)->where("data_krijimit",">=",$prej)->sum("zbritja");
								$klienta +=  Fatura::where("pako","=",$pako->emri)->where("data_krijimit",">=",$prej)->count("pako");
								$kyqje += Fatura::where("pako","=",$pako->emri)->where("data_krijimit",">=",$prej)->where("kyqja",">",0)->count("pako");
							}
							if($deri!=""&&$prej==""){
								$faturuar += Fatura::where("pako","=",$pako->emri)->where("data_krijimit","<=",$deri)->sum("vlera")-Fatura::where("pako","=",$pako->emri)->where("data_krijimit","<=",$deri)->sum("zbritja");
								$klienta +=  Fatura::where("pako","=",$pako->emri)->where("data_krijimit","<=",$deri)->count("pako");
								$kyqje += Fatura::where("pako","=",$pako->emri)->where("data_krijimit","<=",$deri)->where("kyqja",">",0)->count("pako");
							}
							if($deri!=""&&$prej!=""){
								$faturuar += Fatura::where("pako","=",$pako->emri)->where("data_krijimit",">=",$prej)->where("data_krijimit","<=",$deri)->sum("vlera")-Fatura::where("pako","=",$pako->emri)->where("data_krijimit",">=",$prej)->where("data_krijimit","<=",$deri)->sum("zbritja");
								$klienta +=  Fatura::where("pako","=",$pako->emri)->where("data_krijimit",">=",$prej)->where("data_krijimit","<=",$deri)->count("pako");
								$kyqje += Fatura::where("pako","=",$pako->emri)->where("data_krijimit",">=",$prej)->where("data_krijimit","<=",$deri)->where("kyqja",">",0)->count("pako");
							}
							if($prej==""&&$deri==""){
								$faturuar += Fatura::where("pako","=",$pako->emri)->sum("vlera")-Fatura::where("pako","=",$pako->emri)->sum("zbritja");
								$klienta +=  Fatura::where("pako","=",$pako->emri)->count("pako");
								$kyqje += Fatura::where("pako","=",$pako->emri)->where("kyqja",">",0)->count("pako");
							}
								$tv_total += $tv;
								$hyrja_total += $te_hyrat;
							?> 
						</tr>
					@endforeach
					<tr><th></th><th></th><th>{{$faturuar>0?number_format($faturuar,2)."&euro;":0}}</th><th>{{$klienta}}</th><th>{{$kyqje}}</th><th>{{$tv_total}}</th><th>{{$hyrja_total>0?number_format($hyrja_total,2)."&euro;":0}}</th></tr>
					</table>
					<div id="kerkoDaten">
						<form name="data" action="/raportet/pako", method="post">
							<label>Prej: </label>
							<input name="data_krijimit" type="date"/>
							<label> Deri: </label>
							<input name="deri" type="date"/>
							<input type="image" src="/img/submit.png" id="submitButton"/>
						</form>
					</div>