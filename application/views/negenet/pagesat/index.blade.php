				<table>
					<caption><button id="back" onclick="javascript:history.go(-1);">&lt;&lt;Mbrapa</button> Listimi i të gjitha pagesave.</caption>
					<?php $shuma = 0;?>
					@foreach($result->results as $x)
						<?php $shuma += $x->vlera; ?>
					@endforeach
					<tr><th>ID</th><th>User</th><th>Data</th><th>Pershkrimi</th><th>Shuma ( {{$shuma}}&euro; )</th><th>Fatura</th>
						<th>Koment</th></tr>

					@foreach($result->results as $pagesa)
						<tr>
							<td>{{$pagesa->id}}</td>
							<td>{{$pagesa->username}}</td>
							<td>{{date("d-m-Y H:i:s",strtotime($pagesa->data))}}</td>
							<td>{{$pagesa->pershkrimi}}</td>
							<td>{{$pagesa->vlera}}&euro;</td>
							<td>{{$pagesa->fatura_id}}</td>
							<td>@if($pagesa->koment)
								<input type="image" src="/img/koment_active.png"  onclick="alert('{{$pagesa->koment}}')" />
								@else
								<input type="image" src="/img/koment_passive.png" onclick="" />
								@endif
							</td>
						</tr>
					@endforeach
					</table>
					{{$result->links()}}
					<div id="kerkoDaten">
						<form name="data" action="/pagesat/search", method="post">
							<label>Prej: </label>
							<input name="prej" type="date"/>
							<label> Deri: </label>
							<input name="deri" type="date"/>
							<input type="image" src="/img/submit.png" id="submitButton"/>
						</form>
					</div>