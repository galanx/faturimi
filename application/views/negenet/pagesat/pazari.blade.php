				<table>
					<caption><button id="back" onclick="javascript:history.go(-1);">&lt;&lt;Mbrapa</button> Pagesat ditore të internetit.</caption>
					<?php $shuma = 0;?>
					@foreach($result as $x)
						<?php $shuma += $x->vlera; ?>
					@endforeach
					<tr><th>Fat.</th><th>User</th><th>Pershkrimi</th><th>Koment</th><th>Shuma ( {{number_format($shuma,2)}}&euro; )</th><th>Data</th></tr>
					@foreach($result as $pagesa)
						<tr>
							<td>{{$pagesa->fatura_id}}</td>
							<td>{{$pagesa->username}}</td>
							<td>{{$pagesa->pershkrimi}}</td>
							<td>{{$pagesa->koment}}</td>
							<td>{{number_format($pagesa->vlera,2)}}&euro;</td>
							<td>{{date("d-m-Y H:i:s",strtotime($pagesa->data))}}</td>
						</tr>
					@endforeach
					</table>

				<table>
					<caption>Shitja Ditore.</caption>
					<?php $shuma_shitje = 0;?>
					@foreach($shitja as $x)
						@foreach(MalliShitja::where("shitja_id","=",$x->id)->get() as $malli)
							<?php $shuma_shitje += $malli->sasia*$malli->cmimi; ?>
						@endforeach
					@endforeach
					<tr><th>Fat.</th><th>Bleresi</th><th>Produkti</th><th>Pershkrimi</th><th>Koment</th><th>Sasia</th><th>Çmimi</th><th>Total ({{number_format($shuma_shitje,2)}}&euro;)</th><th>Data</th></tr>
					@foreach($shitja as $shitja)
						@foreach(MalliShitja::where("shitja_id","=",$shitja->id)->get() as $produkti)
							<tr>
								<td>{{$shitja->id}}</td>
								<td>{{$shitja->bleresi}}</td>
								<td>{{Produktet::where("id","=",$produkti->p_id)->first()->emri}}</td>
								<td>{{Produktet::where("id","=",$produkti->p_id)->first()->pershkrimi}}</td>
								<td>{{$shitja->koment}}</td>
								<td>{{$produkti->sasia}}</td>
								<td>{{number_format($produkti->cmimi,2)}}&euro;</td>
								<td>{{$produkti->sasia*$produkti->cmimi}}&euro;</td>
								
								<td>{{date("d-m-Y",strtotime($shitja->data))}}</td>
							</tr>
						@endforeach	
					@endforeach
					</table>
					<table style="width:200px;">
						<tr>
							<th>Cash internet</th>
							<th>Bank</th>
						</tr>
						<tr>
							<td>{{number_format($not_bank,2)}}&euro;</td>
							<td>{{number_format($bank,2)}}&euro;</td>
						</tr>
					</table>
					<table style="width:500px;">
						<tr>
							<th>Gjithsej internet</th>
							<th>Gjithsej shitja</th>
							<th>Total pazari ditorë</th>
						</tr>
						<tr>
							<td>{{number_format($shuma,2)}}&euro;</td>
							<td>{{number_format($shuma_shitje,2)}}&euro;</td>
							<td>{{number_format($shuma+$shuma_shitje,2)}}&euro;</td>
						</tr>
					</table>
					<div id="kerkoDaten">
						<form name="data" action="/pagesat/pazarisearch", method="post">
							<label>Prej: </label>
							<input name="prej" type="date"/>
							<label> Deri: </label>
							<input name="deri" type="date"/>
							<input type="image" src="/img/submit.png" id="submitButton"/>
						</form>
					</div>