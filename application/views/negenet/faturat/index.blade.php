				<?php $roli = Role::where("id","=",Auth::user()->role_id)->first(); 
					$faturuar = 0;
				?>
				<table>
					<caption><button id="back" onclick="javascript:history.go(-1);">&lt;&lt;Mbrapa</button> Listimi i të gjitha faturave.</caption>
					<tr><th>ID</th><th>User</th><th>Prej</th><th>Deri</th><th>Pershkrimi</th>
						<th>Vlera</th><th>Zbritja</th><th></th><th></th></tr>
					@foreach($result->results as $fatura)
						<tr><?php $val = $fatura->id_rregullt>9?"":0; ?>
							<td>
								@if($fatura->rregullt==1)
									@if($roli->ndrysho_fature==1)
										{{HTML::link_to_route('fatura_edit',$val.$fatura->id_rregullt."/".date("m",strtotime($fatura->prej)),$fatura->id)}}
									@else
										{{$val.$fatura->id_rregullt."/".date("m",strtotime($fatura->prej))}}
									@endif		
								@else
									@if($roli->ndrysho_fature==1)
										{{HTML::link_to_route('fatura_edit',$fatura->id,$fatura->id)}}
									@else
										{{$fatura->id}}
									@endif
								@endif
							</td>
							<td>{{$fatura->username}}</td>
							<td>{{date("d-m-Y",strtotime($fatura->prej))}}</td>
							<td>{{date("d-m-Y",strtotime($fatura->deri))}}</td>
							<td>{{$fatura->pershkrimi}}</td>
							<td>{{$fatura->vlera}}&euro;</td>
							<td>{{$fatura->zbritja?$fatura->zbritja."&euro;":""}}</td>
							<td>@if($fatura->koment||$fatura->pauzakoment||$fatura=>providerkoment)
								<input type="image" src="/img/koment_active.png"  onclick="alert('{{$fatura->koment.$fatura->providerkoment." - ".$fatura->pauzakoment}}')" />
								@else
								<input type="image" src="/img/koment_passive.png" onclick="" />
								@endif
							</td>
							<td>
								@if($fatura->id)
									@if($roli->printo_fature==1)
									<a href="/fatura/internet/{{$fatura->id}}"><image style="width:20px;;height:20px;" src="/img/Print.png"/></a>
									@endif
								@endif
							</td>
						</tr>
						<?php $faturuar += ($fatura->vlera - $fatura->zbritja) ?>
					@endforeach
					<th></th><th></th><th></th><th></th><th></th><th>Gjithsej {{$faturuar}}&euro;<th></th></th><th></th><th></th>
					</table>
					{{$result->links()}}
					<div id="kerkoDaten">
						<form name="data" action="fatura/search", method="post">
							<label>Prej: </label>
							<input name="prej" type="date"/>
							<label> Deri: </label>
							<input name="deri" type="date"/>
							<input type="image" src="/img/submit.png" id="submitButton"/>
						</form>
					</div>