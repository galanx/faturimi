<?php $roli = Role::where("id","=",Auth::user()->role_id)->first(); ?>
<div id="edit">
	<script type="text/javascript">
			//No characters allowed
			function validate(evt) {
			  var theEvent = evt || window.event;
			  var key = theEvent.keyCode || theEvent.which;
			  key = String.fromCharCode( key );
			  var regex = /[0-9]|\./;
			  if( !regex.test(key) ) {
			    theEvent.returnValue = false;
			    if(theEvent.preventDefault) theEvent.preventDefault();
			  }
			}

			//Nese zbritja checked
			function display_zbritja(){
				if(document.getElementById("zbritja").checked){
					document.getElementById("vleraLabel").setAttribute("class","visible");
					document.getElementById("vlera").setAttribute("class","visible");
					document.getElementById("zbrKomentLabel").setAttribute("class","visible");
					document.getElementById("zbrKoment").setAttribute("class","visible");
				}else{
					document.getElementById("vleraLabel").setAttribute("class","hidden");
					document.getElementById("vlera").setAttribute("class","hidden");
					document.getElementById("zbrKomentLabel").setAttribute("class","hidden");
					document.getElementById("zbrKoment").setAttribute("class","hidden");
				}
			}

			//Nese Pagesa checked
			function display_pagesa(){
				if(document.getElementById("pagesa").checked){
					document.getElementById("shumaLabel").setAttribute("class","visible");
					document.getElementById("shuma").setAttribute("class","visible");
					document.getElementById("pagKomentLabel").setAttribute("class","visible");
					document.getElementById("pagKoment").setAttribute("class","visible");
				}else{
					document.getElementById("shumaLabel").setAttribute("class","hidden");
					document.getElementById("shuma").setAttribute("class","hidden");
					document.getElementById("pagKomentLabel").setAttribute("class","hidden");
					document.getElementById("pagKoment").setAttribute("class","hidden");
				}
			}

			//Nese TV checked
			function display_tv(){
				if(document.getElementById("trio").checked){
					document.getElementById("abonimLabel").setAttribute("class","visible");
					document.getElementById("muaj").setAttribute("class","visible");
					document.getElementById("kyqLabel").setAttribute("class","visible");
					document.getElementById("tvKyqja").setAttribute("class","visible");
					document.getElementById("shifraKLabel").setAttribute("class","visible");
					document.getElementById("shifra").setAttribute("class","visible");
					document.getElementById("cmimiKLabel").setAttribute("class","visible");
					document.getElementById("cmimiK").setAttribute("class","visible");
					document.getElementById("cmimiDLabel").setAttribute("class","visible");
					document.getElementById("cmimiD").setAttribute("class","visible");
					document.getElementById("cmimiILabel").setAttribute("class","visible");
					document.getElementById("cmimiI").setAttribute("class","visible");
				}else{
					document.getElementById("abonimLabel").setAttribute("class","hidden");
					document.getElementById("muaj").setAttribute("class","hidden");
					document.getElementById("kyqLabel").setAttribute("class","hidden");
					document.getElementById("tvKyqja").setAttribute("class","hidden");
					document.getElementById("shifraKLabel").setAttribute("class","hidden");
					document.getElementById("shifra").setAttribute("class","hidden");
					document.getElementById("cmimiKLabel").setAttribute("class","hidden");
					document.getElementById("cmimiK").setAttribute("class","hidden");
					document.getElementById("cmimiDLabel").setAttribute("class","hidden");
					document.getElementById("cmimiD").setAttribute("class","hidden");
					document.getElementById("cmimiILabel").setAttribute("class","hidden");
					document.getElementById("cmimiI").setAttribute("class","hidden");
				}
			}

				//Nese TV checked
			function enable_tvKyqja(){
				if(document.getElementById("tvKyqja").checked){
					document.getElementById("shifra").removeAttribute("disabled");
					document.getElementById("cmimiK").removeAttribute("disabled");
					document.getElementById("cmimiD").removeAttribute("disabled");
					document.getElementById("cmimiI").removeAttribute("disabled");	
				}else{
					document.getElementById("shifra").setAttribute("disabled",true);
					document.getElementById("cmimiK").setAttribute("disabled",true);
					document.getElementById("cmimiD").setAttribute("disabled",true);
					document.getElementById("cmimiI").setAttribute("disabled",true);
				}
			}

			function display_kyqje(){
				if(document.getElementById("kyqje").checked){
					document.getElementById("cmimiKyqjesLabel").setAttribute("class","visible");
					document.getElementById("cmimiKyqjes").setAttribute("class","visible");
					
				}else{
					document.getElementById("cmimiKyqjesLabel").setAttribute("class","hidden");
					document.getElementById("cmimiKyqjes").setAttribute("class","hidden");
				}
			}
			function display_pauze(){
				if(document.getElementById("pauze").checked){
					document.getElementById("ditLabel").setAttribute("class","visible");
					document.getElementById("dite").setAttribute("class","visible");
					document.getElementById("pauzakomentLabel").setAttribute("class","visible");
					document.getElementById("pauzakoment").setAttribute("class","visible");
				}else{
					document.getElementById("ditLabel").setAttribute("class","hidden");
					document.getElementById("dite").setAttribute("class","hidden");
					document.getElementById("pauzakomentLabel").setAttribute("class","hidden");
					document.getElementById("pauzakoment").setAttribute("class","hidden");
				}
			}

	</script>
	<?php $srvid = User::where("username","=",$fatura->username)->first()->srvid; 		
		$tv = Tv::where("fatura_id","=",$fatura->id)->first();
		$shifra = Kartela::where("username","=",$fatura->username)->first();
		$val = $fatura->id_rregullt>9?"":0;
	?>
	<h1>{{HTML::link_to_route("user_view",$fatura->username,$fatura->username)}} - 
		@if($fatura->rregullt==1)
		{{$val.$fatura->id_rregullt."/".date("m",strtotime($fatura->prej))}}
		@else
		{{$fatura->id}}
		@endif
	</h1><br>
	<h3>Vlera e faturës: {{$fatura->vlera}}</h3>
	<p style="color:#FF6666">@if(Session::has('msg'))
		{{Session::get('msg')}}
	@endif<p><br><br>

	
	{{Form::open('fatura/edit','POST')}}


<!--NESE FATURA E FUNDIT-->
@if($fatura->id==Fatura::where("username","=",$fatura->username)->max('id'))
	<!--Internet-->
	<p><label>Kyqje</label>
		@if($fatura->kyqja==0)
		<input name="kyqje" id="kyqje" type="checkbox" onclick="javascript:display_kyqje();"/>
		@else
		<input name="kyqje" id="kyqje" type="checkbox" onclick="javascript:display_kyqje();" checked/>
		@endif
	</p>
	{{Form::label('cmimiKyqjes','Çmimi i Kyqjes: ', array('class'=>'hidden', 'id'=>'cmimiKyqjesLabel'))}}
	{{Form::text('cmimiKyqjes',$fatura->kyqja==0?'':$fatura->kyqja,array('style'=>'width:100px;', 'onkeypress'=>'validate(event)','class'=>'hidden','id'=>'cmimiKyqjes'))}}<br><br>
	
	@if(Id::where("username","=",$fatura->username)->first()->provider!=1)
	<!--Pauza-->
	<p><label>Pauzë</label>
		@if($fatura->pauza==0)
		<input name ="pauze", id="pauze" type="checkbox" onclick="javascript:display_pauze();"/>
		@else
		<input name ="pauze", id="pauze" type="checkbox" onclick="javascript:display_pauze();" checked/>
		@endif
	</p>
	{{Form::label('dit','Data e re: ',array('class'=>'hidden', 'id'=>'ditLabel'))}}
	<p>{{Form::date('dite',$fatura->pauza==0?'':$fatura->data_re,array('class'=>'hidden','id'=>'dite'))}}</p>

	{{Form::label('pauzakoment','Komento pauzen: ',array('class'=>'hidden', 'id'=>'pauzakomentLabel'))}}
	<p>{{Form::textarea('pauzakoment',$fatura->pauzakoment?$fatura->pauzakoment:'',array('class'=>'hidden', 'id'=>'pauzakoment', 'style'=>'width:200px;height:50px;margin-bottom:20px;'))}}</p>

	<select name="pako", id="pako">
		<option selected>{{$fatura->pako}}</option>
		@foreach(Pako::where("emri" ,"!=", $fatura->pako)->get() as $pako)
			<option>{{$pako->emri}}</option>
		@endforeach
	</select><br><br>	
	<!--{{Form::label('periudha','Periudha: ')}}
	{{Form::text("periudha",$fatura->periudha, array('style'=>'width:50px;','onkeypress'=>'validate(event)'))}}Muaj<br><br>-->
	@else
	{{Form::label('prej','Prej: ')}}<br>
	{{Form::date('prej',$fatura->prej)}}<br><br>
	{{Form::label('deri','Deri: ')}}<br>
	{{Form::date('deri',$fatura->deri)}}<br><br>
	{{Form::label('pershkrimi','Pershkrimi: ')}}<br>
	{{Form::textarea('pershkrimi',$fatura->pershkrimi,array('style'=>'width:300px;height:100px;'))}}<br><br>
	{{Form::label('cmimiProv','Vlera: ')}}<br>
	{{Form::text('cmimiProv',$fatura->vlera,array('style'=>'width:100px;','onkeypress'=>'validate(event)', 'id'=>'cmimiProv'))}}<br><br>
	@endif

	<!--TV-->
	<p><label>TV</label>
		@if($fatura->tv==0)
		<input name="tv" id="trio" type="checkbox" onclick="javascript:display_tv();"/>
		@else
		<input name="tv" id="trio" type="checkbox" onclick="javascript:display_tv();" checked/>
		@endif
	</p>
	<?php $kodi = Kode::first()?Kode::first()->kodi:"";?>
	<?php $kode_history = Tv::where("fatura_id","=",$fatura->id)->first()?Tv::where("fatura_id","=",$fatura->id)->first()->kodi:$kodi; ?>
	{{Form::label('abonim','Mbushja: ',array('class'=>'hidden', 'id'=>'abonimLabel'))}}
	{{Form::text('mbushja',$kode_history,array('style'=>'width:170px;','class'=>'hidden','id'=>'muaj'))}}
	<p><label class="hidden" id="kyqLabel">Kyqje</label>
		@if($fatura->tv==1&&($tv->cmimi_karteles>0||$tv->cmimi_dekoder>0||$tv->cmimi_instalimit>0))
		<input name="tvKyqja" id="tvKyqja" type="checkbox" onclick="javascript:enable_tvKyqja();" class="hidden" checked/>
		{{Form::label('shifraKartela','Shifra e Karteles: ',array('class'=>'hidden', 'id'=>'shifraKLabel'))}}
		<p>{{Form::text('shifra',$shifra?$shifra->kartela:"",array('style'=>'width:150px;','class'=>'hidden','id'=>'shifra'))}}</p>
		{{Form::label('cmimiKartela','Çmimi i Karteles: ',array('class'=>'hidden', 'id'=>'cmimiKLabel'))}}
		<p>{{Form::text('cmimiK',$tv?$tv->cmimi_kartele:"",array('style'=>'width:100px;', 'onkeypress'=>'validate(event)','class'=>'hidden','id'=>'cmimiK'))}}</p>
		{{Form::label('cmimiDekoder','Çmimi i Dekoderit: ',array('class'=>'hidden', 'id'=>'cmimiDLabel'))}}
		<p>{{Form::text('cmimiD',$tv?$tv->cmimi_dekoder:"",array('style'=>'width:100px;', 'onkeypress'=>'validate(event)','class'=>'hidden','id'=>'cmimiD'))}}</p>
		{{Form::label('cmimiInstalimit','Çmimi i Instalimit: ',array('class'=>'hidden', 'id'=>'cmimiILabel'))}}
		<p>{{Form::text('cmimiI',$tv?$tv->cmimi_instalimit:"",array('style'=>'width:100px;', 'onkeypress'=>'validate(event)','class'=>'hidden','id'=>'cmimiI'))}}</p>
		@else
		<input name="tvKyqja" id="tvKyqja" type="checkbox" onclick="javascript:enable_tvKyqja();" class="hidden"/>
		{{Form::label('shifraKartela','Shifra e Karteles: ',array('class'=>'hidden', 'id'=>'shifraKLabel'))}}
		<p>{{Form::text('shifra',$shifra?$shifra->kartela:"",array('style'=>'width:150px;','class'=>'hidden','id'=>'shifra','disabled'))}}</p>
		{{Form::label('cmimiKartela','Çmimi i Karteles: ',array('class'=>'hidden', 'id'=>'cmimiKLabel'))}}
		<p>{{Form::text('cmimiK',$tv?$tv->cmimi_kartele:"",array('style'=>'width:100px;', 'onkeypress'=>'validate(event)','class'=>'hidden','id'=>'cmimiK', 'disabled'))}}</p>
		{{Form::label('cmimiDekoder','Çmimi i Dekoderit: ',array('class'=>'hidden', 'id'=>'cmimiDLabel'))}}
		<p>{{Form::text('cmimiD',$tv?$tv->cmimi_dekoder:"",array('style'=>'width:100px;', 'onkeypress'=>'validate(event)','class'=>'hidden','id'=>'cmimiD', 'disabled'))}}</p>
		{{Form::label('cmimiInstalimit','Çmimi i Instalimit: ',array('class'=>'hidden', 'id'=>'cmimiILabel'))}}
		<p>{{Form::text('cmimiI',$tv?$tv->cmimi_instalimit:"",array('style'=>'width:100px;', 'onkeypress'=>'validate(event)','class'=>'hidden','id'=>'cmimiI', 'disabled'))}}</p>
		@endif
	</p>

	@if($roli->zbritje==1&&Id::where("username","=",$fatura->username)->first()->provider==0)
		<!--Zbritja-->
		<p><label>Zbritje</label>
			@if($fatura->zbritja==0)
			<input name ="zbritja", id="zbritja" type="checkbox" onclick="javascript:display_zbritja();"/>
			@else
			<input name ="zbritja", id="zbritja" type="checkbox" onclick="javascript:display_zbritja();" checked/>
			@endif
		</p>
		{{Form::label('vlera','Vlera e zbritur: ',array('class'=>'hidden', 'id'=>'vleraLabel'))}}
		<p>{{Form::text('vlera',$fatura->zbritja==0?"":$fatura->zbritja,array('style'=>'width:100px;', 'onkeypress'=>'validate(event)','class'=>'hidden','id'=>'vlera'))}}</p>
		{{Form::label('koment','Arsyeja e zbritjes (e detyrueshme): ',array('class'=>'hidden', 'id'=>'zbrKomentLabel'))}}
		<p>{{Form::textarea('koment',$fatura->koment?$fatura->koment:'',array('class'=>'hidden', 'id'=>'zbrKoment', 'style'=>'width:200px;height:50px;margin-bottom:20px;'))}}</p>
	@endif

	<!--Rregullt-->
	<p><label>E rregullt</label>
		@if($fatura->rregullt==0)
		<input name ="rregullt", id="rregullt" type="checkbox"/>
		@else
		<input name ="rregullt", id="rregullt" type="checkbox" checked/>
		@endif
	</p>
@endif
<!--END FATURA E FUNDIT-->
	@if(Id::where("username","=",$fatura->username)->first()->provider==0)
		@if($roli->shto_pagese==1)
		<!--Pagesa-->
		<p><label>Pagesë</label><input name ="pagesa", id="pagesa" type="checkbox" onclick="javascript:display_pagesa();"/></p>
		{{Form::label('shuma','Shuma e paguar: ',array('class'=>'hidden', 'id'=>'shumaLabel'))}}
		<p>{{Form::text('shuma',"",array('style'=>'width:100px;', 'onkeypress'=>'validate(event)','class'=>'hidden','id'=>'shuma'))}}</p>
		{{Form::label('pagKoment','Koment (opsionale): ',array('class'=>'hidden', 'id'=>'pagKomentLabel'))}}
		<p>{{Form::textarea('pagKoment','',array('class'=>'hidden', 'id'=>'pagKoment'))}}</p>
		@endif
	@else
	{{Form::label('komentp','Koment: ')}}<br>
	{{Form::textarea('komentp',$fatura->providerkoment,array('style'=>'width:300px;height:100px;'))}}<br><br>	
	@endif
	{{Form::hidden('id',$fatura->id)}}
	{{Form::image('/img/submit.png', '',array('style'=>'width:50px; height:30px;'))}}	
	{{Form::close()}}	
</div>		
