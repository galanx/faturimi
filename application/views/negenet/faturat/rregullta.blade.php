		<?php $roli = Role::where("id","=",Auth::user()->role_id)->first(); 

		?>
		<table><caption><button id="back" onclick="javascript:history.go(-1);">&lt;&lt;Mbrapa</button> Faturat e kerkuara.</caption>
					<tr><th>ID</th><th>User</th><th>Prej</th><th>Deri</th><th>Pershkrimi</th><th>Vlera</th><th>Zbritja</th><th></th><th></th></tr>
					<?php $total = 0; 
						$fat_tvsh = 0;
						$fat_pa_tvsh = 0;
					?>
					@foreach($result as $fatura)
						<tr><?php $val = $fatura->id_rregullt>9?"":0; ?>
							<td>
								@if($fatura->rregullt==1)
									@if($roli->ndrysho_fature==1)
										{{HTML::link_to_route('fatura_edit',$val.$fatura->id_rregullt."/".date("m",strtotime($fatura->prej)),$fatura->id)}}
									@else
										{{$val.$fatura->id_rregullt."/".date("m",strtotime($fatura->prej))}}
									@endif		
								@else
									@if($roli->ndrysho_fature==1)
										{{HTML::link_to_route('fatura_edit',$fatura->id,$fatura->id)}}
									@else
										{{$fatura->id}}
									@endif	
								@endif
							</td>
							<td>{{$fatura->username}}</td>
							<td>{{date("d-m-Y",strtotime($fatura->prej))}}</td>
							<td>{{date("d-m-Y",strtotime($fatura->deri))}}</td>
							<td>{{$fatura->pershkrimi}}</td>
							<td>{{$fatura->vlera}}&euro;</td>
							<td>{{$fatura->zbritja?$fatura->zbritja."&euro;":""}}</td>
							<td>@if($fatura->koment||$fatura->pauzakoment||$fatura->providerkoment)
								<input type="image" src="/img/koment_active.png"  onclick="alert('{{$fatura->koment.$fatura->providerkoment." - ".$fatura->pauzakoment}}')" />
								@else
								<input type="image" src="/img/koment_passive.png" onclick="" />
								@endif
							</td>
							<td>
								@if($fatura->id)
									@if($roli->printo_fature==1)
									<a href="/fatura/internet/{{$fatura->id}}"><image style="width:20px;;height:20px;" src="/img/Print.png"/></a>
									@endif
								@endif
							</td>
						</tr>
						<?php

							$vlera_totale = $fatura->vlera-$fatura->zbritja;
							$perqindja = 13.79;
							$tvsh = ($perqindja/100)*$vlera_totale;
							$pa_tvsh = $vlera_totale-((13.79/100)*$vlera_totale);

							$total += $vlera_totale;
							$fat_tvsh += $tvsh;
							$fat_pa_tvsh += $pa_tvsh;
						?>	
					@endforeach
					</table>
					<table style="width:500px;">
						<tr><th>Gjithsej pa TVSH</th><th>TVSH</th><th> Gjithsej me TVSH</th></tr>
						<tr><td>{{number_format($fat_pa_tvsh,2)}}&euro;</td><td>{{number_format($fat_tvsh,2)}}&euro;</th><td>{{number_format($total,2)}}&euro;</td></tr>
					</table>
					<div id="kerkoDaten">
						<form name="data" action="/fatura/search", method="post">
							<label>Prej: </label>
							<input name="prej" type="date"/>
							<label> Deri: </label>
							<input name="deri" type="date"/>
							<input type="image" src="/img/submit.png" id="submitButton"/>
						</form>
					</div>