<div id="edit">
	<script type="text/javascript">
			//No characters allowed
			function validate(evt) {
			  var theEvent = evt || window.event;
			  var key = theEvent.keyCode || theEvent.which;
			  key = String.fromCharCode( key );
			  var regex = /[0-9]|\./;
			  if( !regex.test(key) ) {
			    theEvent.returnValue = false;
			    if(theEvent.preventDefault) theEvent.preventDefault();
			  }
			}

			//Nese zbritja checked
			function display_zbritja(){
				if(document.getElementById("zbritja").checked){
					document.getElementById("vleraLabel").setAttribute("class","visible");
					document.getElementById("vlera").setAttribute("class","visible");
					document.getElementById("zbrKomentLabel").setAttribute("class","visible");
					document.getElementById("zbrKoment").setAttribute("class","visible");
				}else{
					document.getElementById("vleraLabel").setAttribute("class","hidden");
					document.getElementById("vlera").setAttribute("class","hidden");
					document.getElementById("zbrKomentLabel").setAttribute("class","hidden");
					document.getElementById("zbrKoment").setAttribute("class","hidden");
				}
			}

			//Nese Pagesa checked
			function display_pagesa(){
				if(document.getElementById("pagesa").checked){
					document.getElementById("shumaLabel").setAttribute("class","visible");
					document.getElementById("shuma").setAttribute("class","visible");
					document.getElementById("pagKomentLabel").setAttribute("class","visible");
					document.getElementById("pagKoment").setAttribute("class","visible");
				}else{
					document.getElementById("shumaLabel").setAttribute("class","hidden");
					document.getElementById("shuma").setAttribute("class","hidden");
					document.getElementById("pagKomentLabel").setAttribute("class","hidden");
					document.getElementById("pagKoment").setAttribute("class","hidden");
				}
			}

			//Nese TV checked
			function display_tv(){
				if(document.getElementById("trio").checked){
					document.getElementById("abonimLabel").setAttribute("class","visible");
					document.getElementById("muaj").setAttribute("class","visible");
					document.getElementById("kyqLabel").setAttribute("class","visible");
					document.getElementById("tvKyqja").setAttribute("class","visible");
					document.getElementById("shifraKLabel").setAttribute("class","visible");
					document.getElementById("shifra").setAttribute("class","visible");
					document.getElementById("cmimiKLabel").setAttribute("class","visible");
					document.getElementById("cmimiK").setAttribute("class","visible");
					document.getElementById("cmimiDLabel").setAttribute("class","visible");
					document.getElementById("cmimiD").setAttribute("class","visible");
					document.getElementById("cmimiILabel").setAttribute("class","visible");
					document.getElementById("cmimiI").setAttribute("class","visible");
				}else{
					document.getElementById("abonimLabel").setAttribute("class","hidden");
					document.getElementById("muaj").setAttribute("class","hidden");
					document.getElementById("kyqLabel").setAttribute("class","hidden");
					document.getElementById("tvKyqja").setAttribute("class","hidden");
					document.getElementById("shifraKLabel").setAttribute("class","hidden");
					document.getElementById("shifra").setAttribute("class","hidden");
					document.getElementById("cmimiKLabel").setAttribute("class","hidden");
					document.getElementById("cmimiK").setAttribute("class","hidden");
					document.getElementById("cmimiDLabel").setAttribute("class","hidden");
					document.getElementById("cmimiD").setAttribute("class","hidden");
					document.getElementById("cmimiILabel").setAttribute("class","hidden");
					document.getElementById("cmimiI").setAttribute("class","hidden");
				}
			}

				//Nese TV checked
			function enable_tvKyqja(){
				if(document.getElementById("tvKyqja").checked){
					document.getElementById("shifra").removeAttribute("disabled");
					document.getElementById("cmimiK").removeAttribute("disabled");
					document.getElementById("cmimiD").removeAttribute("disabled");
					document.getElementById("cmimiI").removeAttribute("disabled");	
				}else{
					document.getElementById("shifra").setAttribute("disabled",true);
					document.getElementById("cmimiK").setAttribute("disabled",true);
					document.getElementById("cmimiD").setAttribute("disabled",true);
					document.getElementById("cmimiI").setAttribute("disabled",true);
				}
			}

			function display_kyqje(){
				if(document.getElementById("kyqje").checked){
					document.getElementById("cmimiKyqjesLabel").setAttribute("class","visible");
					document.getElementById("cmimiKyqjes").setAttribute("class","visible");
					
				}else{
					document.getElementById("cmimiKyqjesLabel").setAttribute("class","hidden");
					document.getElementById("cmimiKyqjes").setAttribute("class","hidden");
				}
			}
			function display_pauze(){
				if(document.getElementById("pauze").checked){
					document.getElementById("ditLabel").setAttribute("class","visible");
					document.getElementById("dite").setAttribute("class","visible");
					
				}else{
					document.getElementById("ditLabel").setAttribute("class","hidden");
					document.getElementById("dite").setAttribute("class","hidden");
				}
			}

	</script>
	<?php $srvid = User::where("username","=",$user)->first()->srvid; 
		$service = Service::where("srvid","=",$srvid)->first()->srvname;
	?>
	<h1>{{HTML::link_to_route("user_view",$user,$user)}}</h1><br>
	<p style="color:#FF6666">@if(Session::has('msg'))
		{{Session::get('msg')}}
	@endif<p><br><br>

	
	{{Form::open('shto/klient/fatura','POST')}}

	<!--Internet-->
	<p><label>Kyqje</label><input name="kyqje" id="kyqje" type="checkbox" onclick="javascript:display_kyqje();"/></p>
	{{Form::label('cmimiKyqjes','Çmimi i Kyqjes: ', array('class'=>'hidden', 'id'=>'cmimiKyqjesLabel'))}}
	{{Form::text('cmimiKyqjes',"",array('style'=>'width:100px;', 'onkeypress'=>'validate(event)','class'=>'hidden','id'=>'cmimiKyqjes'))}}<br><br>
	
	<select name="pako", id="pako">
		<option value="{{$service}}" selected>{{$service}}</option>
		@foreach(Pako::where("emri","!=",$service)->get() as $pako)
			<option>{{$pako->emri}}</option>
		@endforeach
	</select><br><br>
	<!--{{Form::label('periudha','Periudha: ')}}
	{{Form::text("periudha",'1', array('style'=>'width:50px;','onkeypress'=>'validate(event)'))}}Muaj<br><br>-->

	@if(Id::where("username","=",$user)->first()->provider!=1)
	<!--TV-->
	<p><label>TV</label><input name="tv" id="trio" type="checkbox" onclick="javascript:display_tv();"/></p>
	{{Form::label('abonim','Mbushja: ',array('class'=>'hidden', 'id'=>'abonimLabel'))}}
	{{Form::text('mbushja',Kode::first()?Kode::first()->kodi:"",array('style'=>'width:170px;','class'=>'hidden','id'=>'muaj'))}}
	<p><label class="hidden" id="kyqLabel">Kyqje</label>
		<input name="tvKyqja" id="tvKyqja" type="checkbox" onclick="javascript:enable_tvKyqja();" class="hidden"/></p>
		{{Form::label('shifraKartela','Shifra e Karteles: ',array('class'=>'hidden', 'id'=>'shifraKLabel'))}}
			<p>{{Form::text('shifra',"",array('style'=>'width:150px;','class'=>'hidden','id'=>'shifra', 'disabled'))}}</p>
			{{Form::label('cmimiKartela','Çmimi i Karteles: ',array('class'=>'hidden', 'id'=>'cmimiKLabel'))}}
			<p>{{Form::text('cmimiK',"",array('style'=>'width:100px;', 'onkeypress'=>'validate(event)','class'=>'hidden','id'=>'cmimiK', 'disabled'))}}</p>
			{{Form::label('cmimiDekoder','Çmimi i Dekoderit: ',array('class'=>'hidden', 'id'=>'cmimiDLabel'))}}
			<p>{{Form::text('cmimiD',"",array('style'=>'width:100px;', 'onkeypress'=>'validate(event)','class'=>'hidden','id'=>'cmimiD', 'disabled'))}}</p>
			{{Form::label('cmimiInstalimit','Çmimi i Instalimit: ',array('class'=>'hidden', 'id'=>'cmimiILabel'))}}
			<p>{{Form::text('cmimiI',"",array('style'=>'width:100px;', 'onkeypress'=>'validate(event)','class'=>'hidden','id'=>'cmimiI', 'disabled'))}}</p>
	@endif

	<!--Zbritja-->
	<p><label>Zbritje</label><input name ="zbritja", id="zbritja" type="checkbox" onclick="javascript:display_zbritja();"/></p>
	{{Form::label('vlera','Vlera e zbritur: ',array('class'=>'hidden', 'id'=>'vleraLabel'))}}
	<p>{{Form::text('vlera',"",array('style'=>'width:100px;', 'onkeypress'=>'validate(event)','class'=>'hidden','id'=>'vlera'))}}</p>
	{{Form::label('koment','Arsyeja e zbritjes (e detyrueshme): ',array('class'=>'hidden', 'id'=>'zbrKomentLabel'))}}
	<p>{{Form::textarea('koment','',array('class'=>'hidden', 'id'=>'zbrKoment', 'style'=>'width:200px;height:50px;margin-bottom:20px;'))}}</p>
	<p><label>E rregullt</label><input name ="rregullt", id="rregullt" type="checkbox"/></p>

	@if(Id::where("username","=",$user)->first()->provider==0)
	<!--Pagesa-->
	<p><label>Pagesë</label><input name ="pagesa", id="pagesa" type="checkbox" onclick="javascript:display_pagesa();"/></p>
	{{Form::label('shuma','Shuma e paguar: ',array('class'=>'hidden', 'id'=>'shumaLabel'))}}
	<p>{{Form::text('shuma',"",array('style'=>'width:100px;', 'onkeypress'=>'validate(event)','class'=>'hidden','id'=>'shuma'))}}</p>
	{{Form::label('pagKoment','Koment (opsionale): ',array('class'=>'hidden', 'id'=>'pagKomentLabel'))}}
	<p>{{Form::textarea('pagKoment','',array('class'=>'hidden', 'id'=>'pagKoment'))}}</p>
	
	@endif
	{{Form::hidden('user',$user)}}
	{{Form::image('/img/submit.png', '',array('style'=>'width:50px; height:30px;'))}}
	{{Form::close()}}	
</div>