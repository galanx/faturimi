

<script>
		function validate(evt) {
			  var theEvent = evt || window.event;
			  var key = theEvent.keyCode || theEvent.which;
			  key = String.fromCharCode( key );
			  var regex = /[0-9]|\./;
			  if( !regex.test(key) ) {
			    theEvent.returnValue = false;
			    if(theEvent.preventDefault) theEvent.preventDefault();
			  }
			}
</script>
<div id="edit">
<h1>{{HTML::link_to_route('user_view',$user,$user)}}</h1>
<br><br>
<p style="color:#FF6666">@if(Session::has('msg'))
		{{Session::get('msg')}}<br><br>
	@endif<p>
{{Form::open('shto/fatura','POST')}}
{{Form::label('prej','Prej: ')}}<br>
{{Form::date('prej')}}<br><br>
{{Form::label('deri','Deri: ')}}<br>
{{Form::date('deri')}}<br><br>
{{Form::label('pershkrimi','Pershkrimi: ')}}<br>
{{Form::textarea('pershkrimi','',array('style'=>'width:300px;height:100px;'))}}<br><br>
{{Form::label('vlera','Vlera: ')}}<br>
{{Form::text('vlera','',array('style'=>'width:100px;','onkeypress'=>'validate(event)', 'id'=>'vlera'))}}<br>
{{Form::hidden('user',$user)}}
{{Form::image('/img/submit.png', '',array('style'=>'width:50px; height:30px;'))}}
{{Form::close()}}
</div>