@include('negenet.konto.aside')
<?php $roli = Role::where("id","=",$id)->first(); ?>
<div id="user-main" style="width:750px;float:right;">
	<div id="edit">
		<h1>Ndrysho Role</h1><br><br>
		<p style="color:#FF6666">@if(Session::has('msg'))
		{{Session::get('msg')}}<br><br>
		@endif<p>
		<p>@if(Session::has('success'))
		{{Session::get('success')}}<br><br>
		@endif<p>
		{{Form::open("konto/edit/role","POST",array("style"=>"width:100%;min-height:420px;"))}}
		{{Form::label("emri","Emri i rolit")}}<br>
		{{Form::text("emri",$roli->emri)}}<br><br>
		<div id="left" style="width:100px;height:100%;float:left;">
			<label for="klientet">Klientet </label>
			@if($roli->shiqo_klient==1)
				<input value="1" type="checkbox" id="klientet" name="klientet" checked/>
			@else
				<input value="1" type="checkbox" id="klientet" name="klientet"/>
			@endif	
			<br>
			<label for="shto_fature">Shto Faturë </label>
			@if($roli->shto_fature==1)
				<input value="1" type="checkbox" id="shto_fature" name="shto_fature" checked/>
			@else
				<input value="1" type="checkbox" id="shto_fature" name="shto_fature"/>
			@endif	
			<br>
			<label for="ndrysho_fature">Ndrysho Faturë </label>
			@if($roli->ndrysho_fature==1)
				<input value="1" type="checkbox" id="ndrysho_fature" name="ndrysho_fature" checked/>
			@else
				<input value="1" type="checkbox" id="ndrysho_fature" name="ndrysho_fature"/>
			@endif	
			<br>
			<label for="printo_fature">Printo Faturë </label>
			@if($roli->printo_fature==1)
				<input value="1" type="checkbox" id="printo_fature" name="printo_fature" checked/>
			@else
				<input value="1" type="checkbox" id="printo_fature" name="printo_fature"/>
			@endif	
			<br>
			<label for="shto_pagese">Shto Pagesë </label>
			@if($roli->shto_pagese==1)
				<input value="1" type="checkbox" id="shto_pagese" name="shto_pagese" checked/>
			@else
				<input value="1" type="checkbox" id="shto_pagese" name="shto_pagese"/>
			@endif	
			<br>
		</div>
		<div id="middle" style="width:100px;height:100%;float:left;">
			<label for="zbritje">Bëj Zbritje </label>
			@if($roli->zbritje==1)
				<input value="1" type="checkbox" id="zbritje" name="zbritje" checked/>
			@else
				<input value="1" type="checkbox" id="zbritje" name="zbritje"/>
			@endif	
			<br>
			<label for="tv">TV </label>
			@if($roli->shiqo_tv==1)
				<input value="1" type="checkbox" id="tv" name="tv" checked/>
			@else
				<input value="1" type="checkbox" id="tv" name="tv"/>
			@endif	
			<br>
			<label for="kaca">Kaca </label>
			@if($roli->shiqo_kaca==1)
				<input value="1" type="checkbox" id="kaca" name="kaca" checked/>
			@else
				<input value="1" type="checkbox" id="kaca" name="kaca"/>
			@endif	
			<br>
			<label for="hyra">Shto të hyra </label>
			@if($roli->shto_hyra==1)
				<input value="1" type="checkbox" id="hyra" name="hyra" checked/>
			@else
				<input value="1" type="checkbox" id="hyra" name="hyra"/>
			@endif	
			<br>
			<label for="dala">Shto të dala </label>
			@if($roli->shto_dala==1)
				<input value="1" type="checkbox" id="dala" name="dala" checked/>
			@else
				<input value="1" type="checkbox" id="dala" name="dala"/>
			@endif	
			<br>
		</div>
		<div id="right" style="width:100px;height:100%;float:left;">
			<label for="raportet">Raportet </label>
			@if($roli->shiqo_raportet==1)
				<input value="1" type="checkbox" id="raportet" name="raportet" checked/>
			@else
				<input value="1" type="checkbox" id="raportet" name="raportet"/>
			@endif	
			<br>
			<label for="log">Log </label>
			@if($roli->shiqo_log==1)
				<input value="1" type="checkbox" id="log" name="log" checked/>
			@else
				<input value="1" type="checkbox" id="log" name="log"/>
			@endif	
			<br>
			<label for="provider">Provider </label>
			@if($roli->shiqo_provider==1)
				<input value="1" type="checkbox" id="provider" name="provider" checked/>
			@else
				<input value="1" type="checkbox" id="provider" name="provider"/>
			@endif	
			<br>
			<label for="ndrysho_skadimin">Ndrysho Skadimin </label>
			@if($roli->ndrysho_skadimin==1)
				<input value="1" type="checkbox" id="ndrysho_skadimin" name="ndrysho_skadimin" checked/>
			@else
				<input value="1" type="checkbox" id="ndrysho_skadimin" name="ndrysho_skadimin"/>
			@endif	
			<br>
			<label for="pakot">Pakot </label>
			@if($roli->pakot==1)
				<input value="1" type="checkbox" id="pakot" name="pakot" checked/>
			@else
				<input value="1" type="checkbox" id="pakot" name="pakot"/>
			@endif	
			<br>
		</div>
		<div id="rightMore" style="width:100px;height:100%;float:left;">
			<label for="shitja">Shitja </label>
			@if($roli->shitja==1)
				<input value="1" type="checkbox" id="shitja" name="shitja" checked/>
			@else
				<input value="1" type="checkbox" id="shitja" name="shitja"/>
			@endif	
			<br>
			<label for="konto">Konto </label>
			@if($roli->konto==1)
				<input value="1" type="checkbox" id="konto" name="konto" checked/>
			@else
				<input value="1" type="checkbox" id="konto" name="konto"/>
			@endif		
			<br>
			<label for="shto_user">Shto Konto </label>
			@if($roli->shto_user==1)
				<input value="1" type="checkbox" id="shto_user" name="shto_user" checked/>
			@else
				<input value="1" type="checkbox" id="shto_user" name="shto_user"/>
			@endif		
			<br>
			<label for="ndrysho_user">Ndrysho Konto </label>
			@if($roli->ndrysho_user==1)
				<input value="1" type="checkbox" id="ndrysho_user" name="ndrysho_user" checked/>
			@else
				<input value="1" type="checkbox" id="ndrysho_user" name="ndrysho_user"/>
			@endif		
			<br>
			<label for="shto_rol">Shto Rol </label>
			@if($roli->shto_rol==1)
				<input value="1" type="checkbox" id="shto_rol" name="shto_rol" checked/>
			@else
				<input value="1" type="checkbox" id="shto_rol" name="shto_rol"/>
			@endif		
			<br>
		</div>
		<div id="rightMost" style="width:100px;height:100%;float:left;">
			<label for="ndrysho_rol">Ndrysho Rol </label>
			@if($roli->ndrysho_rol==1)
				<input value="1" type="checkbox" id="ndrysho_rol" name="ndrysho_rol" checked/>
			@else
				<input value="1" type="checkbox" id="ndrysho_rol" name="ndrysho_rol"/>
			@endif		
			<br>
			<label for="fshij_user">Fshij Konto </label>
			@if($roli->fshij_user)
				<input value="1" type="checkbox" id="fshij_user" name="fshij_user" checked/>
			@else
				<input value="1" type="checkbox" id="fshij_user" name="fshij_user"/>
			@endif		
			<br>
			<label for="fshij_rol">Fshij Rol </label>
			@if($roli->fshij_rol==1)
				<input value="1" type="checkbox" id="fshij_rol" name="fshij_rol" checked/>
			@else
				<input value="1" type="checkbox" id="fshij_rol" name="fshij_rol"/>
			@endif	
			<br>
		</div>
		<br>
		{{Form::hidden("id",$roli->id)}}	
		{{Form::image('/img/submit.png', '',array('style'=>'width:50px; height:30px;display:block; clear:both;margin:0 auto;'))}}
		{{Form::close()}}
	</div>
</div>