@include('negenet.konto.aside')
<div id="user-main" style="width:750px;float:right;">
	<div id="edit">
		<h1>Shto Konto</h1><br><br>
		<p style="color:#FF6666">@if(Session::has('msg'))
		{{Session::get('msg')}}<br><br>
		@endif<p>
		<p>@if(Session::has('success'))
		{{Session::get('success')}}<br><br>
		@endif<p>
		{{Form::open("konto/shto/konto","POST")}}
		{{Form::label("user","Emri")}}<br>
		{{Form::text("user")}}<br><br>
		{{Form::label("password", "Fjalëkalimi")}}<br>
		{{Form::password("password")}}<br><br>
		{{Form::label("confirm", "Konfirmo Fjalëkalimin")}}<br>
		{{Form::password("confirm")}}<br><br>
		<select name="rolet">
			<option value="" selected>--Zgjidheni Rolin--</option>
			@foreach(Role::where("id","!=",1)->get() as $role)
			<option value="{{$role->id}}">{{$role->emri}}</option>
			@endforeach
		</select><br><br>
		{{Form::image('/img/submit.png', '',array('style'=>'width:50px; height:30px;'))}}
		{{Form::close()}}
	</div>
</div>