@include('negenet.konto.aside')
<?php $user = Admin::where("id","=",$id)->first(); 
	$role = Role::where("id","=",$user->role_id)->first();
?>
<div id="user-main" style="width:750px;float:right;">
	<div id="edit">
		<h1>Ndrysho Konto</h1><br><br>
		<p style="color:#FF6666">@if(Session::has('msg'))
		{{Session::get('msg')}}<br><br>
		@endif<p>
		<p>@if(Session::has('success'))
		{{Session::get('success')}}<br><br>
		@endif<p>
		{{Form::open("konto/edit/konto","POST")}}
		{{Form::label("user","Emri")}}<br>
		{{Form::text("user",$user->username)}}
		{{Form::label("password", "Fjalëkalimi")}}<br>
		{{Form::password("password")}}<br><br>
		{{Form::label("confirm", "Konfirmo Fjalëkalimin")}}<br>
		{{Form::password("confirm")}}<br><br>
		<select name="rolet">
			<option value="{{$user->role_id}}" selected>{{$role->emri}}</option>
			@foreach(Role::where("id","!=",$user->role_id)->where("id","!=",1)->get() as $role)
			<option value="{{$role->id}}">{{$role->emri}}</option>
			@endforeach
		</select><br><br>
		{{Form::hidden("id",$user->id)}}
		{{Form::image('/img/submit.png', '',array('style'=>'width:50px; height:30px;'))}}
		{{Form::close()}}
	</div>
</div>