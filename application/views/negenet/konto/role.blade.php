@include('negenet.konto.aside')
<?php $roli = Role::where("id","=",Auth::user()->role_id)->first(); ?>
<div id="user-main" style="width:750px;float:right;">
	{{Form::open("konto/delete/roles","POST", array("style"=>"margin:20px;"))}}
	<table style="width:500px">
		<p style="color:#FF6666">@if(Session::has('msg'))
		{{Session::get('msg')}}<br><br>
		@endif<p>
		<tr><th></th><th>ID</th><th>Roli</th></tr>
	@foreach($result as $role)
		<tr>
			<td>
				@if($roli->fshij_rol==1)
					<input type="checkbox" name="delete[]" value="{{$role->id}}"/>
				@else
					<input type="checkbox" name="delete[]" value="{{$role->id}}" disabled/>
				@endif	
			</td>
			<td>{{$role->id}}</td>
			<td>
				@if($role->id==1)
				{{$role->emri}}
				@else
					@if($roli->ndrysho_rol==1)
					{{HTML::link_to_route("konto_ndrysho_role",$role->emri,$role->id)}}
					@else
					{{$role->emri}}
					@endif
				@endif
			</td>
		</tr>
	@endforeach
		@if($roli->fshij_rol==1)
			<tr>{{Form::image('/img/delete.png', '',array('style'=>'width:30px; height:30px;'))}}</tr>
		@endif	
		{{Form::close()}}
	</table>
</div>