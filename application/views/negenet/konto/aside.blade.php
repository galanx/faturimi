<?php $roli = Role::where("id","=",Auth::user()->role_id)->first(); ?>
<div id="user-aside" style="float:left;">
	<div id="user-menu">
		<ul>
			<li id="lista_kontot">{{HTML::link_to_route("konto","Lista e Kontove")}}</li>
			<li id="lista_rolet">{{HTML::link_to_route("konto_role","Lista e Roleve")}}</li>
			<li id="info">{{HTML::link_to_route("konto_ndrysho","Të dhënat")}}</li>
			@if($roli->shto_user==1)
				<li id="shto_konto">{{HTML::link_to_route("konto_shto","Shto Konto")}}</li>
			@endif
			@if($roli->shto_rol==1)	
			<li id="shto_role">{{HTML::link_to_route("konto_shto_role","Shto Role")}}</li>
			@endif
		</ul>
	</div>	
</div><script>
				window.onload = function(){
				document.getElementById("konto").setAttribute("class","current");
				}
	</script>			