@include('negenet.konto.aside')
<?php $roli = Role::where("id","=",Auth::user()->role_id)->first(); ?>
<div id="user-main" style="width:750px;float:right;">
	{{Form::open("konto/delete/konto","POST", array("style"=>"margin:20px;"))}}
	<table style="width:500px">
		<p style="color:#FF6666">@if(Session::has('msg'))
		{{Session::get('msg')}}<br><br>
	@endif<p>
		<tr><th></th><th>ID</th><th>User</th><th>Roli</th></tr>
	@foreach($result as $user)
		<tr><td>
			@if($roli->fshij_user==1)
				<input type="checkbox" name="delete[]" value="{{$user->id}}"/>
			@else
				<input type="checkbox" name="delete[]" value="{{$user->id}}" disabled/>	
			@endif	
			</td>
			<td>{{$user->id}}</td>
			<td>
				@if($user->role_id==1||$user->id==Auth::user()->id)
				{{$user->username}}
				@else
					@if($roli->ndrysho_user==1)
					{{HTML::link_to_route("konto_ndrysho_konto",$user->username,$user->id)}}
					@else
					{{$user->username}}
					@endif
				@endif
			</td>
			<td>{{Role::where("id","=",$user->role_id)->first()->emri}}</td>
		</tr>
	@endforeach
		@if($roli->fshij_user==1)
			<tr>{{Form::image('/img/delete.png', '',array('style'=>'width:30px; height:30px;'))}}</tr>
		@endif	
		{{Form::close()}}
	</table>
</div>