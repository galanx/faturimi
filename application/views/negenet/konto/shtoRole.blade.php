@include('negenet.konto.aside')
<div id="user-main" style="width:750px;float:right;">
	<div id="edit">
		<h1>Shto Role</h1><br><br>
		<p style="color:#FF6666">@if(Session::has('msg'))
		{{Session::get('msg')}}<br><br>
		@endif<p>
		<p>@if(Session::has('success'))
		{{Session::get('success')}}<br><br>
		@endif<p>
		{{Form::open("konto/add/role","POST",array("style"=>"width:100%;min-height:420px;"))}}
		{{Form::label("emri","Emri i rolit")}}<br>
		{{Form::text("emri")}}<br><br>
		<div id="left" style="width:100px;height:100%;float:left;">
			<label for="klientet">Klientet </label><input value="1" type="checkbox" id="klientet" name="klientet"/><br>
			<label for="shto_fature">Shto Faturë </label><input value="1" type="checkbox" id="shto_fature" name="shto_fature"/><br>
			<label for="ndrysho_fature">Ndrysho Faturë </label><input value="1" type="checkbox" id="ndrysho_fature" name="ndrysho_fature"/><br>
			<label for="printo_fature">Printo Faturë </label><input value="1" type="checkbox" id="printo_fature" name="printo_fature"/><br>
			<label for="shto_pagese">Shto Pagesë </label><input value="1" type="checkbox" id="shto_pagese" name="shto_pagese"/><br>
			
		</div>
		<div id="middle" style="width:100px;height:100%;float:left;">
			<label for="zbritje">Bëj Zbritje </label><input value="1" type="checkbox" id="zbritje" name="zbritje"/><br>
			<label for="tv">TV </label><input value="1" type="checkbox" id="tv" name="tv"/><br>
			<label for="kaca">Kaca </label><input value="1" type="checkbox" id="kaca" name="kaca"/><br>
			<label for="hyra">Shto të hyra </label><input value="1" type="checkbox" id="hyra" name="hyra"/><br>
			<label for="dala">Shto të dala </label><input value="1" type="checkbox" id="dala" name="dala"/><br>
		</div>
		<div id="right" style="width:100px;height:100%;float:left;">
			<label for="raportet">Raportet </label><input value="1" type="checkbox" id="raportet" name="raportet"/><br>
			<label for="log">Log </label><input value="1" type="checkbox" id="log" name="log"/><br>
			<label for="provider">Provider </label><input value="1" type="checkbox" id="provider" name="provider"/><br>
			<label for="ndrysho_skadimin">Ndrysho Skadimin </label><input value="1" type="checkbox" id="ndrysho_skadimin" name="ndrysho_skadimin"/><br>
			<label for="pakot">Pakot </label><input value="1" type="checkbox" id="pakot" name="pakot"/><br>
		</div>
		<div id="rightMore" style="width:100px;height:100%;float:left;">
			<label for="shitja">Shitja </label><input value="1" type="checkbox" id="shitja" name="shitja"/><br>
			<label for="konto">Konto </label><input value="1" type="checkbox" id="konto" name="konto"/><br>
			<label for="shto_user">Shto Konto </label><input value="1" type="checkbox" id="shto_user" name="shto_user"/><br>
			<label for="ndrysho_user">Ndrysho Konto </label><input value="1" type="checkbox" id="ndrysho_user" name="ndrysho_user"/><br>
			<label for="shto_rol">Shto Rol </label><input value="1" type="checkbox" id="shto_rol" name="shto_rol"/><br>
		</div>
		<div id="rightMost" style="width:100px;height:100%;float:left;">
			<label for="ndrysho_rol">Ndrysho Rol </label><input value="1" type="checkbox" id="ndrysho_rol" name="ndrysho_rol"/><br>
			<label for="fshij_user">Fshij Konto </label><input value="1" type="checkbox" id="fshij_user" name="fshij_user"/><br>
			<label for="fshij_rol">Fshij Rol </label><input value="1" type="checkbox" id="fshij_rol" name="fshij_rol"/><br>
		</div>
		<br>	
		{{Form::image('/img/submit.png', '',array('style'=>'width:50px; height:30px;display:block; clear:both;margin:0 auto;'))}}
		{{Form::close()}}
	</div>
</div>