				<table>
					<caption><button id="back" onclick="javascript:history.go(-1);">&lt;&lt;Mbrapa</button> Historiku i të gjithë abonuesve për TV.</caption>
					<tr><th>ID</th><th>User</th><th>Kartela</th><th>Abonim</th><th>Mbushja</th><th>Dekoderi</th><th>Kartela</th><th>Instalimi</th>
						<th>Të hyrat</th><th>Data</th><th>Fatura</th></tr>
						<?php $total = 0 ?>
					@foreach($result->results as $tv)
						<tr>	
							<td>{{$tv->id}}</td>
							<td>{{HTML::link_to_route("user_view",$tv->username,$tv->username)}}</td>
							<td>{{HTML::link_to_route("kartela_edit",Kartela::where("username","=",$tv->username)->first()?Kartela::where("username","=",$tv->username)->first()->kartela:"",$tv->username)}}</td>
							<td>{{$tv->abonim}} Muaj</td>
							<td>{{$tv->kodi}}</td>
							<td>{{$tv->cmimi_dekoder}}&euro;</td>
							<td>{{$tv->cmimi_kartele}}&euro;</td>
							<td>{{$tv->cmimi_instalimit}}&euro;</td>
							<td>{{$tv->cmimi_instalimit+$tv->cmimi_dekoder+$tv->cmimi_kartele+($tv->cmimi_mujor*$tv->abonim)}}&euro;</td>
							<td>{{date("d-m-Y",strtotime($tv->data))}}</td>
							<td>{{$tv->fatura_id}}</td>
						</tr><?php $total += $tv->cmimi_instalimit+$tv->cmimi_dekoder+$tv->cmimi_kartele+($tv->cmimi_mujor*$tv->abonim) ?>
					@endforeach
					<th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th>Gjithsej: {{$total}}&euro;</th><th></th><th></th>
					</table>
					{{$result->links()}}
					<div id="kerkoDaten">
						<form name="data" action="/tv/search", method="post">
							<label>Prej: </label>
							<input name="prej" type="date"/>
							<label> Deri: </label>
							<input name="deri" type="date"/>
							<input type="image" src="/img/submit.png" id="submitButton"/>
						</form>
					</div>