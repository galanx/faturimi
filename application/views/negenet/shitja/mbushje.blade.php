		<script>
				function validate(evt) {
			  var theEvent = evt || window.event;
			  var key = theEvent.keyCode || theEvent.which;
			  key = String.fromCharCode( key );
			  var regex = /[0-9]|\./;
			  if( !regex.test(key) ) {
			    theEvent.returnValue = false;
			    if(theEvent.preventDefault) theEvent.preventDefault();
			  }
			}

</script>

		<div id="skadimi">
			@if(Session::has("msg"))
			<p style="color:#FF6666">{{Session::get("msg")}}</p><br><br>
			@endif
			@if(Session::has("success"))
			<p style="color:#33CC33">{{Session::get("success")}}</p><br><br>
			@endif
			{{Form::open('shitja/dergombushje/','POST')}}
			{{Form::label('emri','Emri: ')}}<br>
			{{Form::text("emri")}}<br><br>
			{{Form::label('tel','Tel: ')}}<br>
			{{Form::text("tel")}}<br><br>
			{{Form::label('smart','Smart Kartela: ')}}<br>
			{{Form::text("smart")}}<br><br>
			{{Form::label('periudha','Sa muaj? ')}}<br>
			{{Form::text('periudha',"",array('style'=>'width:70px;padding:0;margin:0;', 'onkeypress'=>'validate(event)',"class"=>"sasia" ,'id'=>'periudha'))}}<br><br>
			{{Form::image("/img/submit.png","",array("style"=>"width:60px;"))}}
			{{Form::close()}}
		</div>