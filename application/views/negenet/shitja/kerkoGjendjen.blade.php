<script>
				function validate(evt) {
			  var theEvent = evt || window.event;
			  var key = theEvent.keyCode || theEvent.which;
			  key = String.fromCharCode( key );
			  var regex = /[0-9]|\./;
			  if( !regex.test(key) ) {
			    theEvent.returnValue = false;
			    if(theEvent.preventDefault) theEvent.preventDefault();
			  }
			}

							window.onload = function(){
				document.getElementById("shitja").setAttribute("class","current");
				}
</script>
<div id="shitja-interface">
	<div id="shitja-main">
		<div id="top-shitja">
			{{HTML::link("shitja/stokuGjendja","Gjendja e Stokut", array("class"=>"paglink", "style"=>"color:orange"))}}
			{{HTML::link("shitja/kode","Shto Kode për TV", array("class"=>"paglink"))}}
			{{HTML::link("shitja/malli","Hyrjet e Mallit", array("class"=>"paglink"))}}
			{{HTML::link("shitja","Faturat e Shitjes", array("class"=>"paglink"))}}
			{{HTML::link("shitja/shtoProdukt","Regjistro Produkt", array("class"=>"paglink"))}}
			{{HTML::link("shitja/shtoFature","Shto Faturë", array("class"=>"paglink"))}}
			{{HTML::link("shitja/shtoHyrje","Shto Hyrje", array("class"=>"paglink"))}}
		</div>
					<div id="kerkoProdukt" style="margin-top:50px;">
						<form name="data" action="/shitja/kerkoMallin", method="post">
							<label>Kerko: </label>
							<input name="kerkoMallin" type="text"/>
							<input type="image" src="/img/submit.png" id="submitButton"/>
						</form>
					</div>
		<table id="shitja-table">
			<tr>
				<th>ID</th><th>Produkti</th><th>Pershkrimi</th><th>Çmimi</th><th>Gjendja</th>
			</tr>
			@foreach($shitjet as $shitja)
			<tr>
				<td>{{$shitja->id}}</td>
				<td>{{$shitja->emri}}</td>
				<td>{{$shitja->pershkrimi}}</td>
				<td>{{$shitja->cmimi}}&euro;</td>
				<td>{{$shitja->gjendja}}</td>
			</tr>
			@endforeach
		</table>
	</div>
</div>
