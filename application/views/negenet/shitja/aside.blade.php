<?php $i = 0?>
@foreach(Produktet::all() as $produkt)	

	<input type="hidden" id="list{{$i}}" name="{{$produkt->id}}" value="{{$produkt->emri}}">
	<?php $i++; ?>
@endforeach	
<input type="hidden" id="numri" value="{{$i}}"/>

<div id="shitja-aside">
		<h2>Fatura</h2>
			<p style="color:#FF6666">@if(Session::has('f-msg'))
			{{Session::get('f-msg')}}<br><br>
			@endif</p>
		{{Form::open("shitja/shto/fatura", "POST")}}
		{{Form::label("bleresi","Bleresi: ")}}<br>
		{{Form::text("bleresi")}}<br><br>
		{{Form::label("produkti","Produkti: ")}}<br>
		
		<div id="inhere">
			<select name="produkti" id="produkti">
				<option value="" selected>--Zgjidheni Produktin--</option>
				@foreach(Produktet::all() as $produkt)
				<option value="{{$produkt->id}}">{{$produkt->emri}}</option>
				@endforeach
			</select><a href="javascript:AddFormField('inhere','text','','','div','select')" style="text-decoration:none;">
			<span style="font-weight:bold;color:green;">[+]</span>
			</a><br><br>

			{{Form::label("sasia","Sasia: ")}}
			{{Form::text('sasia',"",array('style'=>'width:50px;', 'onkeypress'=>'validate(event)','id'=>'sasia'))}}<br><br>
			{{Form::label("cmimi","Çmimi: ")}}
			{{Form::text('cmimi',"",array('style'=>'width:50px;', 'onkeypress'=>'validate(event)','id'=>'cmimi'))}}
			<br><br>
		</div>	
		&nbsp;&nbsp;&nbsp;<span style="color:#f8f8f8;">Total: <span id="result"></span>&euro;</span>
		<br><br>
		{{Form::label("pershkrimi","Pershkrimi: ")}}<br>
		{{Form::textarea("pershkrimi","",array("rows"=>"3","cols"=>"18"))}}<br><br>
		{{Form::label("koment","Koment: ")}}<br>
		{{Form::textarea("koment","",array("rows"=>"3","cols"=>"18"))}}<br>
		{{Form::image('/img/submit.png', '',array('style'=>'width:50px; height:30px;'))}}
		{{Form::close()}}

		<form>

		<h2>Hyrjet e Mallit</h2>
			<p style="color:#FF6666">@if(Session::has('h-msg'))
			{{Session::get('h-msg')}}<br><br>
			@endif</p>
		{{Form::open("shitja/shto/hyrje", "POST")}}
		{{Form::label("furnizuesi","Furnizuesi: ")}}<br>
		{{Form::text("furnizuesi")}}<br><br>
		{{Form::label("produkti","Produkti: ")}}<br>
		<div id="inhyrje">
		<select name="produkti" id="produkti">
			<option value="" selected>--Zgjidheni Produktin--</option>
			@foreach(Produktet::all() as $produkt)
			<option value="{{$produkt->id}}">{{$produkt->emri}}</option>
			@endforeach
		</select>			
		<a href="javascript:AddFormField('inhyrje','text','','','div','select')" style="text-decoration:none;">
			<span style="font-weight:bold;color:green;">[+]</span>
			</a><br><br>
		{{Form::label("sasia","Sasia: ")}}
		{{Form::text('sasia',"",array('style'=>'width:50px;', 'onkeypress'=>'validate(event)','id'=>'sasiah'))}}
		<br><br>
		{{Form::label("cmimi","Çmimi: ")}}
		{{Form::text('cmimi',"",array('style'=>'width:50px;', 'onkeypress'=>'validate(event)','id'=>'cmimih'))}}
		<br></br>
		</div>
		&nbsp;&nbsp;&nbsp;<span style="color:#f8f8f8;">Total: <span id="resulth"></span>&euro;</span>
		<br><br>
		{{Form::label("pershkrimi","Pershkrimi: ")}}<br>
		{{Form::textarea("pershkrimi","",array("rows"=>"3","cols"=>"18"))}}<br>
		{{Form::label("fatura","Fatura: ")}}<br>
		{{Form::text('fatura',"",array('style'=>'width:70px;','id'=>'fatura'))}}<br>
		{{Form::image('/img/submit.png', '',array('style'=>'width:50px; height:30px;'))}}
		{{Form::close()}}
	</div>
			<script>

				window.onload = function(){
				document.getElementById("shitja").setAttribute("class","current");
				}

				$(document).ready(function(){
			    $('#cmimi').keyup(function(){
			        $('#result').text($('#cmimi').val() * $('#sasia').val());
			    	});   
				});

				$(document).ready(function(){
			    $('#sasia').keyup(function(){
			        $('#result').text($('#cmimi').val() * $('#sasia').val());
			    	});   
				});

				$(document).ready(function(){
			    $('#cmimih').keyup(function(){
			        $('#resulth').text($('#cmimih').val() * $('#sasiah').val());
			    	});   
				});

				$(document).ready(function(){
			    $('#sasiah').keyup(function(){
			        $('#resulth').text($('#cmimih').val() * $('#sasiah').val());
			    	});   
				});
			</script>