<script>
				function validate(evt) {
			  var theEvent = evt || window.event;
			  var key = theEvent.keyCode || theEvent.which;
			  key = String.fromCharCode( key );
			  var regex = /[0-9]|\./;
			  if( !regex.test(key) ) {
			    theEvent.returnValue = false;
			    if(theEvent.preventDefault) theEvent.preventDefault();
			  }
			}

							window.onload = function(){
				document.getElementById("shitja").setAttribute("class","current");
				}
</script>
<?php $roli = Role::where("id","=",Auth::user()->role_id)->first(); ?>
<div id="shitja-interface">
	<div id="shitja-main">
		<div id="top-shitja">
			{{HTML::link("shitja/stokuGjendja","Gjendja e Stokut", array("class"=>"paglink"))}}
			{{HTML::link("shitja/kode","Shto Kode për TV", array("class"=>"paglink"))}}
			{{HTML::link("shitja/malli","Hyrjet e Mallit", array("class"=>"paglink", "style"=>"color:orange"))}}
			{{HTML::link("shitja","Faturat e Shitjes", array("class"=>"paglink"))}}
			{{HTML::link("shitja/shtoProdukt","Regjistro Produkt", array("class"=>"paglink"))}}
			{{HTML::link("shitja/shtoFature","Shto Faturë", array("class"=>"paglink"))}}
			{{HTML::link("shitja/shtoHyrje","Shto Hyrje", array("class"=>"paglink"))}}
		</div>
					<div id="kerkoDaten" style="margin-top:50px;">
							<p style="color:#FF6666">@if(Session::has('d-msg'))
							{{Session::get('d-msg')}}
							@endif<p>
						<form name="data" action="/shitja/hyrjet", method="post">
							<label>Prej: </label>
							<input name="prej" type="date"/>
							<label> Deri: </label>
							<input name="deri" type="date"/>
							<input type="image" src="/img/submit.png" id="submitButton"/>
						</form>
					</div>
		<table id="shitja-table" style="width:400px;">
			<tr>
				<th>Fat.</th><th>Furnizuesi</th><th>Data</th><th></th>
			</tr>
			@foreach($shitjet->results as $shitja)
			<tr>
				<td>{{$shitja->fatura}}</td>
				<td>{{$shitja->furnizuesi}}</td>
				<td>{{date("d-m-Y",strtotime($shitja->data))}}</td>
				<td>
					@if($shitja->id)
						@if($roli->printo_fature==1)
						<a href="/fatura/hyrjet/{{$shitja->id}}"><image style="width:20px;;height:20px;" src="/img/Print.png"/></a>
						@endif
					@endif
				</td>
			</tr>
			@endforeach
		</table>
		{{$shitjet->links()}}
	</div>
</div>
