<script>
				function validate(evt) {
			  var theEvent = evt || window.event;
			  var key = theEvent.keyCode || theEvent.which;
			  key = String.fromCharCode( key );
			  var regex = /[0-9]|\./;
			  if( !regex.test(key) ) {
			    theEvent.returnValue = false;
			    if(theEvent.preventDefault) theEvent.preventDefault();
			  }
			}

							window.onload = function(){
				document.getElementById("shitja").setAttribute("class","current");
				}
			<?php $roli = Role::where("id","=",Auth::user()->role_id)->first(); ?>
</script>
<div id="shitja-interface">
	<div id="shitja-main">
		<div id="top-shitja">
			{{HTML::link("shitja/stokuGjendja","Gjendja e Stokut", array("class"=>"paglink"))}}
			{{HTML::link("shitja/kode","Shto Kode për TV", array("class"=>"paglink"))}}
			{{HTML::link("shitja/malli","Hyrjet e Mallit", array("class"=>"paglink"))}}
			{{HTML::link("shitja","Faturat e Shitjes", array("class"=>"paglink", "style"=>"color:orange"))}}
			{{HTML::link("shitja/shtoProdukt","Regjistro Produkt", array("class"=>"paglink"))}}
			{{HTML::link("shitja/shtoFature","Shto Faturë", array("class"=>"paglink"))}}
			{{HTML::link("shitja/shtoHyrje","Shto Hyrje", array("class"=>"paglink"))}}
		</div>
					<div id="kerkoDaten" style="margin-top:50px;">
							<p style="color:#FF6666">@if(Session::has('d-msg'))
							{{Session::get('d-msg')}}
							@endif<p>
						<form name="data" action="/shitja/fatura", method="post">
							<label>Prej: </label>
							<input name="prej" type="date"/>
							<label> Deri: </label>
							<input name="deri" type="date"/>
							<input type="image" src="/img/submit.png" id="submitButton"/>
						</form>
					</div>
		<table id="shitja-table" style="width:400px;">
			<tr>
				<th>Fat.</th><th>Bleresi</th><th>Data</th><th></th><th></th>
			</tr>
			@foreach($shitjet as $shitja)
			<tr>
				<td>
					@if($roli->ndrysho_fature==1)
						{{HTML::link_to_route("shitja_ndrysho",$shitja->id,$shitja->id)}}
					@else
						{{$shitja->id}}
					@endif
				</td>
				@if($shitja->paguar==0)
					<td style="background:#FF6666;">{{$shitja->bleresi}}</td>
				@else
					<td>{{$shitja->bleresi}}</td>
				@endif	
				<td>{{date("d-m-Y",strtotime($shitja->data))}}</td>
				<td>@if($shitja->koment)
						<input type="image" src="/img/koment_active.png"  onclick="alert('{{$shitja->koment}}')" />
					@else
						<input type="image" src="/img/koment_passive.png" onclick="" />
					@endif
				</td>
				<td>
					@if($shitja->id)
						@if($roli->printo_fature==1)
						<a href="/fatura/shitja/{{$shitja->id}}"><image style="width:20px;;height:20px;" src="/img/Print.png"/></a>
						<a href="/fatura/shitjarregullt/{{$shitja->id}}">RR</a>
						@endif
					@endif
				</td>
			</tr>
			@endforeach
		</table>
	</div>
</div>
