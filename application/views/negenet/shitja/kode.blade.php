			<script>
				function customclick(){
					document.getElementById("kodet").click();
				}

				$(document).ready(function(){
					$("#kodet").change(function(){
						$("#path").text($("#kodet").val());
						$("#custom").css("background","green");
					});
				});
			</script>

<div id="shitja-interface">
	<div id="shitja-main">
		<div id="top-shitja">
			{{HTML::link("shitja/stokuGjendja","Gjendja e Stokut", array("class"=>"paglink"))}}
			{{HTML::link("shitja/kode","Shto Kode për TV", array("class"=>"paglink", "style"=>"color:orange"))}}
			{{HTML::link("shitja/malli","Hyrjet e Mallit", array("class"=>"paglink"))}}
			{{HTML::link("shitja","Faturat e Shitjes", array("class"=>"paglink"))}}
			{{HTML::link("shitja/shtoProdukt","Regjistro Produkt", array("class"=>"paglink"))}}
			{{HTML::link("shitja/shtoFature","Shto Faturë", array("class"=>"paglink"))}}
			{{HTML::link("shitja/shtoHyrje","Shto Hyrje", array("class"=>"paglink"))}}
		</div>

		<div id="edit" style="width:300px;">
			<p style="color:#FF6666">@if(Session::has('msg'))
			{{Session::get('msg')}}<br><br>
			@endif<p>
			{{Form::open_for_files("shitja/shtokode","POST", array("name"=>"frmUpload"))}}
			{{Form::label("kodi","Kodi: ")}}
			{{Form::text("kodi")}}<br>
			<p>{{Form::file("kodet",array("id"=>"kodet", "style"=>"display:none;"))}}
				<input type="button" id="custom" name="custom" value="Importo" onclick="customclick();" style="color:#ccc;font-weight:bold;border-radius:5px;"/>
				<p id="path"></p><br>
			</p>
			{{Form::image('/img/submit.png', '',array('style'=>'width:50px; height:30px;'))}}
			{{Form::close()}}

			<p>Numri i kodeve: {{Kode::count("kodi")}}</p>
		</div>
		<div style="width:500px;margin:0 auto;text-align:center;">
			{{HTML::link("#","Dërgo mbushje",array('style'=>'color:white;text-decoration:none;background:#464646;padding:10px;border:3px dashed #eee;font-size:0.8em;text-align:center;font-weight:bold;', 'onclick'=>"window.open('/shitja/mbushje', 'Skadimi','height=270, width=240, left=500px, top=380px, location=no, resizable=no')"))}}
		</div>
		<table style="width:500px;">
			{{Form::open('shitja/fshijkode',"POST",array("id"=>"delete"))}}
			<tr><th><a href="#" style="color:white;text-decoration:none;" onclick="document.getElementById('delete').submit();">[X]</a></th><th>Id</th><th>Kodi</th><th>Data</th></tr>
			<?php $kodet = Kode::order_by("id","asc")->paginate(); ?>
			@foreach($kodet->results as $kodi)
			<tr>
				<td>{{Form::checkbox('checked[]',$kodi->id)}}</td>
				<td>{{$kodi->id}}</td>
				<td>{{$kodi->kodi}}</td>
				<td>{{$kodi->data}}</td>
			</tr>
			@endforeach
			{{Form::close()}}
		</table>
		{{$kodet->links()}}
	</div>
</div>