<script>
				function validate(evt) {
			  var theEvent = evt || window.event;
			  var key = theEvent.keyCode || theEvent.which;
			  key = String.fromCharCode( key );
			  var regex = /[0-9]|\./;
			  if( !regex.test(key) ) {
			    theEvent.returnValue = false;
			    if(theEvent.preventDefault) theEvent.preventDefault();
			  }
			}

							window.onload = function(){
				document.getElementById("shitja").setAttribute("class","current");
				}
</script>
<div id="shitja-interface">
	<div id="shitja-main">
		<div id="top-shitja">
			{{HTML::link("shitja/stokuGjendja","Gjendja e Stokut", array("class"=>"paglink"))}}
			{{HTML::link("shitja/kode","Shto Kode për TV", array("class"=>"paglink"))}}
			{{HTML::link("shitja/malli","Hyrjet e Mallit", array("class"=>"paglink"))}}
			{{HTML::link("shitja","Faturat e Shitjes", array("class"=>"paglink"))}}
			{{HTML::link("shitja/shtoProdukt","Regjistro Produkt", array("class"=>"paglink","style"=>"color:orange"))}}
			{{HTML::link("shitja/shtoFature","Shto Faturë", array("class"=>"paglink"))}}
			{{HTML::link("shitja/shtoHyrje","Shto Hyrje", array("class"=>"paglink"))}}
		</div>
		<div id="edit">
			<h2>Regjistro Produkt</h2><br><br>
			<p style="color:#FF6666">@if(Session::has('msg'))
			{{Session::get('msg')}}<br><br>
			@endif<p>
			{{Form::open("shitja/shto/produkt", "POST")}}
			{{Form::label("emri","Produkti: ")}}<br>
			{{Form::text("emri")}}<br><br>
			{{Form::label("pershkrimi","Pershkrimi: ")}}<br>
			{{Form::textarea("pershkrimi","",array("rows"=>"3","cols"=>"18"))}}<br><br>
			{{Form::label("cmimi","Çmimi: ")}}<br>
			{{Form::text('cmimi',"",array('style'=>'width:100px;', 'onkeypress'=>'validate(event)','id'=>'cmimi'))}}<br>
			{{Form::image('/img/submit.png', '',array('style'=>'width:50px; height:30px;'))}}
			{{Form::close()}}
		</div>
	</div>
</div>
