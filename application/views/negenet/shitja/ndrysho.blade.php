<script>
				function validate(evt) {
			  var theEvent = evt || window.event;
			  var key = theEvent.keyCode || theEvent.which;
			  key = String.fromCharCode( key );
			  var regex = /[0-9]|\./;
			  if( !regex.test(key) ) {
			    theEvent.returnValue = false;
			    if(theEvent.preventDefault) theEvent.preventDefault();
			  }
			}

				window.onload = function(){
				document.getElementById("shitja").setAttribute("class","current");
				}
</script>
<?php $i = 0?>
@foreach(Produktet::all() as $produkt)	

	<input type="hidden" id="list{{$i}}" name="{{$produkt->id}}" value="{{$produkt->emri}}">
	<?php $i++; ?>
@endforeach	
<input type="hidden" id="numri" value="{{$i}}"/>

<?php $shitja = Shitja::where("id","=",$id)->first();
		$x = 0;
		$sum = 0;
?>
<div id="shitja-interface">
	<div id="shitja-main">
		<div id="top-shitja">
			{{HTML::link("shitja/stokuGjendja","Gjendja e Stokut", array("class"=>"paglink"))}}
			{{HTML::link("shitja/kode","Shto Kode për TV", array("class"=>"paglink"))}}
			{{HTML::link("shitja/malli","Hyrjet e Mallit", array("class"=>"paglink"))}}
			{{HTML::link("shitja","Faturat e Shitjes", array("class"=>"paglink"))}}
			{{HTML::link("shitja/shtoProdukt","Regjistro Produkt", array("class"=>"paglink"))}}
			{{HTML::link("shitja/shtoFature","Shto Faturë", array("class"=>"paglink"))}}
			{{HTML::link("shitja/shtoHyrje","Shto Hyrje", array("class"=>"paglink"))}}
		</div>
			<div id="edit">
			<h2>Ndrysho Faturë</h2><br><br>			
			<p style="color:#FF6666">@if(Session::has('msg'))
			{{Session::get('msg')}}<br><br>
			@endif</p>
			<p>@if(Session::has('success'))
			{{Session::get('success')}}<br><br>
			@endif</p>
			{{Form::open("shitja/edit", "POST")}}
			{{Form::label("bleresi","Bleresi: ")}}
			{{Form::text("bleresi",$shitja->bleresi)}}<br><br>
			<table style="width:auto;" id="inhere" name="">

			<tr>
				<th>Produkti</th><th>Pershkrimi</th><th>Sasia</th><th>Çmimi:</th>
				<!--th><a href="javascript:AddFormField('inhere','text','','','td','select'); kalkulo();" style="text-decoration:none;"
				<span style="font-weight:bold;color:white;font-size:1.3em;"> + </span></th-->
			</tr>
			@foreach(MalliShitja::where("shitja_id","=",$id)->get() as $malli)
			<tr>
				<td>
					<select name="produkti{{$x}}" id="produkti{{$x}}" onchange="valuesofAll(this.value)">
						<option value="{{$malli->p_id}}" selected>{{Produktet::where('id','=',$malli->p_id)->first()->emri}}</option>
						@foreach(Produktet::where("id","!=",$malli->p_id)->get() as $produkt)
						<option value="{{$produkt->id}}">{{$produkt->emri}}</option>
						@endforeach
					</select>
				</td>
				<td id="pershkrimi{{$x}}" class="pershkrimi">{{Produktet::where("id","=",$malli->p_id)->first()->pershkrimi}}</td>
				<td>
					{{Form::text('sasia'.$x,$malli->sasia,array('style'=>'width:50px;padding:0;margin:0;', 'onkeypress'=>'validate(event)',"class"=>"sasia" ,'id'=>'sasia'.$x))}}
				</td>
				<td>
					{{Form::text('cmimi'.$x,$malli->cmimi,array('style'=>'width:50px;padding:0;margin:0;', 'onkeypress'=>'validate(event)',"class"=>"cmimi",'id'=>'cmimi'.$x))}}
				</td>	
			</tr><input type="hidden" name="{{$x}}" value="{{$malli->id}}"/>
			<?php $sum += $malli->sasia*$malli->cmimi; 
				$x++;
			?>
			@endforeach
		</table>
		<span style="color:#f8f8f8;">Vlera: <span id="result">{{$sum}}</span>&euro;</span><br><br><br>
		{{Form::label("paguar","Paguar: ")}}<br>
		@if($shitja->paguar>0)
			{{Form::checkbox("paguar",'',array("checked"))}}<br>
		@else
			{{Form::checkbox("paguar")}}<br>
		@endif
		{{Form::label("fiskal","Numri fiskal")}}
		{{Form::text("fiskal")}}<br>
		{{Form::label("koment","Koment (opsional): ")}}
		{{Form::textarea("koment",$shitja->koment,array('style'=>'width:200px; height:50px;'))}}<br><br>
		{{Form::hidden("saHere","",array("id"=>"saHere"))}}
		{{Form::hidden("x",$x,array("id"=>"x"))}}
		{{Form::hidden("id",$id)}}
		{{Form::image('/img/submit.png', '',array('style'=>'width:50px; height:30px;'))}}
		{{Form::close()}}
		</div>
	</div>
</div>
<script>
	var BASE = "<?php echo URL::base(); ?>";
	var x = document.getElementById("x").value;

		$("#produkti0").change(function(){
			$.post(BASE+'/ajax',{data:$("#produkti0").val()},function(result){
				$("#pershkrimi0").text(result.pershkrimi);
			}, "json");	
		});

		$("#produkti1").change(function(){
			$.post(BASE+'/ajax',{data:$("#produkti1").val()},function(result){
				$("#pershkrimi1").text(result.pershkrimi);
			}, "json");	
		});

		$("#produkti2").change(function(){
			$.post(BASE+'/ajax',{data:$("#produkti2").val()},function(result){
				$("#pershkrimi2").text(result.pershkrimi);
			}, "json");	
		});

		$("#produkti3").change(function(){
			$.post(BASE+'/ajax',{data:$("#produkti3").val()},function(result){
				$("#pershkrimi3").text(result.pershkrimi);
			}, "json");	
		});

		$("#produkti4").change(function(){
			$.post(BASE+'/ajax',{data:$("#produkti4").val()},function(result){
				$("#pershkrimi4").text(result.pershkrimi);
			}, "json");	
		});

		$("#produkti5").change(function(){
			$.post(BASE+'/ajax',{data:$("#produkti5").val()},function(result){
				$("#pershkrimi5").text(result.pershkrimi);
			}, "json");	
		});

		$("#produkti6").change(function(){
			$.post(BASE+'/ajax',{data:$("#produkti6").val()},function(result){
				$("#pershkrimi6").text(result.pershkrimi);
			}, "json");	
		});

		$("#produkti7").change(function(){
			$.post(BASE+'/ajax',{data:$("#produkti7").val()},function(result){
				$("#pershkrimi7").text(result.pershkrimi);
			}, "json");	
		});

		$("#produkti8").change(function(){
			$.post(BASE+'/ajax',{data:$("#produkti8").val()},function(result){
				$("#pershkrimi8").text(result.pershkrimi);
			}, "json");	
		});

		$("#produkti9").change(function(){
			$.post(BASE+'/ajax',{data:$("#produkti9").val()},function(result){
				$("#pershkrimi9").text(result.pershkrimi);
			}, "json");	
		});

		$("#produkti10").change(function(){
			$.post(BASE+'/ajax',{data:$("#produkti10").val()},function(result){
				$("#pershkrimi10").text(result.pershkrimi);
			}, "json");	
		});

		$("#produkti11").change(function(){
			$.post(BASE+'/ajax',{data:$("#produkti11").val()},function(result){
				$("#pershkrimi11").text(result.pershkrimi);
			}, "json");	
		});

		$("#produkti12").change(function(){
			$.post(BASE+'/ajax',{data:$("#produkti12").val()},function(result){
				$("#pershkrimi12").text(result.pershkrimi);
			}, "json");	
		});

		$("#produkti13").change(function(){
			$.post(BASE+'/ajax',{data:$("#produkti13").val()},function(result){
				$("#pershkrimi13").text(result.pershkrimi);
			}, "json");	
		});

		$("#produkti14").change(function(){
			$.post(BASE+'/ajax',{data:$("#produkti14").val()},function(result){
				$("#pershkrimi14").text(result.pershkrimi);
			}, "json");	
		});

		$("#produkti15").change(function(){
			$.post(BASE+'/ajax',{data:$("#produkti15").val()},function(result){
				$("#pershkrimi15").text(result.pershkrimi);
			}, "json");	
		});

		$("#produkti16").change(function(){
			$.post(BASE+'/ajax',{data:$("#produkti16").val()},function(result){
				$("#pershkrimi16").text(result.pershkrimi);
			}, "json");	
		});

		$("#produkti17").change(function(){
			$.post(BASE+'/ajax',{data:$("#produkti17").val()},function(result){
				$("#pershkrimi17").text(result.pershkrimi);
			}, "json");	
		});

		$("#produkti18").change(function(){
			$.post(BASE+'/ajax',{data:$("#produkti18").val()},function(result){
				$("#pershkrimi18").text(result.pershkrimi);
			}, "json");	
		});

		$("#produkti19").change(function(){
			$.post(BASE+'/ajax',{data:$("#produkti19").val()},function(result){
				$("#pershkrimi19").text(result.pershkrimi);
			}, "json");	
		});
/*		
		function valuesofAll(val){
		     $.post(BASE+'/ajax',{data:val},function(result){
		     	$("#produkti0").change(function(){
		     		$("#pershkrimi0").text(result.pershkrimi);
		     	});
				
				$("#produkti1").change(function(){
		     		$("#pershkrimi1").text(result.pershkrimi);
		     	});	
		     }, "json"); //expect response as json..
		}*/
</script>
{{HTML::script('js/kalkulo.js')}}			
					
