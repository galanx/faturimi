					<table>
						<caption><button id="back" onclick="javascript:history.go(-1);">&lt;&lt;Mbrapa</button> Lista e arkivave.</caption>
					<tr><th>ID</th><th>User</th><th>Emri</th><th>Mbiemri</th><th>Adresa</th><th>Vendbanimi</th><th>Kompania</th><th>Tel</th><th>Paketa</th><th>Data e ndaljes</th><th></th></tr>
					@foreach($result as $user)
						<?php
							date_default_timezone_set('Europe/Tirane');
							$expiration_date = date('Y-m-d',strtotime($user->expiration));
							$current_date = date('Y-m-d');
							$expiration = strtotime($expiration_date);
							$current = strtotime($current_date);
							$threedays = $current + 259200;
							$class = "ok";
							
							if($user->enableuser == "1"){
								$class = "aktiv";
							}
							if($expiration<=$current){
								$class = "disabled";
							}
							if($expiration>$current&&$expiration<=$threedays){
								$class = "expired";
							}
							if($user->enableuser == "0"){
								$class = "arkiva";
							}
						?>	
						<tr>
							<td class="{{$class}}">{{Id::where("username","=", $user->username)->first()->id}}</td>
							<td>{{HTML::link_to_route('user_view',$user->username,$user->username)}}</td>
							<td>{{$user->firstname}}</td>
							<td>{{$user->lastname}}</td>
							<td>{{$user->address}}</td>
							<td>{{$user->city}}</td>
							<td>{{$user->company}}</td>
							<td>{{$user->mobile}}</td>
							<td><?php echo $paketa = ($user->srvid!=0)? Service::where("srvid", "=", $user->srvid)->first()->srvname:"" ?></td>
							<td onclick="window.open('/skadimi/{{$user->username}}','Skadimi','height=270, width=240, left=600px, top=180px, location=no, resizable=no')">{{date('d-M-Y', strtotime($user->expiration))}}</td>
							<td>@if($user->comment)
								<input type="image" src="/img/koment_active.png"  onclick="alert('{{$user->comment}}')" />
								@else
								<input type="image" src="/img/koment_passive.png" onclick="" />
								@endif
							</td>
						</tr>
					@endforeach
					</table>