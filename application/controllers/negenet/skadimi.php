<?php
	class Negenet_Skadimi_Controller extends Base_Controller{
		public $layout = "negenet.layouts.skadimi";
		public function action_skadimi($user){
			$this->layout->content = View::make("negenet.admin.skadimi")
			->with('user',$user);
		}

		public function action_edit(){
			$skadimi = Input::get("deri");
			$pse = Input::get("pse");
			$user = Input::get("user");
			date_default_timezone_set("Europe/Tirane");
			if($skadimi==""||$pse==""){
				return Redirect::back()
				->with("msg","Plotëso të gjitha!");
			}else{
				$query = DB::query("UPDATE rm_users SET expiration='".$skadimi."', comment='".$pse." (".Auth::user()->username." - ".date('d-M-Y H:i:s').")' WHERE username='".$user."'");
				if($query){
					//Log
					FaturimiLog::create(array(
						"type"=>"Ndryshim i skadimit",
						"event"=>"Tabela: rm_users, skadimi=".$skadimi.", klienti: ".$user." arsyeja: ".$pse,
						"author"=>Auth::user()->username,
						"date"=>date("Y-m-d H:i:s"),
					));	
					return Redirect::back()
					->with("success","Skadimi u ndryshua!");
				}else{
					return Redirect::back()
					->with("msg","Problem me databazë!");
				}
			}
			
		}

		public function action_tvmbushje(){
			$this->layout->content = View::make("negenet.shitja.mbushje");
		}

		public function action_dergombushje(){
			$emri = Input::get("emri");
			$tel = Input::get("tel");
			$smart = Input::get("smart");
			$periudha = Input::get("periudha");

			$mbushja = "";
			for($i=0;$i<$periudha;$i++){
				if(Kode::count("kodi")>0){
					$kodi = Kode::first();
					$mbushja = $kodi->kodi;
					$to = "info@tring.tv";
					$subject = "Abonim per TV";
					$message = "VCR ".$kodi->kodi." CA ".$smart."\n".$emri." Tel: ".$tel;

					$headers = 'From: NEGENET <shitja@negenet.com>'. "\r\n" .'Reply-To: NEGENET <shitja@negenet.com>';

					$mail = mail($to,$subject,$message,$headers);
					$mail = mail("feka.albert@gmail.com",$subject,$message,$headers);

					//Log
					FaturimiLog::create(array(
						"type"=>"Mbushje per TV",
						"event"=>"Tabela: Kode, kodi=".$mbushja.", klienti: ".$emri,
						"author"=>Auth::user()->username,
						"date"=>date("Y-m-d H:i:s"),
					));

					Kode::where("kodi","=",$kodi->kodi)->delete();
				}else{
					return Redirect::back()->with("msg","Nuk ka mbushje ne stok!");
				}
			}

			return Redirect::back()->with("success","Mbushja u dergua me sukses!");
		}
	}
?>