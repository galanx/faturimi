<?php
	class Negenet_Admin_Controller extends Base_Controller{

		//Ballina
		public function action_index(){

			foreach(User::all() as $user){
				$query = Id::where("username","=",$user->username)->first();
				if(!$query){
					Id::create(array("username" => $user->username));
				}
			}
			$this->layout->content =  View::make("negenet.admin.index")
			->with('users', User::where("enableuser","!=",0)->paginate());
		}

		//Rezultatet e kerkuara
		public function action_search(){
			$search = Input::get("search");
			$arkiva = Input::get("arkiva");
			$result = User::where("username","=",$search)->or_where("company","=",$search)->or_where("city","=",$search)->or_where("firstname","=",$search)->or_where("lastname","=",$search)->or_where("username","like",$search."%")->get();
				$this->layout->content = View::make("negenet.admin.search")
				->with("result", $result);	
			if($arkiva!= -1){
				date_default_timezone_set('Europe/Tirane');

				$current_date = date('Y-m-d H:i:s');
				$current = strtotime($current_date);
				$threedays = $current + 259200;
				$plusthree = date('Y-m-d H:i:s', $threedays);

				$result = User::where("enableuser","=",$arkiva)->get();
				if($arkiva==1){
					$result = User::where("enableuser","=","1")->where("expiration",">",$plusthree)->get();
				}
				if($arkiva==2){
					$result = User::where("enableuser","=","1")->where("expiration", ">",$current_date)->where("expiration","<=",$plusthree)->get();
				}
				if($arkiva==3){
					$result = User::where("expiration","<=",$current_date)->where("enableuser","=","1")->order_by("expiration","asc")->get();
				}
				$this->layout->content = View::make("negenet.admin.search")
				->with("result", $result);
			}
		}

		public function action_searchSingle(){
			$search = Input::get("search");

			$result = User::where("username","=",$search)->or_where("company","=",$search)->or_where("city","=",$search)->or_where("firstname","=",$search)->or_where("lastname","=",$search)->or_where("username","like",$search."%")->get();
			$this->layout->content = View::make("negenet.admin.search")
			->with("result", $result);	
		}

		//Transaksionet per user-in specifik
		public function action_user($user){
			$query = User::where("username","=",$user)->first();
			$adresa = $query->address;
			$paketa = Service::where("srvid","=",$query->srvid)->first()->srvname;
			$ip = $query->staticipcpe;
			$tel = $query->mobile;
			$kompania = $query->company;
			$skadimi = date("d-m-Y",strtotime($query->expiration));
			$class = "ok";

			$providers = User::where("comment","like","%provi%")->get();

			foreach($providers as $provider){
				$prov_status = Id::where("username","=",$provider->username)->first();
				if($prov_status->provider!=1){
					$prov_status->provider = 1;
					$prov_status->save();
				}
			}

			$faturat = Fatura::where("username","=",$user)->get();
			foreach($faturat as $fatura){
				$query = Transaksion::where("fatura_id","=",$fatura->id)->first();
				if(!$query){
					$tr_id = Transaksion::where("username","=",$user)->count()?Transaksion::where("username","=",$user)->count():0;
					Transaksion::create(array("fatura_id" => $fatura->id, "username" => $user, "user_transaction_id"=>$tr_id+1));
				}
			}

			$pagesat = Pagesa::where('username',"=",$user)->get();
			foreach($pagesat as $pagesa){
				$query = Transaksion::where("pagesa_id","=",$pagesa->id)->first();
				if(!$query){
					$tr_id = Transaksion::where("username","=",$user)->count()?Transaksion::where("username","=",$user)->count():0;
					Transaksion::create(array("pagesa_id" => $pagesa->id, "username" => $user,"user_transaction_id"=>$tr_id+1));
					}
				}

			date_default_timezone_set("Europe/Tirane");
			$today = strtotime(date("Y-m-d"));
			$expiration = strtotime(date("Y-m-d",strtotime(User::where("username","=",$user)->first()->expiration)));
			$plusthree = $today+259200; 
			$month = strtotime(date("Y-m-d",$expiration)."+ 1 month")-259200;/*+2.63e+6*/
			$userId = Id::where("username","=",$user)->first();

			if($expiration<=$today){
				$class = "disabled";
			}
			if($expiration>$today&&$expiration<=$plusthree){
				$class = "expired";	
			}
			
			if(User::where("username","=",$user)->first()->enableuser=="0"){
				$class = "arkiva";
			}
			if($expiration<=$plusthree){
				//Automatizimi
				$exists = Fatura::where("deri",">",date('Y-m-d',($expiration-259200)))
				->where("username","=",$user)->first();
				$arkiva = User::where("username","=",$user)->first()->enableuser=="0";

				if(!$exists&&$userId->provider!=1&&$userId->status==1&&!$arkiva){
					$user_pako = User::where("username","=",$user)->first();
					$srvname = Service::where("srvid","=",$user_pako->srvid)->first();
					$cmimi_pako = Pako::where("emri","=",$srvname->srvname)->first()?
					Pako::where("emri","=",$srvname->srvname)->first()->cmimi:"";

					$automatizimi = Fatura::create(array(
						"username"=>$user,
						"prej"=>date('Y-m-d',($expiration-259200)),
						"deri"=>date('Y-m-d',$month),
						"data_krijimit"=>date("Y-m-d"),
						"Pershkrimi"=>"Parapagim për internet",
						"vlera"=>$cmimi_pako,
						"pako"=>$srvname->srvname,
						"periudha"=>1,
					));
					if($automatizimi){
						return Redirect::to_route("user_view",$user);
					}
				}
			}

			$result = Transaksion::where("username","=",$user)->get();
			$this->layout->content = View::make("negenet.admin.user")
			->with('result', $result)->with("username",$user)->with("adresa",$adresa)
			->with('paketa',$paketa)->with("ip",$ip)->with("tel",$tel)->with("kompania",$kompania)
			->with("skadimi",$skadimi)->with("class",$class);
		}

		//Pagesa
		public function action_pagesa($user){
			$this->layout->content = View::make("negenet.admin.pagesa")
			->with('username',$user);
		}

		//Shto pagese
		public function action_shtopag(){
			date_default_timezone_set("Europe/Tirane");
			$today = date("Y-m-d H:i:s");
			$shuma = Input::get("vlera");
			$fatura = Input::get("fatura");
			$komenti = Input::get("koment");
			$user = Input::get("user");

			if($shuma==0||$shuma==""){
				return Redirect::to_route("pagesa", $user)
				->with("msg","Shuma duhet të jetë më e madhe se 0!");
			}if($fatura==""||!Fatura::where("id","=",$fatura)->where("username","=",$user)->first()){
				return Redirect::to_route("pagesa", $user)
				->with("msg","Fatura nuk ekziston ose nuk është e ".$user."!");
			}else{
				$query = Pagesa::create(
					array(
						"vlera"=>$shuma,
						"fatura_id"=>$fatura,
						"koment"=>$komenti,
						"data" => $today,
						"username" => $user,
						)
				);

				$fat_pag = Fatura::where("id","=",$fatura)->first();
				$fat_pag->pagesa += $shuma;
				$fat_pag->save();	

				if($query){
					//Log
					FaturimiLog::create(array(
						"type"=>"Shtim pagese tek provideret",
						"event"=>"Tabela: Pagesa, vlera=".$shuma.", fatura nr.".$fatura.", provideri: ".$user."",
						"author"=>Auth::user()->username,
						"date"=>date("Y-m-d H:i:s"),
					));
					return Redirect::to_route("user_view", $user);
				}else{
					return Redirect::to_route("pagesa", $user)
					->with("msg","Pagesa nuk është regjistruar!");
				}
			}
		}

		//Zbritja
		public function action_zbritja($user){
			$this->layout->content = View::make("negenet.admin.zbritja")
			->with('username',$user);
		}

		//Shto zbritje
		public function action_shtozbr(){
			$shuma = Input::get('vlera');
			$fatura_id = Input::get('fatura');
			$komenti = Input::get('koment');
			$user = Input::get('user');

			if(!Fatura::where("id","=",$fatura_id)->first()){
				return Redirect::to_route("zbritja", $user)
				->with("msg","Fatura nuk ekziston!");
			}
			if($komenti==""){
				return Redirect::to_route("zbritja", $user)
				->with("msg","Arsyeja e zbritjës duhet të plotësohet!");
			}else{
				$query = Fatura::where("id","=",$fatura_id)->first();

				$query->zbritja = $shuma;
				$query->koment = $komenti;
				$query->save();

				if($query){
					//Log
					FaturimiLog::create(array(
						"type"=>"Zbritje ne fature",
						"event"=>"Tabela: Faturat, zbritja=".$shuma.", fatura nr.".$fatura_id.", klienti: ".$user."",
						"author"=>Auth::user()->username,
						"date"=>date("Y-m-d H:i:s"),
					));

					return Redirect::to_route("user_view",$user);
				}else{
					return Redirect::to_route("zbritja",$user)
					->with("msg","Zbritja nuk u regjistrua!");
				}
			}
		}
	}
?>

