<?php
	class Negenet_Faturat_Controller extends Base_Controller{

		//Fatura list
		public function action_index(){
			$result = Fatura::order_by("id","desc")->paginate();
			$this->layout->content = View::make("negenet.faturat.index")
			->with('result', $result);
		}

		//Fatura Search
		public function action_search(){
			$prej = Input::get("prej");
			$deri = Input::get("deri");
			
			if($prej!=""&&$deri!=""){
				$result = Fatura::where("prej",">=",$prej)->where("prej","<=",$deri)->order_by("prej","asc")->get();
			}
			if($prej==""&&$deri!=""){
				$result = Fatura::where("prej","<=",$deri)->order_by("prej","asc")->get();
			}
			if($prej!=""&&$deri==""){
				$result = Fatura::where("prej",">=",$prej)->order_by("prej","asc")->get();
			}else{
				$result = Fatura::where("prej",">=",$prej)->where("prej","<=",$deri)->order_by("prej","asc")->get();
			}		
			$this->layout->content = View::make("negenet.faturat.faturat")
			->with("result",$result);
		}

		//Shto fatura nese provider
		public function action_shto($user){
			$this->layout->content = View::make("negenet.faturat.shto")
			->with('user',$user);
		}
		public function action_shtofat(){
			$prej = Input::get("prej");
			$deri = Input::get("deri");
			$pershk = Input::get("pershkrimi");
			$vlera = Input::get("vlera");
			$user = Input::get("user");
			date_default_timezone_set('Europe/Tirane');
			if($prej==""||$deri==""||$vlera==""||$vlera==0){
				return Redirect::back()
				->with("msg","Duhet t'i plotësoni datat dhe vlerën!");
			}else{
				$query = Fatura::create(array(
					"username" => $user,
					"prej" => $prej,
					"deri" => $deri,
					"data_krijimit"=>date("Y-m-d"),
					"pershkrimi" => $pershk,
					"vlera" => $vlera,
				));

				//Log
				FaturimiLog::create(array(
					"type"=>"Shtim i fatures tek provideret",
					"event"=>"Tabela: Faturat, vlera=".$vlera.", prej: ".$prej.", deri: ".$deri.", pershkrimi: ".$pershk." klienti: ".$user,
					"author"=>Auth::user()->username,
					"date"=>date("Y-m-d H:i:s"),
				));
				return Redirect::to_route("user_view",$user);
			}
		}

		//Shto fatura nese klient
		public function action_shtofatKlientView($user){
			$this->layout->content = View::make("negenet.faturat.shtofatklientview")
			->with("user",$user);
		}
		public function action_shtofatKlient(){
			date_default_timezone_set("Europe/Tirane");
			$today = date("Y-m-d");
			$next_month = date("Y-m-d",strtotime($today."+ 1 month"));

			$user = Input::get('user');

			$kyqje = Input::get("kyqje");
			$cmimiKyqjes = Input::get("cmimiKyqjes");

			$zbrCheck = Input::get("zbritja");
			$zbritja = Input::get('vlera');
			$komenti = Input::get('koment');

			$tv = Input::get("tv");
			$mbushja = Input::get("mbushja");
			$tvKyqja = Input::get("tvKyqja");
			$cmimiKarteles = Input::get("cmimiK")!=""?Input::get("cmimiK"):0;
			$cmimiDekoderit = Input::get("cmimiD")!=""?Input::get("cmimiD"):0;
			$cmimiInstalimit = Input::get("cmimiI")!=""?Input::get("cmimiI"):0;
			$shifra = Input::get("shifra")!=""?Input::get("shifra"):"";
			$kartela = Kartela::where("kartela","=",$shifra)->where("username","!=",$user)->first();
			$tvstat = 0;

			$pako = Input::get("pako");
			//$periudha = Input::get("periudha");

			$pagesa = Input::get("pagesa");
			$shuma = Input::get("shuma");
			$pagKoment = Input::get("pagKoment");

			$pershkrimi = "Parapagim për internet";

			$e_rregullt = Input::get("rregullt");
			$rregullt = 0;
			$id_rregullt="";

			$username = User::where("username","=",$user)->first();
			
			$total = 0;

			if(isset($kyqje)&&($cmimiKyqjes=="")){
				return Redirect::back()
				->with("msg","Vlera e kyqjes nuk duhet të jetë e zbrazet!");
			}else if(isset($kyqje)){
				$pershkrimi .= " + Kyqje ";
				$total += $cmimiKyqjes;
			}
			if(isset($zbrCheck)&&$komenti==""){
				return Redirect::back()
				->with("msg","Arsyeja e zbritjes është e detyrueshme!");
			}
			if(isset($tv)&&($mbushja==""||$mbushja==0)){
				return Redirect::back()
				->with("msg","Abonimi duhet të ketë kodin e mbushjes!");
			}else if(isset($tv)&&($mbushja!=""||$mbushja!=0)){
				$tvstat = 1;
				$pershkrimi	.= " + TV abonim ";
			}
			if(isset($pagesa)&&($shuma==""||$shuma==0)){
				return Redirect::back()
				->with("msg","Pagesa duhet të jetë më e madhe se 0!");
			}
			if(isset($tvKyqja)){
				$pershkrimi	.= " & TV Kyqje.";

				if($cmimiKarteles>0){
					$pershkrimi	.= " + Kartele ";

					$stoku = Produktet::where("emri","like","%kartel%")->first();

					if($stoku){
						$stoku->gjendja -= 1;
						$stoku->save();
					}
				}
				if($cmimiDekoderit>0){
					$pershkrimi	.= " + Dekoder ";

					$stoku = Produktet::where("emri","like","%dekoder%")->first();
					if($stoku){
						$stoku->gjendja -= 1;
						$stoku->save();
					}
				}
				$total += $cmimiKarteles+$cmimiDekoderit+$cmimiInstalimit;
			}
			if($pako==""&&!isset($pagesa)){
				return Redirect::back()
				->with("msg","Ju lutem zgjidheni pakon!");
			}else{
				$pakoid = Pako::where("emri","=",$pako)->first()->srvid;
				$query = DB::query("UPDATE rm_users SET srvid=$pakoid WHERE username = '".$user."'");

				$paketa = Pako::where("emri","=",$pako)->first();
				$total += $paketa->cmimi;
			}
			if($kartela&&isset($tvKyqja)){
				return Redirect::back()
				->with("msg","Kartela është në përdorim nga ".$kartela->username."!");
			}
			if(isset($e_rregullt)){
				$rregullt=1;
				$muaji_ri = date("m",strtotime($today));
				$viti_ri = date("Y",strtotime($today));
				
				$fat_rregullt_count = Fatura::where("rregullt","=",1)
				->where("muaji_rregullues","=",$muaji_ri)
				->where("viti_rregullues","=", $viti_ri)
				->order_by("id_rregullt","desc")->first()?
				Fatura::where("rregullt","=",1)
				->where("muaji_rregullues","=",$muaji_ri)
				->where("viti_rregullues","=", $viti_ri)
				->order_by("id_rregullt","desc")->first()->id_rregullt+1:1;

				$id_rregullt = $fat_rregullt_count;
			}

			$prej = date("Y-m-d",strtotime($username->expiration."- 3 days"));
			$deri = date("Y-m-d",strtotime($prej."+ ".$paketa->oferta_mujore." months"));
			$nr_fatures = Fatura::order_by("id","desc")->first()?Fatura::order_by("id","desc")->first()->id+1:1;

			if(!Fatura::where("prej","=",$prej)->where("username","=",$user)->first()){

				if(isset($tv)){

				if(!Kode::where("kodi","like","%".$mbushja."%")->first()&&!Tv::where("kodi","like","%".$mbushja."%")->first()){
					return Redirect::back()
					->with("msg","Kodi i mbushjes është i gabuar");
				}	

				if(Kartela::where("username","=",$username->username)->first()||(isset($tvKyqja)&&$shifra!="")){
					$kartela = Kartela::where("username","=",$username->username)->first()?
					Kartela::where("username","=",$username->username)->first()->kartela:$shifra;

					$to = "info@tring.tv";
					$subject = "Abonim per TV";
					$message = "VCR ".$mbushja." CA ".$kartela."\n".$username->firstname." ".$username->lastname." Tel: ".$username->mobile;

					$headers = 'From: NEGENET <shitja@negenet.com>'. "\r\n" .'Reply-To: NEGENET <shitja@negenet.com>';

					$mail = mail($to,$subject,$message,$headers);
					$mail = mail("feka.albert@gmail.com",$subject,$message,$headers);
					
				}else{
					return Redirect::back()
					->with("msg", "Mbushja nuk mund të dërgohet sepse personi nuk posedon SMART Kartelë");
				}
					$register = Tv::create(array(
						"cmimi_kartele"=>$cmimiKarteles,
						"cmimi_dekoder"=>$cmimiDekoderit,
						"cmimi_instalimit"=>$cmimiInstalimit,
						"username" => $user,
						"abonim" => $paketa->oferta_mujore,
						"kodi" => $mbushja,
						"data"=>$today,
						"fatura_id"=>$nr_fatures,
					));

					//Log
					FaturimiLog::create(array(
						"type"=>"Mbushje per TV",
						"event"=>"Tabela: Kode, kodi=".$mbushja.", klienti: ".$user,
						"author"=>Auth::user()->username,
						"date"=>date("Y-m-d H:i:s"),
					));	
					//Perdor mbushjen
					Kode::where("kodi","like","%".$mbushja."%")->delete();
					//-----

					$kartela_usr = Kartela::where("username","=",$user)->first();
					if($kartela_usr&&$shifra!=""){
						$kartela_usr->kartela = $shifra;
						$kartela_usr->save();
					}else{
						$kartela = Kartela::create(array(
							"kartela" => $shifra,
							"username"=>$user,
							"data_blerjes"=>$today,
						));
					}
				}
				
				if(isset($pagesa)){
					date_default_timezone_set("Europe/Tirane");
					$paguar = Pagesa::create(array(
						"vlera" => $shuma,
						"data" => date("Y-m-d H:i:s"),
						"username" => $user,
						"koment" => $pagKoment,
						"fatura_id" => $nr_fatures,
					));

					//Log
					FaturimiLog::create(array(
						"type"=>"Pagese interneti",
						"event"=>"Tabela: Pagesat, vlera=".$shuma.", fatura nr.".$nr_fatures.", klienti: ".$user."",
						"author"=>Auth::user()->username,
						"date"=>date("Y-m-d H:i:s"),
					));

					$expiration = date("Y-m-d",strtotime($deri) + 259200);
					if($paguar){
						DB::query("UPDATE rm_users SET expiration = '".$expiration."' WHERE username='".$user."'");
					}
				}	

				$query = Fatura::create(array(
					'data_krijimit'=>$today,
					'prej'=>$prej,
					'deri'=>$deri,
					'pershkrimi'=>$pershkrimi,
					'vlera'=>$total,
					'kyqja'=>$cmimiKyqjes,
					'periudha'=>$paketa->oferta_mujore,
					'koment'=>$komenti,
					'zbritja'=>$zbritja,
					'username'=>$user,
					'tv'=>$tvstat,
					'pako'=>$paketa->emri,
					'pagesa'=>$shuma,
					"rregullt"=>$rregullt,
					"id_rregullt"=>$id_rregullt,
					"muaji_rregullues"=>date("m",strtotime($prej)),
					"viti_rregullues"=>date("Y",strtotime($prej)),
				));

					//Log
					FaturimiLog::create(array(
						"type"=>"Shtim fature per internet",
						"event"=>"Tabela: Faturat, vlera=".$shuma.", pershkrimi: ".$pershkrimi.", klienti: ".$user.", prej: ".$prej.", deri: ".$deri." , kyqja=".$cmimiKyqjes.", periudha=".$paketa->oferta_mujore." muaj , zbritja = ".$zbritja." , pako = ".$paketa->emri." tv=".$tvstat.", e_rregullt=".$rregullt,
						"author"=>Auth::user()->username,
						"date"=>date("Y-m-d H:i:s"),
					));
				return Redirect::to_route("user_view",$user);
			}else{
				return Redirect::back()
				->with("msg","Fatura për muajin ".date("d/m/Y",strtotime($prej))." - ".date("d/m/Y",strtotime($deri))." ekziston!");
			}
		}

		//Fatura edit view
		public function action_edit($id){
			$fatura = Fatura::where("id",'=',$id)->first();
			$this->layout->content = View::make("negenet.faturat.edit")
			->with('fatura', $fatura);
		}

		//Nrysho faturen
		public function action_ndrysho(){

			date_default_timezone_set("Europe/Tirane");
			$today = date("Y-m-d");
			$next_month = date("Y-m-d",strtotime($today." + 1 month"));
			//Query
			$fatura = Fatura::where("id",'=',Input::get('id'))->first();
			$service = User::where("username","=",$fatura->username)->first();
			$user = Id::where("username","=",$fatura->username)->first();
			if($user->provider!=0){
				$fatura->prej = Input::get("prej")!=""?Input::get("prej"):$fatura->prej;
				$fatura->deri = Input::get("deri")!=""?Input::get("deri"):$fatura->deri;
				$fatura->pershkrimi = Input::get("pershkrimi");
				$fatura->providerkoment = Input::get("komentp")!=""?Input::get("komentp"):$fatura->koment;
				$fatura->vlera = Input::get("cmimiProv")!=""?Input::get("cmimiProv"):$fatura->vlera;
				$fatura->save();

				//Log
					FaturimiLog::create(array(
						"type"=>"Ndryshim fature tek provideret",
						"event"=>"Tabela: Faturat, vlera=".(Input::get("cmimiProv")!=""?Input::get("cmimiProv"):$fatura->vlera).", prej: ".(Input::get("prej")!=""?Input::get("prej"):$fatura->prej).", deri: ".(Input::get("deri")!=""?Input::get("deri"):$fatura->deri)." fatura nr.".$fatura->id,
						"author"=>Auth::user()->username,
						"date"=>date("Y-m-d H:i:s"),
					));
			}
			
			//Zbritja
			$zbrCheck = Input::get("zbritja");

			//E rregullt
			$e_rregullt = Input::get("rregullt");
			//Kyqje Internet
			$kyqje = Input::get("kyqje");

			//TV
			$tv = Input::get("tv");

			//Pagesa
			$pagesa = Input::get("pagesa");

			//Pako
			$pako = Input::get("pako");
			//$periudha = Input::get("periudha");

			$dite=0;
			//Status
			$status = Input::get("pauze");
			$pauzakoment = Input::get("pauzakoment");
			if(isset($status)){
				$dite = Input::get("dite");

				if($dite==""){
					return Redirect::back()
					->with("msg","Data e pauzës nuk duhet të lihet zbrazët!");
				}
				if($pauzakoment==""){
					return Redirect::back()
					->with("msg","Komenti i pauzës nuk duhet të lihet zbrazët!");
				}
			}
			if($pako==""&&$user->provider!=1&&!isset($pagesa)){
				return Redirect::back()
				->with("msg","Ju lutem zgjidheni pakon!");
			}

			if($dite!=""){
				if($fatura->pauza==0){
					$fatura->pauza = date("d",strtotime($dite)-strtotime($fatura->prej));
				}
					
				$fatura->pauzakoment = $pauzakoment;
				$fatura->save();
				$fatura->data_re=$dite;
				if($fatura->data_vjeter=="0000-00-00"||$fatura->data_vjeter == ""){
					$fatura->data_vjeter = $fatura->prej;
				}	
				$fatura->prej = $fatura->data_re;
				$fatura->save();

					//Log
					FaturimiLog::create(array(
						"type"=>"Pauze",
						"event"=>"Tabela: Faturat, pauza=".$fatura->pauza." dite, klienti=".$fatura->username.", fatura nr.".$fatura->id,
						"author"=>Auth::user()->username,
						"date"=>date("Y-m-d H:i:s"),
					));
			}else if(!isset($status)){
				if($fatura->pauza>0){
					$fatura->prej = $fatura->data_vjeter;
				}		
				$fatura->data_vjeter="";
				$fatura->data_re="";
				$fatura->pauza = 0;
				$fatura->pauzakoment = "";
				$fatura->save();
			}

			if(isset($e_rregullt)&&$fatura->rregullt==0){
				$muaji_ri = date("m",strtotime($fatura->prej));
				$viti_ri = date("Y",strtotime($fatura->prej));

				$fat_rregullt_count = Fatura::where("rregullt","=",1)
				->where("muaji_rregullues","=",$muaji_ri)
				->where("id","!=",$fatura->id)
				->where("viti_rregullues","=", $viti_ri)
				->order_by("id_rregullt","desc")->first()?
				Fatura::where("rregullt","=",1)
				->where("muaji_rregullues","=",$muaji_ri)
				->where("id","!=",$fatura->id)
				->where("viti_rregullues","=", $viti_ri)
				->order_by("id_rregullt","desc")->first()->id_rregullt+1:1;

				$fatura->muaji_rregullues = date("m",strtotime($fatura->prej));
				$fatura->viti_rregullues = date("Y",strtotime($fatura->prej));
				$fatura->rregullt=1;
				$fatura->id_rregullt = $fat_rregullt_count;
				$fatura->save();

				//Log
					FaturimiLog::create(array(
						"type"=>"Ndryshim fature - Rregullt/Pa-rregullt",
						"event"=>"Tabela: Faturat, klienti: ".$fatura->username.", fatura nr.".$fatura->id,
						"author"=>Auth::user()->username,
						"date"=>date("Y-m-d H:i:s"),
					));
			}else if(!isset($e_rregullt)){
				$fatura->rregullt=0;
				$fatura->id_rregullt = "";
				$fatura->save();
			}


			//Pako check
			if($pako!=""){
				$paketa = Pako::where("emri","=",$pako)->first();
				if($fatura->data_re!="000-00-00"&&$fatura->pauza>0){
					$time = strtotime($fatura->data_re."+ ".$paketa->oferta_mujore." month");
				}else{
					$time = strtotime($fatura->prej."+ ".$paketa->oferta_mujore." month");
				}
					
				$fatura->deri = date("Y-m-d",$time);

				$fatura->vlera = $paketa->cmimi;
				/*if($periudha==0||$periudha==""){		
					return Redirect::back()
					->with("msg","Periudha duhet të jetë minimum 1 muaj!");
				}else{
					$fatura->periudha = $periudha;
				}*/
				$fatura->periudha = $paketa->oferta_mujore;	
				$fatura->pako = $paketa->emri;
				$fatura->save();
				$pakoid = Pako::where("emri","=",$pako)->first()->srvid;

				if($service&&$fatura->id==Fatura::where("username","=",$fatura->username)->max('id')){
					$query = DB::query("UPDATE rm_users SET srvid=$pakoid WHERE username = '".$fatura->username."'");
				}else{
					return Redirect::back()
					->with("msg","Pako nuk mundi te ndërrohet sepse fatura është e vjetër.");
				}
			}
			
			//Kyqje check
			if(isset($kyqje)){
				$cmimiKyqjes = Input::get("cmimiKyqjes");
				if($cmimiKyqjes==""){
					return Redirect::back()
					->with('msg', 'Çmimi i kyqjes duhet të plotësohet nëse zgjidhet kyqja!');
				}else{
					$fatura->kyqja = $cmimiKyqjes;
					if(!strpos($fatura->pershkrimi," + Kyqje ")!==false){
						$fatura->pershkrimi .= " + Kyqje ";
					}
					
					$fatura->vlera += $cmimiKyqjes;
					$fatura->save();
				}

				//Log
					FaturimiLog::create(array(
						"type"=>"Ndryshim fatura - kyqje",
						"event"=>"Tabela: Faturat, vlera e kyqjes=".$cmimiKyqjes." , klienti: ".$fatura->username.", fatura nr.".$fatura->id,
						"author"=>Auth::user()->username,
						"date"=>date("Y-m-d H:i:s"),
					));
			}else if(!isset($pagesa)){
				$fatura->pershkrimi = str_replace(" + Kyqje ","",$fatura->pershkrimi);
				//$fatura->vlera -= $fatura->kyqja;
				$fatura->kyqja = 0;
				$fatura->save();
			}
			//Zbritja check
			if(isset($zbrCheck)){
				$zbritja = Input::get('vlera');
				$komenti = Input::get('koment');
				if($komenti==""){
					return Redirect::back()
					->with('msg', 'Arsyeja e zbritjës duhet të plotësohet nëse zgjidhet zbritja!');
				}else{
					$fatura->koment = $komenti;						
					$fatura->zbritja = $zbritja;
					$fatura->save();

					//Log
					FaturimiLog::create(array(
						"type"=>"Zbritje",
						"event"=>"Tabela: Faturat, zbritja=".$zbritja.", klienti: ".$fatura->username.", fatura nr.".$fatura->id,
						"author"=>Auth::user()->username,
						"date"=>date("Y-m-d H:i:s"),
					));
				}
			}else if(!isset($zbrCheck)){
				$fatura->koment = '';
				$fatura->zbritja = 0;
				$fatura->save();
			}

			//TV check
			if(isset($tv)){
				date_default_timezone_set("Europe/Tirane");
				$today = date("Y-m-d");
				$mbushja = Input::get("mbushja");

				if(!Kode::where("kodi","like","%".$mbushja."%")->first()&&!Tv::where("kodi","like","%".$mbushja."%")->first()){
					return Redirect::back()
					->with("msg","Kodi i mbushjes është i gabuar");
				}

				if(!strpos($fatura->pershkrimi," + TV abonim ")!==false){
					if(Id::where("username","=",$fatura->username)->first()->provider!=1){
						$fatura->pershkrimi .= " + TV abonim ";
					}
					$fatura->save();
				}
				
				$shifra = Input::get("shifra");
				$cmimiKarteles = Input::get("cmimiK")!=""?Input::get("cmimiK"):0;
				$cmimiDekoderit = Input::get("cmimiD")!=""?Input::get("cmimiD"):0;
				$cmimiInstalimit = Input::get("cmimiI")!=""?Input::get("cmimiI"):0;

				if($mbushja==""||$mbushja==0){
					return Redirect::back()
					->with('msg', 'Abonimi duhet të ketë mbushjen!');
				}

				$tv = Tv::where("fatura_id","=",$fatura->id)->first();
				$tvKyqja = Input::get("tvKyqja");

					if(!$tv){
						if(Kartela::where("username","=",$fatura->username)->first()||(isset($tvKyqja)&&$shifra!="")){
							$kartela = Kartela::where("username","=",$fatura->username)->first()?
							Kartela::where("username","=",$fatura->username)->first()->kartela:$shifra;

							$to = "info@tring.tv";
							$subject = "Abonim per TV";
							$message = "VCR ".$mbushja." CA ".$kartela."\n".$service->firstname." ".$service->lastname." Tel: ".$service->mobile;
							$headers = 'From: NEGENET <shitja@negenet.com>' . "\r\n" .
												'Reply-To: NEGENET <shitja@negenet.com>';

							$mail = mail($to,$subject,$message,$headers);
							$mail = mail("feka.albert@gmail.com",$subject,$message,$headers);
							
							//Log
							FaturimiLog::create(array(
								"type"=>"Mbushje per TV",
								"event"=>"Tabela: Kode, kodi=".$mbushja.", klienti: ".$fatura->username,
								"author"=>Auth::user()->username,
								"date"=>date("Y-m-d H:i:s"),
							));					

							Kode::where("kodi","like","%".$mbushja."%")->delete();
						}else{
							return Redirect::back()
							->with("msg","Mbushja nuk mund të dërgohet sepse personi nuk posedon SMART Kartelë");
						}
						$register = Tv::create(array(
							"cmimi_kartele"=>$cmimiKarteles,
							"cmimi_dekoder"=>$cmimiDekoderit,
							"cmimi_instalimit"=>$cmimiInstalimit,
							"username" => $fatura->username,
							"abonim" => $pako==""?1:$paketa->oferta_mujore,
							"kodi" => $mbushja,
							"data"=>$today,
							"fatura_id"=>$fatura->id,
						));
					}else{
						$tv->abonim = $pako==""?1:$paketa->oferta_mujore;
						if(Kartela::where("username","=",$fatura->username)->first()||(isset($tvKyqja)&&$shifra!="")){
							if(Kode::where("kodi","like","%".$mbushja."%")->first()){
								
								$kartela = Kartela::where("username","=",$fatura->username)->first()?
								Kartela::where("username","=",$fatura->username)->first()->kartela:$shifra;
								
								$to = "info@tring.tv";
								$subject = "Abonim per TV";
								$message = "VCR ".$mbushja." CA ".$kartela."\n".$service->firstname." ".$service->lastname." Tel: ".$service->mobile;

								$headers = 'From: NEGENET <shitja@negenet.com>' . "\r\n" .
													'Reply-To: NEGENET <shitja@negenet.com>';

								$mail = mail($to,$subject,$message,$headers);
								$mail = mail("feka.albert@gmail.com",$subject,$message,$headers);

								//Log
								FaturimiLog::create(array(
									"type"=>"Mbushje per TV",
									"event"=>"Tabela: Kode, kodi=".$mbushja.", klienti: ".$fatura->username,
									"author"=>Auth::user()->username,
									"date"=>date("Y-m-d H:i:s"),
								));					

								Kode::where("kodi","like","%".$mbushja."%")->delete();
							}
						}else{
							return Redirect::back()
							->with("msg","Mbushja nuk mund të dërgohet sepse personi nuk posedon SMART Kartelë");
						}

						$tv->kodi = $mbushja;
						$tv->save();
					}


				if(isset($tvKyqja)){
					if(!strpos($fatura->pershkrimi," & TV Kyqje.")!==false){
						if(Id::where("username","=",$fatura->username)->first()->provider!=1){
							$fatura->pershkrimi .= " & TV Kyqje.";
						}	
						$fatura->save();
					}
					
					if($cmimiKarteles>0&&!strpos($fatura->pershkrimi," + Kartele ")){
						$fatura->pershkrimi .= " + Kartele ";
						$fatura->save();

						$stoku = Produktet::where("emri","like","%kartel%")->first();
						if($stoku){
							$stoku->gjendja -= 1;
							$stoku->save();
						}
					}else if($cmimiKarteles==""||$cmimiKarteles==0){
						if(strpos($fatura->pershkrimi," + Kartele ")){
							$stoku = Produktet::where("emri","like","%kartel%")->first();

							if($stoku){
								$stoku->gjendja += 1;
								$stoku->save();
							}	
						}	
						$fatura->pershkrimi = str_replace(" + Kartele ","",$fatura->pershkrimi);
						$fatura->save();
					}

					if($cmimiDekoderit>0&&!strpos($fatura->pershkrimi," + Dekoder ")){
						$fatura->pershkrimi .= " + Dekoder ";
						$fatura->save();

						$stoku = Produktet::where("emri","like","%dekoder%")->first();
						if($stoku){
							$stoku->gjendja -= 1;
							$stoku->save();
						}

					}else if($cmimiDekoderit==""||$cmimiDekoderit==0){

						if(strpos($fatura->pershkrimi," + Dekoder ")){
							$stoku = Produktet::where("emri","like","%dekoder%")->first();
							if($stoku){
								$stoku->gjendja += 1;
								$stoku->save();	
							}
						}							

						$fatura->pershkrimi = str_replace(" + Dekoder ","",$fatura->pershkrimi);
						$fatura->save();
					}

						$kartela = Kartela::where("kartela","=",$shifra)->where("username","!=",$fatura->username)->first();
						if($kartela){
							return Redirect::back()
							->with("msg","Kartela është në përdorim nga ".$kartela->username."!");
						}else{
							$kartela_usr = Kartela::where("username","=",$fatura->username)->first();
							if($kartela_usr){
								$kartela_usr->kartela = $shifra;
								$kartela_usr->save();
							}else{
								$kartela = Kartela::create(array(
									"kartela" => $shifra,
									"username"=>$fatura->username,
									"data_blerjes"=>$today,
								));
							}
						}
						if($tv){
							$tv->cmimi_dekoder = $cmimiDekoderit;
							$tv->cmimi_kartele = $cmimiKarteles;
							$tv->cmimi_instalimit = $cmimiInstalimit;
							$tv->save();
							$fatura->vlera +=($tv->cmimi_dekoder+$tv->cmimi_kartele+$tv->cmimi_instalimit);
						}else{
							$fatura->vlera +=($cmimiDekoderit+$cmimiKarteles+$cmimiInstalimit);
						}
						$fatura->save();

				}else if(!isset($pagesa)){
					if($tv){

						if(strpos($fatura->pershkrimi," + Dekoder ")){
							$stoku = Produktet::where("emri","like","%dekoder%")->first();
							if($stoku){
								$stoku->gjendja += 1;
								$stoku->save();	
							}
						}

						if(strpos($fatura->pershkrimi," + Kartele ")){
							$stoku = Produktet::where("emri","like","%kartel%")->first();
							if($stoku){
								$stoku->gjendja += 1;
								$stoku->save();	
							}
						}

						$fatura->pershkrimi = str_replace(" & TV Kyqje.","",$fatura->pershkrimi);
						$fatura->pershkrimi = str_replace(" + Dekoder ","",$fatura->pershkrimi);
						$fatura->pershkrimi = str_replace(" + Kartele ","",$fatura->pershkrimi);
						$tv->cmimi_dekoder = 0;
						$tv->cmimi_kartele = 0;
						$tv->cmimi_instalimit = 0;
						$tv->save();
						$fatura->save();
					}
				}

				$fatura->tv = 1;
				$fatura->save();

				//Log
					FaturimiLog::create(array(
						"type"=>"Ndryshim fature - TV",
						"event"=>"Tabela: Faturat, klienti: ".$fatura->username." , fatura nr.".$fatura->id,
						"author"=>Auth::user()->username,
						"date"=>date("Y-m-d H:i:s"),
					));
			}else if(!isset($pagesa)){
				$tv = Tv::where("fatura_id","=",$fatura->id)->first();
				
					$fatura->pershkrimi = str_replace(" + TV abonim ","",$fatura->pershkrimi);
					$fatura->pershkrimi = str_replace(" & TV Kyqje.","",$fatura->pershkrimi);
					$fatura->pershkrimi = str_replace(" + Dekoder ","",$fatura->pershkrimi);
					$fatura->pershkrimi = str_replace(" + Kartele ","",$fatura->pershkrimi);
				if($tv){	
					$tv_delete = TV::where("fatura_id","=",$fatura->id)->delete();
					$fatura->tv = 0;
					
				}$fatura->save();
			}

			//Pagesa check
			if(isset($pagesa)){
				date_default_timezone_set("Europe/Tirane");
				$today = date("Y-m-d H:i:s");

				$shuma = Input::get("shuma");
				$pagKoment = Input::get("pagKoment");

				if($shuma==""||$shuma==0){
					return Redirect::back()
					->with("msg", "Pagesa nuk mund të jetë 0!");
				}else{
					$paguar = Pagesa::create(array(
						"vlera" => $shuma,
						"data" => $today,
						"username" => $fatura->username,
						"koment" => $pagKoment,
						"fatura_id" => $fatura->id,
					));
					
					//Log
					FaturimiLog::create(array(
						"type"=>"Pagese",
						"event"=>"Tabela: Pagesat, vlera=".$shuma.", klienti: ".$fatura->username.", fatura nr.".$fatura->id,
						"author"=>Auth::user()->username,
						"date"=>date("Y-m-d H:i:s"),
					));

					$fatura->pagesa += $shuma;
					$fatura->save();

					$pag_fatura = Pagesa::where("fatura_id","=",$fatura->id)->get();
					$count = 0;
					foreach($pag_fatura as $fat){
						$count++;
					}

					$expiration = date("Y-m-d",strtotime($fatura->deri) + 259200);
					$shtyj = User::where("username","=",$fatura->username)
					->first();
					
					if($shtyj->expiration<$expiration){
						if($count<2){
							DB::query("UPDATE rm_users SET expiration = '".$expiration."' WHERE username='".$fatura->username."'");
						}	
					}
				}
			}
			return Redirect::to_route("user_view",$fatura->username);
		}
	}
?>