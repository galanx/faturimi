<?php
	class Negenet_FaturimiLog_Controller extends Base_Controller{

		public function action_index(){
			$result = FaturimiLog::order_by("id","desc")->paginate();
			$this->layout->content = View::make("negenet.faturimilog.index")
			->with("result",$result);
		}

		public function action_search(){
			$prej = Input::get("prej");
			$deri = Input::get("deri");
			
			if($prej!=""&&$deri!=""){
				$result = FaturimiLog::where("date",">=",$prej)->where("date","<=",$deri)->order_by("date","asc")->get();
			}
			if($prej==""&&$deri!=""){
				$result = FaturimiLog::where("date","<=",$deri)->order_by("date","asc")->get();
			}
			if($prej!=""&&$deri==""){
				$result = FaturimiLog::where("date",">=",$prej)->order_by("date","asc")->get();
			}else{
				$result = FaturimiLog::where("date",">=",$prej)->where("date","<=",$deri)->order_by("date","asc")->get();
			}		
			$this->layout->content = View::make("negenet.faturimilog.search")
			->with("result",$result);
		}
	}
?>