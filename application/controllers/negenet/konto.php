<?php
	class Negenet_Konto_Controller extends Base_Controller{

		public function action_index(){
			$result = Admin::all();
			$this->layout->content = View::make("negenet.konto.index")
			->with("result",$result);
		}

		public function action_role(){
			$result = Role::all();
			$this->layout->content = View::make("negenet.konto.role")
			->with("result",$result);
		}

		public function action_roleClick(){
			$result = Role::all();
			$this->layout->content = View::make("negenet.konto.role")
			->with("result",$result);
		}

		public function action_info(){
			$this->layout->content = View::make("negenet.konto.info");
		}

		public function action_shto(){
			$this->layout->content = View::make("negenet.konto.shto");
		}

		public function action_ndryshoKonto($id){
			$this->layout->content = View::make("negenet.konto.ndryshokonto")
			->with("id",$id);
		}

		public function action_shtoRole(){
			$this->layout->content = View::make("negenet.konto.shtorole");
		}

		public function action_ndryshoRole($id){
			$this->layout->content = View::make("negenet.konto.ndryshorole")
			->with("id",$id);
		}

		public function action_kontoInfo(){
			$user = Input::get("user");
			$password = Input::get("password");
			$confirm = Input::get("confirm");

			if($user==""){
				return Redirect::back()
				->with("msg","Ju lutem plotësoni emrin!");
			}
			if($password==""){
				return Redirect::back()
				->with("msg","Ju lutem plotësojeni fjalëkalimin");
			}
			if($password!=$confirm){
				return Redirect::back()
				->with("msg","Fjalëkalimi duhet të konfirmohet");
			}

			$query = Admin::where("id","=",Auth::user()->id)->first();

			if($query){
				$query->username = $user;
				$query->password = Hash::make($password);
				$query->save();

					//Log
					FaturimiLog::create(array(
						"type"=>"Konto - Ndryshim i te dhenave",
						"event"=>"Tabela: Konto, user=".$user.", password=".Hash::make($password),
						"author"=>Auth::user()->username,
						"date"=>date("Y-m-d H:i:s"),
					));
				return Redirect::back()
				->with("success","Te dhënat u ndryshuan me sukses!");
			}
		}

		public function action_kontoShto(){
			$user = Input::get("user");
			$password = Input::get("password");
			$confirm = Input::get("confirm");
			$role = Input::get("rolet");

			if($user==""){
				return Redirect::back()
				->with("msg","Ju lutem plotësoni emrin!");
			}
			if(Admin::where("username","=",$user)->first()){
				return Redirect::back()
				->with("msg","Konto ekziston!");
			}
			if($password==""){
				return Redirect::back()
				->with("msg","Ju lutem plotësojeni fjalëkalimin");
			}
			if($password!=$confirm){
				return Redirect::back()
				->with("msg","Fjalëkalimi duhet të konfirmohet");
			}if($role==""){
				return Redirect::back()
				->with("msg", "Roli duhet të zgjidhet!");
			}

			$query = Admin::create(array(
					"username"=>$user,
					"password"=>Hash::make($password),
					"role_id"=>$role,
				));

					//Log
					FaturimiLog::create(array(
						"type"=>"Konto - Konto e re",
						"event"=>"Tabela: Konto, user=".$user.", password=".Hash::make($password).", roli=".$role,
						"author"=>Auth::user()->username,
						"date"=>date("Y-m-d H:i:s"),
					));

			if($query){
				return Redirect::back()
				->with("success","Konto ".$user." u regjistrua me sukses!");
			}
		}

		public function action_kontoEdit(){
			$id = Input::get("id");
			$user = Input::get("user");
			$password = Input::get("password");
			$confirm = Input::get("confirm");
			$role = Input::get("rolet");

			if($user==""){
				return Redirect::back()
				->with("msg","Ju lutem plotësoni emrin!");
			}
			if(Admin::where("username","=",$user)->where("id","!=",$id)->first()){
				return Redirect::back()
				->with("msg","Konto ekziston!");
			}
			if($password==""){
				return Redirect::back()
				->with("msg","Ju lutem plotësojeni fjalëkalimin");
			}
			if($password!=$confirm){
				return Redirect::back()
				->with("msg","Fjalëkalimi duhet të konfirmohet");
			}if($role==""){
				return Redirect::back()
				->with("msg", "Roli duhet të zgjidhet!");
			}

			$query = Admin::where("id","=",$id)->first();

			if($query){
				$query->username = $user;
				$query->password = Hash::make($password);
				$query->role_id = $role;
				$query->save();

				//Log
					FaturimiLog::create(array(
						"type"=>"Konto - Konto ndryshim",
						"event"=>"Tabela: Konto, user=".$user.", password=".Hash::make($password).", roli=".$role,
						"author"=>Auth::user()->username,
						"date"=>date("Y-m-d H:i:s"),
					));
				return Redirect::back()
				->with("success","Konto u ndryshua me sukses!");
			}
		}

		public function action_addRole(){
			$emri = Input::get("emri");
			$klientet = Input::get("klientet");
			$shto_fature = Input::get("shto_fature");
			$ndrysho_fature = Input::get("ndrysho_fature");
			$printo_fature = Input::get("printo_fature");
			$shto_pagese = Input::get("shto_pagese");
			$zbritje = Input::get("zbritje");
			$tv = Input::get("tv");
			$kaca = Input::get("kaca");
			$hyra = Input::get("hyra");
			$dala = Input::get("dala");
			$raportet = Input::get("raportet");
			$log = Input::get("log");
			$provider = Input::get("provider");
			$ndrysho_skadimin = Input::get("ndrysho_skadimin");
			$pakot = Input::get("pakot");
			$shitja = Input::get("shitja");
			$konto = Input::get("konto");
			$shto_user = Input::get("shto_user");
			$ndrysho_user = Input::get("ndrysho_user");
			$shto_rol = Input::get("shto_rol");
			$ndrysho_rol = Input::get("ndrysho_rol");
			$fshij_user = Input::get("fshij_user");
			$fshij_rol = Input::get("fshij_rol");

			if($emri==""){
				return Redirect::back()
				->with("msg","Emri duhet te plotësohet");
			}

			$query = Role::create(array(
				"emri"=>$emri,
				"shiqo_klient"=>$klientet,
				"shto_fature"=>$shto_fature,
				"ndrysho_fature"=>$ndrysho_fature,
				"printo_fature"=>$printo_fature,
				"shto_pagese"=>$shto_pagese,
				"zbritje"=>$zbritje,
				"shiqo_tv"=>$tv,
				"shiqo_kaca"=>$kaca,
				"shto_hyra"=>$hyra,
				"shto_dala"=>$dala,
				"shiqo_raportet"=>$raportet,
				"shiqo_log"=>$log,
				"shiqo_provider"=>$provider,
				"ndrysho_skadimin"=>$ndrysho_skadimin,
				"pakot"=>$pakot,
				"shitja"=>$shitja,
				"konto"=>$konto,
				"shto_user"=>$shto_user,
				"ndrysho_user"=>$ndrysho_user,
				"shto_rol"=>$shto_rol,
				"ndrysho_rol"=>$ndrysho_rol,
				"fshij_user"=>$fshij_user,
				"fshij_rol"=>$fshij_rol,
			));

					//Log
					FaturimiLog::create(array(
						"type"=>"Konto - Rol i ri",
						"event"=>"Tabela: Konto, emri i rolit=".$emri,
						"author"=>Auth::user()->username,
						"date"=>date("Y-m-d H:i:s"),
					));

			if($query){
				return Redirect::back()
				->with("success","Roli është regjistruar me sukses!");
			}
		}

		public function action_editRole(){
			$id = Input::get("id");
			$emri = Input::get("emri");
			$klientet = Input::get("klientet");
			$shto_fature = Input::get("shto_fature");
			$ndrysho_fature = Input::get("ndrysho_fature");
			$printo_fature = Input::get("printo_fature");
			$shto_pagese = Input::get("shto_pagese");
			$zbritje = Input::get("zbritje");
			$tv = Input::get("tv");
			$kaca = Input::get("kaca");
			$hyra = Input::get("hyra");
			$dala = Input::get("dala");
			$raportet = Input::get("raportet");
			$log = Input::get("log");
			$provider = Input::get("provider");
			$ndrysho_skadimin = Input::get("ndrysho_skadimin");
			$pakot = Input::get("pakot");
			$shitja = Input::get("shitja");
			$konto = Input::get("konto");
			$shto_user = Input::get("shto_user");
			$ndrysho_user = Input::get("ndrysho_user");
			$shto_rol = Input::get("shto_rol");
			$ndrysho_rol = Input::get("ndrysho_rol");
			$fshij_user = Input::get("fshij_user");
			$fshij_rol = Input::get("fshij_rol");

			if($emri==""){
				return Redirect::back()
				->with("msg","Emri duhet te plotësohet");
			}

			$query = Role::where("id","=",$id)->first();

			if($query){
				$query->emri = $emri;
				$query->shiqo_klient = $klientet;
				$query->shto_fature = $shto_fature;
				$query->ndrysho_fature = $ndrysho_fature;
				$query->printo_fature = $printo_fature;
				$query->shto_pagese = $shto_pagese;
				$query->zbritje = $zbritje;
				$query->shiqo_tv = $tv;
				$query->shiqo_kaca = $kaca;
				$query->shto_hyra = $hyra;
				$query->shto_dala = $dala;
				$query->shiqo_raportet = $raportet;
				$query->shiqo_log = $log;
				$query->shiqo_provider = $provider;
				$query->ndrysho_skadimin = $ndrysho_skadimin;
				$query->pakot = $pakot;
				$query->shitja = $shitja;
				$query->konto = $konto;
				$query->shto_user = $shto_user;
				$query->shto_rol = $shto_rol;
				$query->ndrysho_user = $ndrysho_user;
				$query->ndrysho_rol = $ndrysho_rol;
				$query->fshij_user = $fshij_user;
				$query->fshij_rol = $fshij_rol;

				$query->save();


				//Log
					FaturimiLog::create(array(
						"type"=>"Konto -  Ndryshim i rolit",
						"event"=>"Tabela: Konto, emri i rolit=".$emri,
						"author"=>Auth::user()->username,
						"date"=>date("Y-m-d H:i:s"),
					));
				return Redirect::back()
				->with("success","Roli u ndryshua me sukses!");
			}
		}

		public function action_deleteKonto(){
			$checkbox = Input::get("delete");
			if($checkbox==""){
				return Redirect::back();
			}
			
			foreach($checkbox as $checked){

				if($checked==Auth::user()->id){
					return Redirect::back()
					->with("msg","Nuk mund ta terminoni vetveten!");
				}
				if(Admin::where("id","=",$checked)->first()->role_id==1){
					return Redirect::back()
					->with("msg","Nuk mund ta terminoni Super Administratorin!");
				}
				$delete = Admin::where("id","=",$checked)
				->where("id","!=",Auth::user()->id)
				->where("role_id","!=",1)->delete();


					//Log
					FaturimiLog::create(array(
						"type"=>"Konto - Fshirje e kontove",
						"event"=>"Tabela: Konto, konto e fshire=".$checked,
						"author"=>Auth::user()->username,
						"date"=>date("Y-m-d H:i:s"),
					));
			}

			return Redirect::back();
		}

		public function action_deleteRole(){
			$checkbox = Input::get("delete");

			if($checkbox==""){
				return Redirect::back();
			}

			foreach($checkbox as $checked){

				if($checked==1){
					return Redirect::back()
					->with("msg","Nuk mund ta terminoni Super Administratorin!");
				}
				if($checked == Auth::user()->role_id){
					return Redirect::back()
					->with("msg","Nuk mund ta terminoni rolin që posedoni për momentin!");
				}
				$delete = Role::where("id","=",$checked)
				->where("id","!=",Auth::user()->role_id)
				->where("id","!=",1)->delete();


					//Log
					FaturimiLog::create(array(
						"type"=>"Konto - Fshirje e roleve",
						"event"=>"Tabela: Konto, nr i rolit=".$checked,
						"author"=>Auth::user()->username,
						"date"=>date("Y-m-d H:i:s"),
					));
			}

			return Redirect::back();
		}
	}
?>