<?php
	class Negenet_Login_Controller extends Base_Controller{

		public $layout = "negenet.layouts.login.login";

		public function action_index(){
			$this->layout->content = View::make("negenet.admin.login");
		}

		public function action_authenticate(){
			$username = Input::get('username');
			$password = Input::get('password');
			$credentials =  array(
				'username'=> $username, 
				'password'=> $password,
			);
			
			$id = Admin::where("username","=",$username)->first()?
			Admin::where("username","=",$username)->first()->id:"";

			if(Auth::attempt($credentials)){
				if(Auth::login($id))
					date_default_timezone_set("Europe/Tirane");
					//Log
					FaturimiLog::create(array(
						"type"=>"Log in",
						"event"=>"Tabela: Konto, emri=".$username,
						"author"=>Auth::user()->username,
						"date"=>date("Y-m-d H:i:s"),
					));
					return Redirect::to_route("admin");
			}else{
				return Redirect::to_route("login")
				->with("msg","Të dhënat nuk janë të sakta!");
			}
		}

		public function action_logout(){
				date_default_timezone_set("Europe/Tirane");					
				//Log
					FaturimiLog::create(array(
						"type"=>"Log out",
						"event"=>"Tabela: Konto, emri=".Auth::user()->username,
						"author"=>Auth::user()->username,
						"date"=>date("Y-m-d H:i:s"),
					));
			Auth::logout();

			return Redirect::to_route("login");
		}
	}		
?>