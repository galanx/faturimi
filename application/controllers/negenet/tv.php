<?php
	class Negenet_Tv_Controller extends Base_Controller{

		public function action_index(){
			$result = Tv::order_by("id","asc")->paginate();
			$this->layout->content = View::make("negenet.tv.index")
			->with("result",$result);
		}

		public function action_tv(){
			$this->layout->content = View::make("negenet.admin.tv");
		}

		public function action_search(){
			$prej = Input::get("prej");
			$deri = Input::get("deri");
			
			if($prej!=""&&$deri!=""){
				$result = Tv::where("data",">=",$prej)->where("data","<=",$deri)->order_by("data","asc")->get();
			}
			if($prej==""&&$deri!=""){
				$result = Tv::where("data","<=",$deri)->order_by("data","asc")->get();
			}
			if($prej!=""&&$deri==""){
				$result = Tv::where("data",">=",$prej)->order_by("data","asc")->get();
			}else{
				$result = Tv::where("data",">=",$prej)->where("data","<=",$deri)->order_by("data","asc")->get();
			}		
			$this->layout->content = View::make("negenet.tv.tv")
			->with("result",$result);
		}

		public function action_kartela($user){
			$this->layout->content = View::make("negenet.tv.kartela")
			->with("user",$user);
		}

		public function action_kartelaEdit(){
			$user = Input::get("user");
			$kartela = Input::get("kartela");

			if($kartela==""){
				return Redirect::back()
				->with("msg", "Kartela nuk mund të lihet zbrazët!");
			}

			$exists = Kartela::where("kartela","=",$kartela)->where("username","!=",$user)->first();
			if($exists){
				return Redirect::back()
				->with("msg", "Kartela është në perdorim!");
			}

			$query = Kartela::where("username","=",$user)->first();
			$query->kartela = $kartela;
			$query->save();

			return Redirect::back()
			->with("success","Kartela u ndryshua me sukses!");
		}
	}
?>