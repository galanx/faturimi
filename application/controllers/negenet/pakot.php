<?php
	class Negenet_Pakot_Controller extends Base_Controller{

		public function action_pakot(){
			$this->layout->content = View::make("negenet.admin.pakot")
			->with("services",Service::all());
		}

		public function action_edit(){
			$pako = Input::get("pakot");
			$muaj = Input::get("muaj")?Input::get("muaj"):1;
			$cmimi = Input::get("cmimi");
			$emri = "";

			//Muaj check and concatenation
			if($muaj>1){
				$emri = $pako."_".$muaj."_mujor";
			}else{
				$emri = $pako;
			}
				
			if($pako==""){
				return Redirect::to_route("pakot")
				->with("error","Ju lutem zgjidheni pakon!");
			}if($cmimi==""||$cmimi==0){
				return Redirect::to_route("pakot")
				->with("error","Çmimi nuk mund të jetë 0!");
			}
			
			//Check for srvid in rm_services
			$srvid = Service::where("srvname","=",$pako)->first();
			//Does the pako exist?
			$query = Pako::where("emri","=",$emri)->first();
			if($query){
					$query->cmimi = $cmimi;
					$query->oferta_mujore = $muaj;
					$query->srvid = $srvid->srvid;
					$query->save();

					//Log
					FaturimiLog::create(array(
						"type"=>"Cmimet e pakove - Ndryshim",
						"event"=>"Tabela: Pako, pako=".$srvid->srvname.", cmimi=".$cmimi.", periudha mujore = ".$muaj,
						"author"=>Auth::user()->username,
						"date"=>date("Y-m-d H:i:s"),
					));	
				return Redirect::to_route("pakot")
				->with("msg","Pako është ndryshuar me sukses!");
			}else{
				Pako::create(array(
					"emri"=>$emri,
					"cmimi"=>$cmimi,
					"oferta_mujore"=>$muaj,
					"srvid"=>$srvid->srvid,
				));

					//Log
					FaturimiLog::create(array(
						"type"=>"Cmimet e pakove",
						"event"=>"Tabela: Pako, pako=".$srvid->srvname.", cmimi=".$cmimi.", periudha mujore = ".$muaj,
						"author"=>Auth::user()->username,
						"date"=>date("Y-m-d H:i:s"),
					));
						
				return Redirect::to_route("pakot")
				->with("msg","Pako është ndryshuar me sukses!");
			}
		}
	}
?>