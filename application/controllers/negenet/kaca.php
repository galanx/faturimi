<?php
	class Negenet_Kaca_Controller extends Base_Controller{

		public function action_index(){

			$te_hyra = Kaca::sum("te_hyra");
			$te_dala = Kaca::sum("te_dala");
			$gjendja = $te_hyra - $te_dala;

			$this->layout->content = View::make("negenet.kaca.index")
			->with("totali", $gjendja)
			->with("te_hyra",$te_hyra)
			->with("te_dala",$te_dala);
		}

		public function action_shto(){
			$te_hyra = Input::get("hyra");
			$te_dala = Input::get("dala");

			date_default_timezone_set('Europe/Tirane');
			$data = date('Y-m-d H:i:s');
			$dalaVlera = Input::get("dalaVlera")!=""?Input::get("dalaVlera"):0;
			$vlera = Input::get("hyraVlera")!=""?Input::get("hyraVlera"):0;
			$gjendja = $vlera - $dalaVlera;

			if(isset($te_hyra)){

				$furnizuesi = Input::get("furnizuesi");
				$pershkrimi = Input::get("pershkrimi");
				
				if($vlera!=""||$vlera!=0&&$furnizuesi!=""&&$pershkrimi!=""){
					$query = Kaca::create(array(
					"user"=>Auth::user()->username,
					"te_hyra"=>$vlera,
					"furnizuesi"=>$furnizuesi,
					"pershkrimi"=>$pershkrimi,
					"data"=>$data,
					));

					//Log
					FaturimiLog::create(array(
						"type"=>"Kaca - Te hyra",
						"event"=>"Tabela: Kaca, vlera=".$vlera.", furnizuesi=".$furnizuesi.", pershkrimi=".$pershkrimi,
						"author"=>Auth::user()->username,
						"date"=>date("Y-m-d H:i:s"),
					));

				}else{
					return Redirect::back()
					->with("msg", "Plotëso të gjitha informatat për të hyrat!");
				}	
			}
			if(isset($te_dala)){
				$fatura = Input::get("fatura");
				$furnizuesiD = Input::get("furnizuesiDala");
				$shpenzuesi = Input::get("shpenzuesi");
				$pershkrimiD = Input::get("pershkrimiDala");

				if($dalaVlera==""||$dalaVlera==0){
					return Redirect::back()
					->with("msg", "Vlera e të dalave nuk mund të jetë 0!");
				}
				if($shpenzuesi==""){
					return Redirect::back()
					->with("msg", "Shpenzuesi duhet të ceket!");
				}
				if($pershkrimiD==""){
					return Redirect::back()
					->with("msg", "Keni harruar përshkrimin!");
				}

				$query = Kaca::create(array(
					"user"=>Auth::user()->username,
					"te_dala"=>$dalaVlera,
					"fatura"=>$fatura,
					"data"=>$data,
					"furnizuesi"=>$furnizuesiD,
					"shpenzuesi"=>$shpenzuesi,
					"pershkrimi"=>$pershkrimiD,
				));

					//Log
					FaturimiLog::create(array(
						"type"=>"Kaca - Te dala",
						"event"=>"Tabela: Kaca, vlera=".$dalaVlera.", shpenzuesi=".$shpenzuesi." , furnizuesi=".$furnizuesiD.", pershkrimi=".$pershkrimiD. " fatura nr.".$fatura,
						"author"=>Auth::user()->username,
						"date"=>date("Y-m-d H:i:s"),
					));
			}
			

			$totali = Kaca::sum("te_hyra") - Kaca::sum("te_dala");
			return Redirect::to_route("kaca");
		}

		public function action_search(){
			$prej = Input::get("prej");
			$deri = Input::get("deri");

			if($prej!=""&&$deri!=""){
				$result = Kaca::where("data",">=",$prej)->where("data","<=",$deri)->order_by("data","asc")->get();
			}
			if($prej==""&&$deri!=""){
				$result = Kaca::where("data","<=",$deri)->order_by("data","asc")->get();
			}
			if($prej!=""&&$deri==""){
				$result = Kaca::where("data",">=",$prej)->order_by("data","asc")->get();
			}else{
				$result = Kaca::where("data",">=",$prej)->where("data","<=",$deri)->order_by("data","asc")->get();
			}		
			$this->layout->content = View::make("negenet.kaca.kaca")
			->with("result",$result);
		}
	}
?>