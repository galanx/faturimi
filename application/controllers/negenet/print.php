<?php
	class Negenet_Print_Controller extends Base_Controller{
		public $layout = "negenet.layouts.print";

		public function action_internet($id){
			$fatura = Fatura::where("id","=",$id)->first();
			if($fatura->rregullt == 1){
				$this->layout->content = View::make("negenet.print.internetrregullt")
				->with("id",$fatura->id_rregullt>9?
				$fatura->id_rregullt."/".date("m",strtotime($fatura->data_krijimit)):
				"0".$fatura->id_rregullt."/".date("m",strtotime($fatura->data_krijimit)))
				->with("normal_id",$id);
			}else{
				$this->layout->content = View::make("negenet.print.internet")
				->with("id",$id);
			}
		}

		public function action_shitja($id){
			$fatura = Shitja::where("id","=",$id)->first();
			$this->layout->content = View::make("negenet.print.shitje")
			->with("fatura",$fatura);
		}
		public function action_shitjerregullt($id){
			$fatura = Shitja::where("id","=",$id)->first();
			$this->layout->content = View::make("negenet.print.shitjerregullt")
			->with("fatura",$fatura);
		}
		public function action_hyrja($id){
			$malli= Import::where("id","=",$id)->first();
			$this->layout->content = View::make("negenet.print.hyrje")
			->with("malli",$malli);
		}
	}
?>