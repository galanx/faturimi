<?php
	class Negenet_Raportet_Controller extends Base_Controller{

		public function action_raportet(){
			$this->layout->content = View::make("negenet.admin.raportet");
		}

		public function action_check(){
			$value = Input::get("checks");
			$prej = Input::get("prej");
			$deri = Input::get("deri");
			$user = Input::get("klienti");

			if($prej==""&&$deri==""){
				return Redirect::back()
				->with("msg","Data duhet të zgjidhet patjetër!");
			}

			//PAGESA CHECK
			if($value==1&&$prej==""&&$deri==""&&$user==""){
				return Redirect::to_route("pagesat");
			}
			if($value==1&&$prej!=""&&$deri==""&&$user==""){
				$result = Pagesa::where("data",">=",$prej)->order_by("data","asc")->get();
				$this->layout->content = View::make("negenet.pagesat.pagesat")
				->with("result",$result);
			}
			if($value==1&&$prej!=""&&$deri!=""&&$user==""){
				$result = Pagesa::where("data",">=",$prej)->where("data","<=",$deri)->order_by("data","asc")->get();
				$this->layout->content = View::make("negenet.pagesat.pagesat")
				->with("result",$result);
			}
			if($value==1&&$prej==""&&$deri!=""&&$user==""){
				$result = Pagesa::where("data","<=",$deri)->order_by("data","asc")->get();
				$this->layout->content = View::make("negenet.pagesat.pagesat")
				->with("result",$result);
			}
			if($value==1&&$prej==""&&$deri==""&&$user!=""){
				$result = Pagesa::where("username","like",$user."%")->get();
				$this->layout->content = View::make("negenet.pagesat.pagesat")
				->with("result",$result);
			}
			if($value==1&&$prej!=""&&$deri==""&&$user!=""){
				$result = Pagesa::where("username","like",$user."%")
				->where("data",">=",$prej)->order_by("data","asc")->get();
				$this->layout->content = View::make("negenet.pagesat.pagesat")
				->with("result",$result);
			}
			if($value==1&&$prej==""&&$deri!=""&&$user!=""){
				$result = Pagesa::where("username","like",$user."%")
				->where("data","<=",$deri)->order_by("data","asc")->get();
				$this->layout->content = View::make("negenet.pagesat.pagesat")
				->with("result",$result);
			}
			if($value==1&&$prej!=""&&$deri!=""&&$user!=""){
				$result = Pagesa::where("username","like",$user."%")
				->where("data",">=",$prej)->where("data","<=",$deri)->order_by("data","asc")->get();
				$this->layout->content = View::make("negenet.pagesat.pagesat")
				->with("result",$result);
			}


			//FATURA CHECK
			if($value==2&&$prej==""&&$deri==""&&$user==""){
				return Redirect::to_route("fatura");
			}
			if($value==2&&$prej!=""&&$deri==""&&$user==""){
				$result = Fatura::where("prej",">=",$prej)->order_by("prej","asc")->get();
				$this->layout->content = View::make("negenet.faturat.faturat")
				->with("result",$result);
			}
			if($value==2&&$prej!=""&&$deri!=""&&$user==""){
				$result = Fatura::where("prej",">=",$prej)->where("prej","<=",$deri)->order_by("prej","asc")->get();
				$this->layout->content = View::make("negenet.faturat.faturat")
				->with("result",$result);
			}
			if($value==2&&$prej==""&&$deri!=""&&$user==""){
				$result = Fatura::where("prej","<=",$deri)->order_by("prej","asc")->get();
				$this->layout->content = View::make("negenet.faturat.faturat")
				->with("result",$result);
			}
			if($value==2&&$prej==""&&$deri==""&&$user!=""){
				$result = Fatura::where("username","like",$user."%")->get();
				$this->layout->content = View::make("negenet.faturat.faturat")
				->with("result",$result);
			}
			if($value==2&&$prej!=""&&$deri==""&&$user!=""){
				$result = Fatura::where("username","like",$user."%")
				->where("prej",">=",$prej)->order_by("prej","asc")->get();
				$this->layout->content = View::make("negenet.faturat.faturat")
				->with("result",$result);
			}
			if($value==2&&$prej==""&&$deri!=""&&$user!=""){
				$result = Fatura::where("username","like",$user."%")
				->where("prej","<=",$deri)->order_by("prej","asc")->get();
				$this->layout->content = View::make("negenet.faturat.faturat")
				->with("result",$result);
			}
			if($value==2&&$prej!=""&&$deri!=""&&$user!=""){
				$result = Fatura::where("username","like",$user."%")
				->where("prej",">=",$prej)->where("prej","<=",$deri)->order_by("prej","asc")->get();
				$this->layout->content = View::make("negenet.faturat.faturat")
				->with("result",$result);
			}

			//TV CHECK
			if($value==3&&$prej==""&&$deri==""&&$user==""){
				return Redirect::to_route("tv_index");
			}
			if($value==3&&$prej!=""&&$deri==""&&$user==""){
				$result = Tv::where("data",">=",$prej)->order_by("data","asc")->get();
				$this->layout->content = View::make("negenet.tv.tv")
				->with("result",$result);
			}
			if($value==3&&$prej!=""&&$deri!=""&&$user==""){
				$result = Tv::where("data",">=",$prej)->where("data","<=",$deri)->order_by("data","asc")->get();
				$this->layout->content = View::make("negenet.tv.tv")
				->with("result",$result);
			}
			if($value==3&&$prej==""&&$deri!=""&&$user==""){
				$result = Tv::where("data","<=",$deri)->order_by("data","asc")->get();
				$this->layout->content = View::make("negenet.tv.tv")
				->with("result",$result);
			}
			if($value==3&&$prej==""&&$deri==""&&$user!=""){
				$result = Tv::where("username","like",$user."%")->get();
				$this->layout->content = View::make("negenet.tv.tv")
				->with("result",$result);
			}
			if($value==3&&$prej!=""&&$deri==""&&$user!=""){
				$result = Tv::where("username","like",$user."%")
				->where("data",">=",$prej)->order_by("data","asc")->get();
				$this->layout->content = View::make("negenet.tv.tv")
				->with("result",$result);
			}
			if($value==3&&$prej==""&&$deri!=""&&$user!=""){
				$result = Tv::where("username","like",$user."%")
				->where("data","<=",$deri)->order_by("data","asc")->get();
				$this->layout->content = View::make("negenet.tv.tv")
				->with("result",$result);
			}
			if($value==3&&$prej!=""&&$deri!=""&&$user!=""){
				$result = Tv::where("username","like",$user."%")
				->where("data",">=",$prej)->where("data","<=",$deri)->order_by("data","asc")->get();
				$this->layout->content = View::make("negenet.tv.tv")
				->with("result",$result);
			}

			//KYQJET CHECK
			if($value==4&&$prej==""&&$deri==""&&$user==""){
				return Redirect::to_route("admin");
			}
			if($value==4&&$prej!=""&&$deri==""&&$user==""){
				$result = User::where("createdon",">=",$prej)->order_by("createdon","asc")->get();
				$this->layout->content = View::make("negenet.kyqjet.kyqjet")
				->with("result",$result);
			}
			if($value==4&&$prej!=""&&$deri!=""&&$user==""){
				$result = User::where("createdon",">=",$prej)->where("createdon","<=",$deri)->order_by("createdon","asc")->get();
				$this->layout->content = View::make("negenet.kyqjet.kyqjet")
				->with("result",$result);
			}
			if($value==4&&$prej==""&&$deri!=""&&$user==""){
				$result = User::where("createdon","<=",$deri)->order_by("createdon","asc")->get();
				$this->layout->content = View::make("negenet.kyqjet.kyqjet")
				->with("result",$result);
			}
			if($value==4&&$prej==""&&$deri==""&&$user!=""){
				$result = User::where("username","like",$user."%")->get();
				$this->layout->content = View::make("negenet.kyqjet.kyqjet")
				->with("result",$result);
			}
			if($value==4&&$prej!=""&&$deri==""&&$user!=""){
				$result = User::where("username","like",$user."%")
				->where("createdon",">=",$prej)->order_by("createdon","asc")->get();
				$this->layout->content = View::make("negenet.kyqjet.kyqjet")
				->with("result",$result);
			}
			if($value==4&&$prej==""&&$deri!=""&&$user!=""){
				$result = User::where("username","like",$user."%")
				->where("createdon","<=",$deri)->order_by("createdon","asc")->get();
				$this->layout->content = View::make("negenet.kyqjet.kyqjet")
				->with("result",$result);
			}
			if($value==4&&$prej!=""&&$deri!=""&&$user!=""){
				$result = User::where("username","like",$user."%")
				->where("createdon",">=",$prej)->where("createdon","<=",$deri)->order_by("createdon","asc")->get();
				$this->layout->content = View::make("negenet.kyqjet.kyqjet")
				->with("result",$result);
			}

			//ARKIVAT CHECK
			if($value==5&&$prej==""&&$deri==""&&$user==""){
				$result = User::where("enableuser","=","0")->get();
				$this->layout->content = View::make("negenet.arkivat.arkivat")
				->with('result',$result);
			}
			if($value==5&&$prej!=""&&$deri==""&&$user==""){
				$result = User::where("expiration",">=",$prej)->where("enableuser","=","0")->order_by("expiration","asc")->get();
				$this->layout->content = View::make("negenet.arkivat.arkivat")
				->with("result",$result);
			}
			if($value==5&&$prej!=""&&$deri!=""&&$user==""){
				$result = User::where("expiration",">=",$prej)->where("expiration","<=",$deri)->where("enableuser","=","0")->order_by("expiration","asc")->get();
				$this->layout->content = View::make("negenet.arkivat.arkivat")
				->with("result",$result);
			}
			if($value==5&&$prej==""&&$deri!=""&&$user==""){
				$result = User::where("expiration","<=",$deri)->where("enableuser","=","0")->order_by("expiration","asc")->get();
				$this->layout->content = View::make("negenet.arkivat.arkivat")
				->with("result",$result);
			}
			if($value==5&&$prej==""&&$deri==""&&$user!=""){
				$result = User::where("username","like",$user."%")->where("enableuser","=","0")->get();
				$this->layout->content = View::make("negenet.arkivat.arkivat")
				->with("result",$result);
			}
			if($value==5&&$prej!=""&&$deri==""&&$user!=""){
				$result = User::where("username","like",$user."%")
				->where("expiration",">=",$prej)->order_by("expiration","asc")->where("enableuser","=","0")->get();
				$this->layout->content = View::make("negenet.arkivat.arkivat")
				->with("result",$result);
			}
			if($value==5&&$prej==""&&$deri!=""&&$user!=""){
				$result = User::where("username","like",$user."%")
				->where("expiration","<=",$deri)->where("enableuser","=","0")->order_by("expiration","asc")->get();
				$this->layout->content = View::make("negenet.arkivat.arkivat")
				->with("result",$result);
			}
			if($value==5&&$prej!=""&&$deri!=""&&$user!=""){
				$result = User::where("username","like",$user."%")
				->where("expiration",">=",$prej)->where("expiration","<=",$deri)->where("enableuser","=","0")->order_by("expiration","asc")->get();
				$this->layout->content = View::make("negenet.arkivat.arkivat")
				->with("result",$result);
			}

			//FATURA TE RREGULLTA CHECK
			if($value==6&&$prej==""&&$deri==""&&$user==""){
				$result = Fatura::where("rregullt","=",1)->order_by("muaji_rregullues","asc")->order_by("id_rregullt","asc")->get();
				$this->layout->content = View::make("negenet.faturat.rregullta")
				->with("result",$result);
			}
			if($value==6&&$prej!=""&&$deri==""&&$user==""){
				$result = Fatura::where("prej",">=",$prej)
				->where("rregullt","=",1)->order_by("muaji_rregullues","asc")->order_by("id_rregullt","asc")->get();
				$this->layout->content = View::make("negenet.faturat.faturat")
				->with("result",$result);
			}
			if($value==6&&$prej!=""&&$deri!=""&&$user==""){
				$result = Fatura::where("prej",">=",$prej)->where("prej","<=",$deri)
				->where("rregullt","=",1)->order_by("muaji_rregullues","asc")->order_by("id_rregullt","asc")->get();
				$this->layout->content = View::make("negenet.faturat.faturat")
				->with("result",$result);
			}
			if($value==6&&$prej==""&&$deri!=""&&$user==""){
				$result = Fatura::where("prej","<=",$deri)
				->where("rregullt","=",1)->order_by("muaji_rregullues","asc")->order_by("id_rregullt","asc")->get();
				$this->layout->content = View::make("negenet.faturat.faturat")
				->with("result",$result);
			}
			if($value==6&&$prej==""&&$deri==""&&$user!=""){
				$result = Fatura::where("username","like",$user."%")
				->where("rregullt","=",1)->order_by("muaji_rregullues","asc")->order_by("id_rregullt","asc")->get();
				$this->layout->content = View::make("negenet.faturat.faturat")
				->with("result",$result);
			}
			if($value==6&&$prej!=""&&$deri==""&&$user!=""){
				$result = Fatura::where("username","like",$user."%")
				->where("prej",">=",$prej)->where("rregullt","=",1)->order_by("muaji_rregullues","asc")->order_by("id_rregullt","asc")->get();
				$this->layout->content = View::make("negenet.faturat.faturat")
				->with("result",$result);
			}
			if($value==6&&$prej==""&&$deri!=""&&$user!=""){
				$result = Fatura::where("username","like",$user."%")
				->where("prej","<=",$deri)->where("rregullt","=",1)->order_by("muaji_rregullues","asc")->order_by("id_rregullt","asc")->get();
				$this->layout->content = View::make("negenet.faturat.faturat")
				->with("result",$result);
			}
			if($value==6&&$prej!=""&&$deri!=""&&$user!=""){
				$result = Fatura::where("username","like",$user."%")
				->where("prej",">=",$prej)->where("prej","<=",$deri)->where("rregullt","=",1)
				->order_by("muaji_rregullues","asc")->order_by("id_rregullt","asc")->get();
				$this->layout->content = View::make("negenet.faturat.faturat")
				->with("result",$result);
			}

			//PAGESAT SIPAS PAKOS
			if($value==7){
				$result = Pako::all();
				$this->layout->content = View::make("negenet.pagesat.sipaspaketes")
				->with("result",$result)->with("prej",$prej)->with("deri",$deri);
			}
		}
	}
?>