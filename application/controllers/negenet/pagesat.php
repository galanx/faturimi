<?php
	class Negenet_Pagesat_Controller extends Base_Controller{

		public function action_index(){
			$result = Pagesa::order_by("id","desc")->paginate();
			$this->layout->content = View::make("negenet.pagesat.index")
			->with("result",$result);
		}

		public function action_search(){
			$prej = Input::get("prej");
			$deri = Input::get("deri");
			
			if($prej!=""&&$deri!=""){
				$result = Pagesa::where("data",">=",$prej)->where("data","<=",$deri)->order_by("data","asc")->get();
			}
			if($prej==""&&$deri!=""){
				$result = Pagesa::where("data","<=",$deri)->order_by("data","asc")->get();
			}
			if($prej!=""&&$deri==""){
				$result = Pagesa::where("data",">=",$prej)->order_by("data","asc")->get();
			}else{
				$result = Pagesa::where("data",">=",$prej)->where("data","<=",$deri)->order_by("data","asc")->get();
			}		
			$this->layout->content = View::make("negenet.pagesat.pagesat")
			->with("result",$result);
		}

		public function action_pazari(){
			date_default_timezone_set("Europe/Tirane");
			$not_bank = Pagesa::where("koment","not like","%bank%")->where("data",">",date("Y-m-d H:i:s",strtotime("- 12 hour")))->sum("vlera");
			$bank = Pagesa::where("koment","like","%bank%")->where("data",">",date("Y-m-d H:i:s",strtotime("- 12 hour")))->sum("vlera");
			$result = Pagesa::where("data",">",date("Y-m-d H:i:s",strtotime("- 12 hour")))->order_by("data","asc")->get();
			$shitja = Shitja::where("data",">",date("Y-m-d",strtotime("- 24 hour")))->where("paguar",">",0)->order_by("data","asc")->get();
			$this->layout->content = View::make("negenet.pagesat.pazari")
			->with("result",$result)->with("shitja",$shitja)->with("not_bank",$not_bank)->with("bank",$bank);
		}

		public function action_pazarisearch(){
			date_default_timezone_set("Europe/Tirane");

			$prej = Input::get("prej");
			$deri = Input::get("deri");
			
			if($prej!=""&&$deri!=""){
				$result = Pagesa::where("data",">=",$prej)->where("data","<=",$deri)->order_by("data","asc")->get();
				$shitja = Shitja::where("data",">=",$prej)->where("paguar",">",0)->where("data","<=",$deri)->order_by("data","asc")->get();
			}
			if($prej==""&&$deri!=""){
				$result = Pagesa::where("data","<=",$deri)->order_by("data","asc")->get();
				$shitja = Shitja::where("data","<=",$deri)->where("paguar",">",0)->order_by("data","asc")->get();
			}
			if($prej!=""&&$deri==""){
				$result = Pagesa::where("data",">=",$prej)->order_by("data","asc")->get();
				$shitja = Shitja::where("data",">=",$prej)->where("paguar",">",0)->order_by("data","asc")->get();
			}else{
				$result = Pagesa::where("data",">=",$prej)->where("data","<=",$deri)->order_by("data","asc")->get();
				$shitja = Shitja::where("data",">=",$prej)->where("data","<=",$deri)->where("paguar",">",0)->order_by("data","asc")->get();
			}

			$this->layout->content = View::make("negenet.pagesat.pazarisearch")
			->with("result",$result)->with("shitja",$shitja);
		}

		public function action_sipasPakos(){
			$prej = Input::get("prej");
			$deri = Input::get("deri");
			
			$result = Pako::all();
			$this->layout->content = View::make("negenet.pagesat.sipaspaketes")
			->with("result",$result)->with("prej",$prej)->with("deri",$deri);
		}
	}
?>