<?php
	class Negenet_Shitja_Controller extends Base_Controller{

		public function action_index(){
			$this->layout->content = View::make("negenet.shitja.index")
			->with("shitjet",Shitja::order_by("id","desc")->paginate());
		}

		public function action_regjistroProdukt(){
			$this->layout->content = View::make("negenet.shitja.regjistroprodukt");
		}

		public function action_stokuGjendja(){
			$result = Produktet::order_by("id","asc")->paginate();
			$this->layout->content = View::make("negenet.shitja.gjendja")
			->with("shitjet",$result);
		}
		public function action_kerkoMallin(){
			$kerko = Input::get("kerkoMallin");
			$result = Produktet::where("emri","like",$kerko."%")->get();
			$this->layout->content = View::make("negenet.shitja.kerkogjendjen")
			->with("shitjet",$result);
		}
		public function action_hyrjet(){
			$this->layout->content = View::make("negenet.shitja.malli")
			->with("shitjet",Import::order_by("id","desc")->paginate());
		}

		public function action_kode(){
			$this->layout->content = View::make("negenet.shitja.kode");
		}

		public function action_shtoKode(){
			date_default_timezone_set("Europe/Tirane");
			$kodi = Input::get("kodi");

			if(trim($kodi)!=""){
				if(Kode::where("kodi","like","%".$kodi."%")->first()){
					return Redirect::back()
					->with("msg","Kodi: ".$kodi." ekziston në listë!");
				}

				$query = Kode::create(array(
					"kodi"=> $kodi,
					"data"=>date("Y-m-d"),
				));
			}

			//File
			$kodet = Input::file("kodet");
			
			if(Input::file("kodet.name")!=""){
				
				$filename = Input::file("kodet.name").'.'.File::extension(Input::file("kodet.name"));
				$path = "public/uploads/";
				
				Input::upload("kodet",$path,$filename);

				$file2=$path.$filename;
				$str=file_get_contents("$file2");
				$str = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $str);
				file_put_contents("$file2", "$str");

				$file = fopen($path.$filename,"r");
				while(!feof($file)){
					$string = fgets($file);
					if(Kode::where("kodi","like","%".$string."%")->first()){
						return Redirect::back()
						->with("msg","Kodi: ".$string." ekziston në listë!");
					}

					$query = Kode::create(array(
						"kodi"=> preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "",$string),
						"data"=>date("Y-m-d"),
					));
				}
				$file = fclose($file);
				$file = File::delete($path.$filename);
			}	
			return Redirect::back();
		}

		//Regjistro Produkt
		public function action_shtoProdukt(){
			$emri = Input::get("emri");
			$pershkrimi = Input::get("pershkrimi");
			$cmimi = Input::get("cmimi");

			if($emri==""||$cmimi==""||$pershkrimi==""||$cmimi==0){
				return Redirect::back()
				->with("msg","Ju lutem plotësoni të gjitha");
			}
			if(Produktet::where("emri","=",$emri)->first()){
				return Redirect::back()
				->with("msg","Produkti ekziston në databazë");
			}
			else if(!Produktet::where("emri","=",$emri)->first()){
				$regjistro = Produktet::create(array(
					"emri"=>$emri,
					"pershkrimi"=>$pershkrimi,
					"cmimi"=>$cmimi,
				));

					//Log
					FaturimiLog::create(array(
						"type"=>"Shitja - Regjistrim i ri ne stok",
						"event"=>"Tabela: shitja, produkti=".$emri.", pershkrimi: ".$pershkrimi." cmimi: ".$cmimi,
						"author"=>Auth::user()->username,
						"date"=>date("Y-m-d H:i:s"),
					));	
				return Redirect::to_route("stokuGjendja");
			}
		}

		public function action_shtoFature(){
			$this->layout->content = View::make("negenet.shitja.shtofature");
		}

		public function action_shtoHyrje(){
			$this->layout->content = View::make("negenet.shitja.shtohyrje");
		}

		public function action_faturat(){
			$prej = Input::get("prej");
			$deri = Input::get("deri");

			$result = Shitja::where("data",">=",$prej)->where("data","<=",$deri)->order_by("data","asc")->get();

			if($prej==""&&$deri!=""){
				$result = Shitja::where("data","<=",$deri)->order_by("data","asc")->get();
			}
			if($prej!=""&&$deri==""){
				$result = Shitja::where("data",">=",$prej)->order_by("data","asc")->get();
			}
			if($prej!=""&&$deri!=""){
				$result = Shitja::where("data",">=",$prej)->where("data","<=",$deri)->order_by("data","asc")->get();
			}

			$this->layout->content = View::make("negenet.shitja.faturat")
			->with("shitjet",$result);
		}

		public function action_hyrjetMallit(){
			$prej = Input::get("prej");
			$deri = Input::get("deri");

			$result = Import::where("data",">=",$prej)->where("data","<=",$deri)->order_by("data","asc")->get();

			if($prej==""&&$deri!=""){
				$result = Import::where("data","<=",$deri)->order_by("data","asc")->get();
			}
			if($prej!=""&&$deri==""){
				$result = Import::where("data",">=",$prej)->order_by("data","asc")->get();
			}
			if($prej!=""&&$deri!=""){
				$result = Import::where("data",">=",$prej)->where("data","<=",$deri)->order_by("data","asc")->get();
			}

			$this->layout->content = View::make("negenet.shitja.hyrjet")
			->with("shitjet",$result);
		}

		//Shto fature shitje
		public function action_shtoFatura(){
			date_default_timezone_set("Europe/Tirane");
			$saHere = Input::get("saHere");
			$bleresi = Input::get("bleresi");
			$paguar = Input::get("paguar")!=""?1:0;
			$koment = Input::get("koment");

			$produkti = Input::get("produkti");
			$sasia = Input::get("sasia");
			$cmimi = Input::get("cmimi");

			$fatura_id = Shitja::order_by("id","desc")->first()?Shitja::order_by("id","desc")->first()->id+1:1;

			if($bleresi==""){
				return Redirect::back()
				->with("msg","Kerkohet emri i bleresit!");
			}
			if($produkti==""){
				return Redirect::back()
				->with("msg","Zgjedheni produktin per secilen kolone qe shtoni!");
			}

			for($i=1;$i<=$saHere;$i++){
				$produkti2 = Input::get("produkti".$i);
				$sasia2 = Input::get("sasia".$i);
				$cmimi2 = Input::get("cmimi".$i);

				if($produkti2==""){	
				return Redirect::back()
					->with("msg","Zgjedheni produktin per secilen kolone qe shtoni!");
				}
			}

			$shitja = Shitja::create(array(
				"bleresi"=>$bleresi,
				"paguar"=>$paguar,
				"koment"=>$koment,
				"data"=>date("Y-m-d"),
			));

			$query = MalliShitja::create(array(
				"shitja_id"=>$fatura_id,
				"p_id"=>$produkti,
				"sasia"=>$sasia,
				"cmimi"=>$cmimi,
			));

			$produktet = Produktet::where("id","=",$produkti)->first();
			$produktet->gjendja -= $sasia;
			$produktet->save();

			for($i=1;$i<=$saHere;$i++){
				$produkti3 = Input::get("produkti".$i);
				$sasia3 = Input::get("sasia".$i);
				$cmimi3 = Input::get("cmimi".$i);

				$query2 = MalliShitja::create(array(
					"shitja_id"=>$fatura_id,
					"p_id"=>$produkti3,
					"sasia"=>$sasia3,
					"cmimi"=>$cmimi3,
				));

				$produktet3 = Produktet::where("id","=",$produkti3)->first();
				$produktet3->gjendja -= $sasia3;
				$produktet3->save();
			}

					//Log
					FaturimiLog::create(array(
						"type"=>"Fature e re e shitjes",
						"event"=>"Tabela: shitja, bleresi=".$bleresi,
						"author"=>Auth::user()->username,
						"date"=>date("Y-m-d H:i:s"),
					));	
			return Redirect::to_route("fat_shitja",$fatura_id);	
		}

		//Shto hyrje te mallit
		public function action_addHyrje(){
			date_default_timezone_set("Europe/Tirane");
			$saHere = Input::get("saHere");
			$furnizuesi = Input::get("furnizuesi");
			$fatura = Input::get("fatura");

			$produkti = Input::get("produkti");
			$sasia = Input::get("sasia");
			$cmimi = Input::get("cmimi");

			$import_id = Import::order_by("id","desc")->first()?Import::order_by("id","desc")->first()->id+1:1;

			if($furnizuesi==""){
				return Redirect::back()
				->with("msg","Kerkohet emri i furnizuesit!");
			}
			if($produkti==""){
				return Redirect::back()
				->with("msg","Zgjedheni produktin per secilen kolone qe shtoni!");
			}

			for($i=1;$i<=$saHere;$i++){
				$produkti2 = Input::get("produkti".$i);
				$sasia2 = Input::get("sasia".$i);
				$cmimi2 = Input::get("cmimi".$i);

				if($produkti2==""){	
				return Redirect::back()
					->with("msg","Zgjedheni produktin per secilen kolone qe shtoni!");
				}
			}

			$shitja = Import::create(array(
				"furnizuesi"=>$furnizuesi,
				"fatura"=>$fatura,
				"data"=>date("Y-m-d"),
			));

			$query = MalliImport::create(array(
				"import_id"=>$import_id,
				"p_id"=>$produkti,
				"sasia"=>$sasia,
				"cmimi"=>$cmimi,
			));

			$produktet = Produktet::where("id","=",$produkti)->first();
			$produktet->gjendja += $sasia;
			$produktet->save();

			for($i=1;$i<=$saHere;$i++){
				$produkti3 = Input::get("produkti".$i);
				$sasia3 = Input::get("sasia".$i);
				$cmimi3 = Input::get("cmimi".$i);

				$query2 = MalliImport::create(array(
					"import_id"=>$import_id,
					"p_id"=>$produkti3,
					"sasia"=>$sasia3,
					"cmimi"=>$cmimi3,
				));

				$produktet3 = Produktet::where("id","=",$produkti3)->first();
				$produktet3->gjendja += $sasia3;
				$produktet3->save();
			}
					//Log
					FaturimiLog::create(array(
						"type"=>"Hyrje e mallit",
						"event"=>"Tabela: import, furnizuesi=".$furnizuesi.", fatura=".$fatura,
						"author"=>Auth::user()->username,
						"date"=>date("Y-m-d H:i:s"),
					));

			return Redirect::to_route("malli");		
		}

		public function action_ndrysho($id){
			$this->layout->content = View::make("negenet.shitja.ndrysho")
			->with("id",$id);
		}

		public function action_edit(){
			$shitja_id = Input::get("id");
			$bleresi = Input::get("bleresi");
			$paguar = Input::get("paguar")!=""?1:0;
			$koment = Input::get("koment");
			$fiskal = Input::get("fiskal");

			$x = Input::get("x");

			if($bleresi==""){
				return Redirect::back()
				->with("msg","Bleresi duhet te plotesohet!");
			}

			$shitja = Shitja::where("id","=",$shitja_id)->first();
			$shitja->bleresi = $bleresi;
			$shitja->paguar = $paguar;
			$shitja->koment = $koment;
			$shitja->fiskal = $fiskal;
			$shitja->save();

			if($paguar==1){
				date_default_timezone_set("Europe/Tirane");
				$shitja->data = date("Y-m-d");
				$shitja->save();
			}

			for($i=0;$i<=($x+1);$i++){
				$produkti2 = Input::get("produkti".$i);
				$sasia2 = Input::get("sasia".$i);
				$cmimi2 = Input::get("cmimi".$i);
				$malli_id = Input::get($i);

				if($produkti2==""){
					break;
				}

				$malli = MalliShitja::where("shitja_id","=",$shitja_id)->where("id","=",$malli_id)->first();
				$prod = Produktet::where("id","=",$produkti2)->first();
				if($malli->p_id!=$produkti2){
					$prod2 = Produktet::where("id","=",$malli->p_id)->first();
					$prod2->gjendja += $malli->sasia;
					$prod2->save();

					$prod->gjendja -= $sasia2;
					$prod->save();
				}else{
					$prod->gjendja += $malli->sasia;
					$prod->gjendja -= $sasia2;
					$prod->save();
				}


				$malli->sasia = $sasia2;
				$malli->cmimi = $cmimi2;
				$malli->p_id = $produkti2;
				$malli->save();
			}

					//Log
					FaturimiLog::create(array(
						"type"=>"Shitja - ndryshim fature",
						"event"=>"Tabela: shitja, fatura=".$shitja->id.", bleresi: ".$bleresi,
						"author"=>Auth::user()->username,
						"date"=>date("Y-m-d H:i:s"),
					));	
			return Redirect::back()
			->with("success","Fatura është ndryshuar me sukses!");
		}

		public function action_fshijkode(){
			$checked = Input::get('checked');

			if($checked==""){
				return Redirect::back();
			}

			foreach($checked as $check){
				$kodi = Kode::where("id","=",$check)->first()?Kode::where("id","=",$check)->first()->kodi:$check;
				//Log
				FaturimiLog::create(array(
					"type"=>"Kode - Fshirje e kodeve",
					"event"=>"Tabela: Kodet, kodi e fshire=".$kodi,
					"author"=>Auth::user()->username,
					"date"=>date("Y-m-d H:i:s"),
				));

				date_default_timezone_set("Europe/Tirane");
				$delete = Kode::where("id","=",$check)->delete();
			}

			return Redirect::back();
		}
	}
?>