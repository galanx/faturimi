<?php
	class Negenet_Provider_Controller extends Base_Controller{

		public function action_provider(){
			$provider = Id::where("provider","=",1)->get();
			$this->layout->content = View::make("negenet.admin.provider")
			->with('result',$provider);
		}
	} 
?>